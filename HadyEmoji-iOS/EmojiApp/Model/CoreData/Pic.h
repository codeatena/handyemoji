//
//  Pic.h
//  EmojiApp
//
//  Created by JinSung Han on 3/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Pic : NSManagedObject

@property (nonatomic, retain) NSString * filename;
@property (nonatomic, retain) NSString * type;

@end
