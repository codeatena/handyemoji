//
//  Pic.m
//  EmojiApp
//
//  Created by JinSung Han on 3/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "Pic.h"


@implementation Pic

@dynamic filename;
@dynamic type;

@end
