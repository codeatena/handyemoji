//
//  Art.m
//  EmojiApp
//
//  Created by JinSung Han on 3/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "Art.h"


@implementation Art

@dynamic text;
@dynamic type;

- (id) init
{
    [super init];
    if (self == nil) {
    }
    return self;
}

@end
