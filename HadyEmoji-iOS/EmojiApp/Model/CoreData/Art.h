//
//  Art.h
//  EmojiApp
//
//  Created by JinSung Han on 3/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Art : NSManagedObject

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * type;

@end
