//
//  PicCategory.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "PicCategory.h"

@implementation PicCategory

@synthesize imageName, title, arrPicImage;

- (id) init
{
    self = [super init];
    
    imageName = [[NSString alloc] init];
    title = [[NSString alloc] init];
    arrPicImage = [[NSMutableArray alloc] init];
    
    return self;
}

@end
