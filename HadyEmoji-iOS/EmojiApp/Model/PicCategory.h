//
//  PicCategory.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PicCategory : NSObject

@property (nonatomic, retain) NSString* imageName;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSMutableArray* arrPicImage;

@end
