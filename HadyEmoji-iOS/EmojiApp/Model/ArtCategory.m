//
//  ArtCategory.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ArtCategory.h"

@implementation ArtCategory

@synthesize imageName, title, arrArtPattern;

- (id) init
{
    self = [super init];
    
    imageName = [[NSString alloc] init];
    title = [[NSString alloc] init];
    arrArtPattern = [[NSMutableArray alloc] init];
    
    return self;
}

@end
