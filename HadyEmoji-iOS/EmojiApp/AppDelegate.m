//
//  AppDelegate.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTabBarViewController.h"
#import "Art.h"
#import "Pic.h"
#import "define.h"

@implementation AppDelegate

@synthesize favEmojiArtArray, favTextArtArray, favStaticPicArray, favAnimatePicArray;
@synthesize savedArtArray, savedPicArray;
@synthesize recentArtArray, recentPicArray;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize nCurrentDevice;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    CGRect screenSize = [UIScreen mainScreen].bounds;
//    NSLog(@"width = %f, height = %f", screenSize.size.width, screenSize.size.height);
    
    if (screenSize.size.height < 500.0f) {
        nCurrentDevice = IPHONE_GENEREAL;
        NSLog(@"IPHONE_GENEREAL");
    } else if (screenSize.size.height < 700.0f) {
        nCurrentDevice = IPHONE_RETINA;
        NSLog(@"IPHONE_RETINA");
    } else {
        nCurrentDevice = IPAD;
        NSLog(@"IPAD");
    }
    
    [self readAllData];
    
    MainTabBarViewController *mainTabBarCtrl = [[MainTabBarViewController alloc] init];
    [self.window setRootViewController: mainTabBarCtrl];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) readAllData
{
    favEmojiArtArray    = [[NSMutableArray alloc] init];
    favTextArtArray     = [[NSMutableArray alloc] init];
    favStaticPicArray   = [[NSMutableArray alloc] init];
    favAnimatePicArray  = [[NSMutableArray alloc] init];
    
    savedArtArray       = [[NSMutableArray alloc] init];
    savedPicArray       = [[NSMutableArray alloc] init];
    
    recentArtArray      = [[NSMutableArray alloc] init];
    recentPicArray      = [[NSMutableArray alloc] init];
    
//    NSMutableArray* arrArt = [[NSMutableArray alloc] initWithArray:[self readAllArt]];

    NSArray* arrArt = [self readAllArt];
    for (Art* art in arrArt) {
        if ([art.type isEqualToString:STR_FAVORITE_ART_EMOJI]) {
            [favEmojiArtArray addObject: art.text];
        }
        if ([art.type isEqualToString:STR_FAVORITE_ART_TEXT]) {
            [favTextArtArray addObject: art.text];
        }
        if ([art.type isEqualToString:STR_SAVED_ART]) {
            [savedArtArray addObject: art.text];
        }
        if ([art.type isEqualToString:STR_RECENT_ART]) {
            [recentArtArray addObject: art.text];
        }
    }
    
    NSArray* arrPic = [self readAllPic];
    
    for (Pic* pic in arrPic) {
        if ([pic.type isEqualToString:STR_FAVORITE_PIC_STATIC]) {
            [favStaticPicArray addObject: pic.filename];
        }
        if ([pic.type isEqualToString:STR_FAVORITE_PIC_ANIMATE]) {
            [favAnimatePicArray addObject: pic.filename];
        }
        if ([pic.type isEqualToString:STR_SAVED_PIC]) {
            [savedPicArray addObject: pic.filename];
        }
        if ([pic.type isEqualToString:STR_RECENT_PIC]) {
            [recentPicArray addObject: pic.filename];
        }
    }
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"EmojiApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"dds.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSArray *) readAllArt
{
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Art"
                                               inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray * array = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return array;
}

- (NSArray*) readAllPic
{
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * entity = [NSEntityDescription entityForName:@"Pic"
                                               inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray * array = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return array;
}

- (void) addArt:(NSString*) text TYPE:(NSString*) strType
{
    Art *newArt = [NSEntityDescription insertNewObjectForEntityForName:@"Art" inManagedObjectContext: _managedObjectContext];
    newArt.text = text;
    newArt.type = strType;
    
    [self saveContext];
}

- (void) refreshArt
{
    NSArray* arrArt = [self readAllArt];
    for (Art* art in arrArt) {
        [self.managedObjectContext deleteObject:art];
        [self saveContext];
    }
    
    for (NSString* str in favEmojiArtArray) {
        [self addArt:str TYPE:STR_FAVORITE_ART_EMOJI];
        [self saveContext];
    }
    for (NSString* str in favTextArtArray) {
        [self addArt:str TYPE:STR_FAVORITE_ART_TEXT];
        [self saveContext];
    }
    for (NSString* str in savedArtArray) {
        [self addArt:str TYPE:STR_SAVED_ART];
        [self saveContext];
    }
    for (NSString* str in recentArtArray) {
        [self addArt:str TYPE:STR_RECENT_ART];
        [self saveContext];
    }
}

- (void) addPic:(NSString*) fileName TYPE:(NSString*)       strType
{
    Pic *newPic = [NSEntityDescription insertNewObjectForEntityForName:@"Pic" inManagedObjectContext: _managedObjectContext];
    newPic.filename = fileName;
    newPic.type = strType;
    
    [self saveContext];
}

- (void) refreshPic
{
    NSArray* arrPic = [self readAllPic];
    for (Pic* pic in arrPic) {
        [self.managedObjectContext deleteObject:pic];
        [self saveContext];
    }
    for (NSString* str in favStaticPicArray) {
        [self addPic:str TYPE:STR_FAVORITE_PIC_STATIC];
        [self saveContext];
    }
    for (NSString* str in favAnimatePicArray) {
        [self addPic:str TYPE:STR_FAVORITE_PIC_ANIMATE];
        [self saveContext];
    }
    for (NSString* str in savedPicArray) {
        [self addPic:str TYPE:STR_SAVED_PIC];
        [self saveContext];
    }
    for (NSString* str in recentPicArray) {
        [self addPic:str TYPE:STR_RECENT_PIC];
        [self saveContext];
    }
}

@end