//
//  KeyBoardViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iAd/ADBannerView.h"
#import "HPGrowingTextView.h"
#import "EmojiViewController.h"
#import <MessageUI/MessageUI.h>
#import "GADBannerView.h"

@interface KeyBoardViewController : UIViewController<HPGrowingTextViewDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet UIButton* btnEmoji;
    IBOutlet UIView*   viewMenu;
//    IBOutlet ADBannerView* adView;
    
    UIView* containerView;
    HPGrowingTextView *textView;
    
    EmojiViewController* emojiViewCtrl;
    GADBannerView*  bannerView_;
}

- (IBAction)onEmoji:(id)sender;
- (IBAction)onArt:(id)sender;
- (IBAction)onSend:(id)sender;
- (IBAction)onDone:(id)sender;

- (void) touchKeyboadTap;
- (void) addEmoji: (NSString*) strAdd;
- (void) onBack;
- (void) onClear;
- (void) setFont: (UIFont*) font;
- (void) addArt: (NSString*) strAdd;

@end
