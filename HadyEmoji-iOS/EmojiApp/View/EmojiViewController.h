//
//  EmojiViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/10/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
@class KeyBoardViewController;

enum CATEGORY_EMOJI_TYPE {
    EMOJI_TYPE_PEOPLE = 0,
    EMOJI_TYPE_NATURE,
    EMOJI_TYPE_OBJECT,
    EMOJI_TYPE_PLACE,
    EMOJI_TYPE_SYMBOL,
    EMOJI_TYPE_FACE,
    EMOJI_TYPE_LIFE,
    EMOJI_TYPE_ABC,
    EMOJI_TYPE_MONEY,
    EMOJI_TYPE_ARROW,
    EMOJI_TYPE_MATH
};

@interface EmojiViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet    UICollectionView*       collEmoji;
    IBOutlet    UITableView*            tblFont;
    
    IBOutlet    UIScrollView*           scrollView;
    
    IBOutlet    UIButton*               btnPeople;
    IBOutlet    UIButton*               btnNature;
    IBOutlet    UIButton*               btnObject;
    IBOutlet    UIButton*               btnPlace;
    IBOutlet    UIButton*               btnSymbol;
    IBOutlet    UIButton*               btnFace;
    IBOutlet    UIButton*               btnLife;
    IBOutlet    UIButton*               btnABC;
    IBOutlet    UIButton*               btnMoney;
    IBOutlet    UIButton*               btnArrow;
    IBOutlet    UIButton*               btnMath;

    NSMutableArray* arrPeople;
    NSMutableArray* arrNature;
    NSMutableArray* arrObject;
    NSMutableArray* arrPlace;
    NSMutableArray* arrSymbol;
    NSMutableArray* arrFace;
    NSMutableArray* arrLife;
    NSMutableArray* arrABC;
    NSMutableArray* arrMoney;
    NSMutableArray* arrArrow;
    NSMutableArray* arrMath;
    
    NSMutableArray* arrCurEmoji;
    
    KeyBoardViewController* parentViewCtrl;
    
    int curEmojiType;
    
    NSMutableArray* arrFontNames;    
}

@property (nonatomic) int curFont;

- (IBAction)onPeople:(id)sender;
- (IBAction)onNature:(id)sender;
- (IBAction)onObject:(id)sender;
- (IBAction)onPlace:(id)sender;
- (IBAction)onSymbol:(id)sender;
- (IBAction)onFace:(id)sender;
- (IBAction)onLife:(id)sender;
- (IBAction)onABC:(id)sender;
- (IBAction)onMoney:(id)sender;
- (IBAction)onArrow:(id)sender;
- (IBAction)onMath:(id)sender;

- (IBAction)onBack:(id)sender;
- (IBAction)onSpace:(id)sender;
- (IBAction)onReturn:(id)sender;
- (IBAction)onClear:(id)sender;

- (void) setParentViewController: (KeyBoardViewController*) keyboardViewCtrl;

- (void) onFont;
- (void) onEmojiOrABC;

@end
