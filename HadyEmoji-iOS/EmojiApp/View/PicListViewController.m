//
//  PicListViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "PicListViewController.h"
#import "PicCategory.h"
#import "SCGIFImageView.h"
#import "PicSectionViewController.h"
#import "define.h"
#import "GADBannerView.h"
#import "AppDelegate.h"

#define GIF_IMAGEVIEW_TAG 1000

@interface PicListViewController ()

@end

@implementation PicListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadPicStaticContent];
    [self loadPicAnimatedContent];
    [self loadPicSectionContent];
    
    nPicType = PIC_TYPE_STATIC;
    
    UISegmentedControl *statFilter = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Static", @"Animated", nil]];
    //    [statFilter setSegmentedControlStyle:UISegmentedControlStyleBar];
    [statFilter setFrame: CGRectMake(0, 0, 160, 30)];
    //    [statFilter sizeToFit];
    self.navigationItem.titleView = statFilter;
    
    [statFilter addTarget: self action: @selector(segmentedSwitch:) forControlEvents:UIControlEventValueChanged];
    
    [statFilter setSelectedSegmentIndex: 0];
    [tblPicAnimated setHidden: YES];
    
    bannerView_ = [[GADBannerView alloc]
                                   initWithFrame:CGRectMake(0.0, 466, GAD_SIZE_320x50.width,                                   GAD_SIZE_320x50.height)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];

    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPAD) {
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
    }
}

- (void) segmentedSwitch: (id) sender
{
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    if (selectedSegment == 0) {
        nPicType = PIC_TYPE_STATIC;
        [tblPicStatic setHidden: NO];
        [tblPicAnimated setHidden: YES];
    }
    if (selectedSegment == 1) {
        nPicType = PIC_TYPE_ANIMATE;
        [tblPicStatic setHidden: YES];
        [tblPicAnimated setHidden: NO];
    }
}

- (void) addStaticSecion:(int) index COUNT:(int) count
{
    NSMutableArray* arrImageName = [[NSMutableArray alloc] init];

    for (int  i = 0; i < count; i++) {
        NSString* strFileName = [NSString stringWithFormat:@"Static_%d_%d.png", index, i + 1];
        if (index == 5) {
            strFileName = [NSString stringWithFormat:@"Static_%d_%d.jpg", index, i + 1];
        }
        [arrImageName addObject:strFileName];
    }
    
    [arrPicSectionStatic addObject:arrImageName];
}

- (void) addAnimateSection:(int) index COUNT:(int) count
{
    NSMutableArray* arrImageName = [[NSMutableArray alloc] init];
    
    for (int  i = 0; i < count; i++) {
        NSString* strFileName = [NSString stringWithFormat:@"Animate_%d_%d.gif", index, i + 1];
        [arrImageName addObject:strFileName];
    }
    
    [arrPicSectionAnimated addObject:arrImageName];
}

- (void) loadPicSectionContent
{
    
//    NSMutableArray* arrImageName = [[NSMutableArray alloc] init];
    
    arrPicSectionStatic = [[NSMutableArray alloc] init];
    arrPicSectionAnimated = [[NSMutableArray alloc] init];
    
    [self addStaticSecion:1 COUNT:46];
    [self addStaticSecion:2 COUNT:26];
    [self addStaticSecion:3 COUNT:34];
    [self addStaticSecion:4 COUNT:38];
    [self addStaticSecion:5 COUNT:18];
    [self addStaticSecion:6 COUNT:15];
    [self addStaticSecion:7 COUNT:37];
    [self addStaticSecion:8 COUNT:12];
    [self addStaticSecion:9 COUNT:30];
    [self addStaticSecion:10 COUNT:19];
    [self addStaticSecion:11 COUNT:25];
    [self addStaticSecion:12 COUNT:30];
    [self addStaticSecion:13 COUNT:14];
    
    [self addAnimateSection:1 COUNT:21];
    [self addAnimateSection:2 COUNT:29];
    [self addAnimateSection:3 COUNT:18];
    [self addAnimateSection:4 COUNT:22];
    [self addAnimateSection:5 COUNT:18];
    [self addAnimateSection:6 COUNT:18];
    [self addAnimateSection:7 COUNT:16];
    [self addAnimateSection:8 COUNT:25];
    [self addAnimateSection:9 COUNT:23];
    [self addAnimateSection:10 COUNT:19];
    [self addAnimateSection:11 COUNT:24];
    [self addAnimateSection:12 COUNT:24];
    [self addAnimateSection:13 COUNT:22];

//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Animal 1_Sta.jpg", @"Animal 2_Sta.jpg", @"Baby_Sta.jpg", @"Black_Sta.jpg", @"Blue_Sta.jpg", @"Box_Sta.jpg", @"Christmas_Sta.jpg", @"Girly_Sta.jpg", @"Gray_Sta.jpg", @"Green_Sta.jpg", @"Greeting_Sta.jpg", @"Life_Sta.jpg", @"Man_Sta.jpg", @"Rabbit_Sta.jpg", @"Sport_Sta.jpg", @"Wow_Sta.jpg", @"Yellow_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"128_1.png", @"128_3.png", @"128_3.png", @"128_4.png", @"128_5.png", @"128_6.png", @"128_7.png", @"128_11.png", @"128_12.png", @"128_13.png", @"128_15.png", @"128_16.png", @"128_18.png", @"128_19.png", @"128_20.png", @"128_21.png", @"128_23.png", @"128_24.png", @"128_25.png", @"128_27.png", @"128_28.png", @"128_29.png", @"128_30.png", @"128_34.png", @"128_35.png", @"idiotic_smile.png", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Blue_Sta.jpg", @"Box_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"amazing.png", @"anger.png", @"bad_egg.png", @"bad_smile.png", @"beaten.png", @"big_smile.png", @"black_heart.png", @"cry.png", @"electric_shock.png", @"exciting.png", @"eyes_droped.png", @"girl.png", @"greedy.png", @"grimace.png", @"haha.png", @"happy.png", @"horror.png", @"money.png", @"nothing.png", @"nothing_to_say.png", @"red_heart.png", @"scorn.png", @"secret_smile.png", @"shame.png", @"shocked.png", @"super_man.png", @"the_iron_man.png", @"unhappy.png", @"victory.png", @"what.png", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Gray_Sta.jpg", @"Green_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Greeting_Sta.jpg", @"Life_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Man_Sta.jpg", @"Rabbit_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Sport_Sta.jpg", @"Wow_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Yellow_Sta.jpg", @"Animal 1_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Animal 2_Sta.jpg", @"Baby_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Black_Sta.jpg", @"Blue_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Box_Sta.jpg", @"Christmas_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Girly_Sta.jpg", @"Gray_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Green_Sta.jpg", @"Greeting_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Life_Sta.jpg", @"Man_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Rabbit_Sta.jpg", @"Sport_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Wow_Sta.jpg", @"Yellow_Sta.jpg", nil];
//    [arrPicSectionStatic addObject: arrImageName];
//    
//    
//    
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Animal 1_Ani.gif", @"Animal 2_Ani.gif", @"Carrot_Ani.gif", @"Cartoon_Ani.gif", @"Christmas_Ani.gif", @"Duck_Ani.gif", @"Face_Ani.gif", @"Greeting_Ani.gif", @"Kitty_Ani.gif", @"Life_Ani.gif", @"Love_Ani.gif", @"Man_Ani.gif", @"Mood_Ani.gif", @"Music_Ani.gif", @"Rabbit_Ani.gif", @"Tiny_Ani.gif", @"Vehicle_Ani.gif", @"Yellow_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Carrot_Ani.gif", @"Cartoon_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Christmas_Ani.gif", @"Duck_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Face_Ani.gif", @"Greeting_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Kitty_Ani.gif", @"Life_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Love_Ani.gif", @"Man_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Mood_Ani.gif", @"Music_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Rabbit_Ani.gif", @"Tiny_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Vehicle_Ani.gif", @"Yellow_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Animal 1_Ani.gif", @"Yellow_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Animal 2_Ani.gif", @"Carrot_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Cartoon_Ani.gif", @"Christmas_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Duck_Ani.gif", @"Face_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Greeting_Ani.gif", @"Kitty_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Life_Ani.gif", @"Love_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Man_Ani.gif", @"Mood_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Music_Ani.gif", @"Rabbit_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
//    
//    arrImageName = [[NSMutableArray alloc] initWithObjects:@"Tiny_Ani.gif", @"Vehicle_Ani.gif", nil];
//    [arrPicSectionAnimated addObject: arrImageName];
}

- (void) loadPicStaticContent
{
    arrPicStatic = [[NSMutableArray alloc] init];
    
    PicCategory* picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Yellow_Sta.jpg";
    picCategory.title = @"Yellow";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Blue_Sta.jpg";
    picCategory.title = @"Blue";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Gray_Sta.jpg";
    picCategory.title = @"Gray";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Black_Sta.jpg";
    picCategory.title = @"Black";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Green_Sta.jpg";
    picCategory.title = @"Green";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Baby_Sta.jpg";
    picCategory.title = @"Baby";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Man_Sta.jpg";
    picCategory.title = @"Man";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Greeting_Sta.jpg";
    picCategory.title = @"Greeting";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Life_Sta.jpg";
    picCategory.title = @"Life";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Sport_Sta.jpg";
    picCategory.title = @"Sport";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Animal 1_Sta.jpg";
    picCategory.title = @"Animal 1";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Animal 2_Sta.jpg";
    picCategory.title = @"Animal 2";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Box_Sta.jpg";
    picCategory.title = @"Box";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Rabbit_Sta.jpg";
    picCategory.title = @"Rabbit";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Wow_Sta.jpg";
    picCategory.title = @"Wow";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Girly_Sta.jpg";
    picCategory.title = @"Girly";
    [arrPicStatic addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Christmas_Sta.jpg";
    picCategory.title = @"Christmas";
    [arrPicStatic addObject: picCategory];
}

- (void) loadPicAnimatedContent
{
    arrPicAnimated = [[NSMutableArray alloc] init];
    
    PicCategory* picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Mood_Ani.gif";
    picCategory.title = @"Mood";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Face_Ani.gif";
    picCategory.title = @"Face";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Yellow_Ani.gif";
    picCategory.title = @"Yellow";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Greeting_Ani.gif";
    picCategory.title = @"Greeting";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Love_Ani.gif";
    picCategory.title = @"Love";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Man_Ani.gif";
    picCategory.title = @"Man";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Music_Ani.gif";
    picCategory.title = @"Music";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Animal 1_Ani.gif";
    picCategory.title = @"Animal 1";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Cartoon_Ani.gif";
    picCategory.title = @"Cartoon";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Tiny_Ani.gif";
    picCategory.title = @"Tiny";
    [arrPicAnimated addObject: picCategory];
    
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Life_Ani.gif";
    picCategory.title = @"Life";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Kitty_Ani.gif";
    picCategory.title = @"Kitty";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Carrot_Ani.gif";
    picCategory.title = @"Carrot";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Duck_Ani.gif";
    picCategory.title = @"Duck";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Rabbit_Ani.gif";
    picCategory.title = @"Rabbit";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Animal 2_Ani.gif";
    picCategory.title = @"Animal 2";
    [arrPicAnimated addObject: picCategory];
    
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Vehicle_Ani.gif";
    picCategory.title = @"Vehicle";
    [arrPicAnimated addObject: picCategory];
 
    picCategory = [[PicCategory alloc] init];
    picCategory.imageName = @"Christmas_Ani.gif";
    picCategory.title = @"Christmas";
    [arrPicAnimated addObject: picCategory];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSInteger count = 0;
    
    if (tableView == tblPicStatic)
        count = [arrPicStatic count];
    if (tableView == tblPicAnimated)
        count = [arrPicAnimated count];
    
    return 13;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"acell"];
    
    SCGIFImageView* scgImageView = nil;
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"acell"];
        
        if (tableView == tblPicStatic) {
            PicCategory* picCategory = [[PicCategory alloc] init];
            picCategory = [arrPicStatic objectAtIndex: indexPath.row];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if (tableView == tblPicAnimated) {
            PicCategory* picCategory = [[PicCategory alloc] init];
            picCategory = [arrPicAnimated objectAtIndex: indexPath.row];
            
            cell.imageView.image = [UIImage imageNamed: @"Animal_Txt.jpg"];

            cell.imageView.hidden = YES;
            scgImageView = [[SCGIFImageView alloc] initWithFrame: CGRectMake(10, 0, 44, 44)];
            
            AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if (delegate.nCurrentDevice == IPAD) {
                scgImageView.frame = CGRectMake(10, 0, 65, 65);
            }

            scgImageView.tag = GIF_IMAGEVIEW_TAG;
            
//            [scgImageView setData:imageData];
            
            [cell addSubview: scgImageView];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    } else {

        if (tableView == tblPicAnimated) {
            scgImageView = (SCGIFImageView *) [cell viewWithTag: GIF_IMAGEVIEW_TAG];
            cell.imageView.hidden = YES;
        }
    }
    
    if (tableView == tblPicAnimated) {
        PicCategory* picCategory = [[PicCategory alloc] init];
        picCategory = [arrPicAnimated objectAtIndex: indexPath.row];
//        cell.textLabel.text = picCategory.title;
        
//        cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
        NSString* strFileName = [NSString stringWithFormat:@"Animate_%d_1.gif", indexPath.row + 1];
        NSString* filePath = [[NSBundle mainBundle] pathForResource:strFileName ofType:nil];
        NSData* imageData = [NSData dataWithContentsOfFile:filePath];
        [scgImageView setData:imageData];
    }
    
    if (tableView == tblPicStatic) {
        PicCategory* picCategory = [[PicCategory alloc] init];
        picCategory = [arrPicStatic objectAtIndex: indexPath.row];
        //        cell.textLabel.text = picCategory.title;
        
//        cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
        
        cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Static_%ld_1.png", indexPath.row + 1]];
        if (indexPath.row == 4) {
            cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Static_%ld_1.jpg", indexPath.row + 1]];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PicSectionViewController* picSectionViewCtrl = nil;
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (delegate.nCurrentDevice == IPAD) {
        picSectionViewCtrl = [[PicSectionViewController alloc] initWithNibName:@"PicSectionViewController_iPad" bundle:nil];
    } else {
        picSectionViewCtrl = [[PicSectionViewController alloc] initWithNibName:@"PicSectionViewController" bundle:nil];
    }
    
    PicCategory* picCategory = [[PicCategory alloc] init];

    NSMutableArray* arrImageName = [[NSMutableArray alloc] init];
    
    if (tableView == tblPicStatic) {
        arrImageName = [arrPicSectionStatic objectAtIndex: indexPath.row];
        picCategory = [arrPicStatic objectAtIndex:indexPath.row];
    }
    if (tableView == tblPicAnimated) {
        arrImageName = [arrPicSectionAnimated objectAtIndex: indexPath.row];
        picCategory = [arrPicAnimated objectAtIndex:indexPath.row];
    }
    
    [picSectionViewCtrl setArrayImageName: arrImageName];
    [picSectionViewCtrl setPicType: nPicType];
    [picSectionViewCtrl setControllerType: CATEGORY_PIC];
//    picSectionViewCtrl.title = picCategory.title;
    
    [self.navigationController pushViewController:picSectionViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

@end
