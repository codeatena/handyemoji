//
//  PicSectionViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "PicSectionViewController.h"
#import "SCGIFImageView.h"
#import "AppDelegate.h"
#import "define.h"
#import "GADBannerView.h"

#define PIC_TAG 1000

@interface PicSectionViewController ()

@end

@implementation PicSectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [collPicSection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
//    arrImageName = [[NSMutableArray alloc] init];
    
    bannerView_ = [[GADBannerView alloc]
                                   initWithFrame:CGRectMake(0.0, 466, GAD_SIZE_320x50.width,                                   GAD_SIZE_320x50.height)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPAD) {
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setArrayImageName:(NSMutableArray*) arr
{
    arrImageName = [[NSMutableArray alloc] init];
    arrImageName = arr;
    
    [collPicSection reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrImageName count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView1 cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    UICollectionViewCell *cell = [collectionView1 dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    SCGIFImageView* scgImageView = nil;
    scgImageView = (SCGIFImageView*) [cell viewWithTag: PIC_TAG];
    
    if (scgImageView == nil) {
        
        scgImageView = [[SCGIFImageView alloc] initWithFrame: CGRectMake(0, 0, 70, 70)];
        
        AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        if (delegate.nCurrentDevice == IPAD) {
            scgImageView.frame = CGRectMake(0, 0, 110, 110);
        }
        
        scgImageView.tag = PIC_TAG;
        
        [cell addSubview: scgImageView];
    }
    
    NSString* fileName = [arrImageName objectAtIndex:indexPath.row];
    NSString* filePath = [[NSBundle mainBundle] pathForResource: fileName ofType:nil];
    NSData* imageData = [NSData dataWithContentsOfFile:filePath];
    [scgImageView setData:imageData];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    curIndex = indexPath.row;
    if (nControllerType == CATEGORY_PIC) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Favourite", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == FAVOURITE_STATIC_PIC || nControllerType == FAVOURITE_ANIMATE_PIC) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Remove", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == SAVED_PIC) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Delete", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == RECENT_PIC) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Favourite", @"Remove", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
//    if (buttonIndex == 0) {
//        [self sendMessage];
//    } else if (buttonIndex == 1) {
//        [self onCopy];
//    } else if (buttonIndex == 2) {
//        [self sendEmail];
//    } else if (buttonIndex == 3) {
//        [self onFavourite];
//    }
    
    NSString* title = [actionSheet buttonTitleAtIndex: buttonIndex];
    NSLog(@"%@", title);
    
    if ([title isEqualToString:@"Message"]) {
        [self sendMessage];
    }
    if ([title isEqualToString:@"Copy"]) {
        [self onCopy];
    }
    if ([title isEqualToString:@"Email"]) {
        [self sendEmail];
    }
    if ([title isEqualToString:@"Favourite"]) {
        [self onFavourite];
    }
    if ([title isEqualToString:@"Remove"] || [title isEqualToString:@"Delete"]) {
        [self onRemove];
    }
}

- (void) sendMessage
{
    NSString* str = [arrImageName objectAtIndex: curIndex];
    NSLog(@"%@", str);
    
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    if([MFMessageComposeViewController canSendText])
    {
//        controller.body = textView.text;
//        controller.recipients = [NSArray arrayWithObjects:@"1(234)567-8910", nil];
        NSString *str1 = [[NSBundle mainBundle] pathForResource:str ofType:nil];
        NSLog(@"%@", str1);
        
        NSData *imageData = [NSData dataWithContentsOfFile:str1];
        
        [controller addAttachmentData:imageData typeIdentifier:@"public.data" filename:str1];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void) sendEmail
{
    NSString* str = [arrImageName objectAtIndex: curIndex];
    NSLog(@"%@", str);
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        //        UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
        //        NSData *imageData = UIImagePNGRepresentation(myImage);
        //        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
        
        NSString *str1 = [[NSBundle mainBundle] pathForResource:str ofType:nil];
        NSLog(@"%@", str1);

        NSData *imageData = [NSData dataWithContentsOfFile:str1];
        
        
        [mailer addAttachmentData:imageData mimeType:@"image/gif" fileName:str];
        
        [self presentViewController:mailer animated:YES completion:nil];
        [mailer release];
    }
}

- (void) onRemove
{
    NSString* str = [arrImageName objectAtIndex: curIndex];

    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (nControllerType == SAVED_PIC) {
        [delegate.savedPicArray removeObject:str];
        [self setArrayImageName:delegate.savedPicArray];
        [delegate refreshPic];
    }
    if (nControllerType == FAVOURITE_STATIC_PIC) {
        [delegate.favStaticPicArray removeObject:str];
        [self setArrayImageName:delegate.favStaticPicArray];
        [delegate refreshPic];
    }
    if (nControllerType == FAVOURITE_ANIMATE_PIC) {
        [delegate.favAnimatePicArray removeObject:str];
        [self setArrayImageName:delegate.favAnimatePicArray];
        [delegate refreshPic];
    }
    if (nControllerType == RECENT_PIC) {
        [delegate.recentPicArray removeObject:str];
        [self setArrayImageName:delegate.recentPicArray];
        [delegate refreshPic];
    }
}

- (void) onCopy
{
    NSString* str = [arrImageName objectAtIndex: curIndex];
    [UIPasteboard generalPasteboard].image = [UIImage imageNamed:str];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate.recentPicArray addObject:str];
    [delegate addPic:str TYPE:STR_RECENT_PIC];
}

- (void) onFavourite
{
    NSString* str = [arrImageName objectAtIndex: curIndex];
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];

    if (nPicType == PIC_TYPE_STATIC) {
        [delegate.favStaticPicArray addObject:str];
        [delegate addPic:str TYPE:STR_FAVORITE_PIC_STATIC];
    } else if (nPicType == PIC_TYPE_ANIMATE) {
        [delegate.favAnimatePicArray addObject:str];
        [delegate addPic:str TYPE:STR_FAVORITE_PIC_ANIMATE];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    NSString* str = [arrImageName objectAtIndex: curIndex];

    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
        {
            AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [delegate.recentPicArray addObject:str];
            [delegate addPic:str TYPE:STR_RECENT_PIC];
            
            break;
        }
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString* str = [arrImageName objectAtIndex: curIndex];

    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
        {
            NSLog(@"Mail sent");
            
            AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [delegate.recentPicArray addObject:str];
            [delegate addPic:str TYPE:STR_RECENT_PIC];
            break;
        }
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) setPicType: (int) nType
{
    nPicType = nType;
}

- (void) setControllerType: (int) type
{
    nControllerType = type;
}

@end
