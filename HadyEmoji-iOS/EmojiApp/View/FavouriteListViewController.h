//
//  FavouriteListViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"

@interface FavouriteListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    GADBannerView*  bannerView_;
}

@end
