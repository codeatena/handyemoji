//
//  ArtSectionViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ArtSectionViewController.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "AppDelegate.h"
#import "define.h"
#import "ArtListViewController.h"
#import "GADBannerView.h"

@interface ArtSectionViewController ()

@end

@implementation ArtSectionViewController

@synthesize strText;
@synthesize parentViewCtrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    bubbleTable.bubbleDataSource = self;
    
    // The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
    // Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
    // Groups are delimited with header which contains date and time for the first message in the group.
    
    bubbleTable.snapInterval = 120;
    
    // The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
    // Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)
    
    bubbleTable.showAvatars = NO;
    
    // Uncomment the line below to add "Now typing" bubble
    // Possible values are
    //    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
    //    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
    //    - NSBubbleTypingTypeNone - no "now typing" bubble
    
    bubbleTable.typingBubble = NSBubbleTypingTypeSomebody;
    [bubbleTable setParent: self];
    [bubbleTable reloadData];
    
    bannerView_ = [[GADBannerView alloc]
                                   initWithFrame:CGRectMake(0.0, 466, GAD_SIZE_320x50.width,                                   GAD_SIZE_320x50.height)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
        
        if (nControllerType == KEYBOART_ART) {
            [bubbleTable setFrame:CGRectMake(0, 65, 320, 453)];
            [bannerView_ setFrame:CGRectMake(0, 518, 320, 50)];
        }
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
        
        if (nControllerType == KEYBOART_ART) {
            [bubbleTable setFrame:CGRectMake(0, 65, 320, 453)];
            [bannerView_ setFrame:CGRectMake(0, 430, 320, 50)];
        }
    }
    if (delegate.nCurrentDevice == IPAD) {
//        bubbleTable.backgroundColor = [UIColor greenColor];
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
        [bubbleTable setFrame:CGRectMake(0, 65, 748, 810)];
        if (nControllerType == KEYBOART_ART) {
            [bannerView_ setFrame:CGRectMake(10, 934, 748, 90)];
            [bubbleTable setFrame:CGRectMake(0, 65, 748, 867)];
        }
    }
    
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIBubbleTableViewDataSource implementation

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
//    NSLog(@"%ld", bubbleData.count);
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

- (void) setArtArray:(NSMutableArray*) arr
{
    int k = 0;
    bubbleData = [[NSMutableArray alloc] init];

    for (NSString* art in arr) {
        
        k ++;
        
        NSBubbleData *bubble = [NSBubbleData dataWithText:art date:[NSDate dateWithTimeIntervalSinceNow:k] type:BubbleTypeMine];
        
        [bubbleData addObject:bubble];
    }
    
    [bubbleTable reloadData];
}

- (void) onAction
{
    if (nControllerType == CATEGORY_ART) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Favourite", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == FAVOURITE_EMOJI_ART || nControllerType == FAVOURITE_TEXT_ART) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Remove", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == SAVED_ART) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Delete", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == RECENT_ART) {
        UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Copy", @"Email", @"Favourite", @"Remove", nil];
        popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [popupQuery showInView:self.view];
    }
    if (nControllerType == KEYBOART_ART) {
        ArtListViewController* artListViewCtrl = (ArtListViewController*) parentViewCtrl;
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            NSLog(@"Dismiss completed");
            [artListViewCtrl addArt:strText];
        }];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    /*
    if (buttonIndex == 0) {
        [self sendMessage];
    } else if (buttonIndex == 1) {
        [self onCopy];
    } else if (buttonIndex == 2) {
        [self sendEmail];
    } else if (buttonIndex == 3) {
        [self onFavourite];
    }
     */
    
    NSString* title = [actionSheet buttonTitleAtIndex: buttonIndex];
    NSLog(@"%@", title);
    
    if ([title isEqualToString:@"Message"]) {
        [self sendMessage];
    }
    if ([title isEqualToString:@"Copy"]) {
        [self onCopy];
    }
    if ([title isEqualToString:@"Email"]) {
        [self sendEmail];
    }
    if ([title isEqualToString:@"Favourite"]) {
        [self onFavourite];
    }
    if ([title isEqualToString:@"Remove"] || [title isEqualToString:@"Delete"]) {
        [self onRemove];
    }
}

- (void) sendMessage
{
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = strText;
        //        controller.recipients = [NSArray arrayWithObjects:@"1(234)567-8910", nil];
        controller.messageComposeDelegate = self;
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void) sendEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        //        [mailer setSubject:@"A Message from MobileTuts+"];
        
        //        UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
        //        NSData *imageData = UIImagePNGRepresentation(myImage);
        //        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
        
        NSString *emailBody = strText;
        [mailer setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController: mailer animated:YES completion:nil];
        [mailer release];
    }
}

- (void) onCopy
{
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString: strText];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate.recentArtArray addObject: strText];
    [delegate addArt:strText TYPE: STR_RECENT_ART];
}

- (void) onFavourite
{
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (nArtType == ART_TYPE_EMOJI) {
        [delegate.favEmojiArtArray addObject: strText];
        [delegate addArt:strText TYPE: STR_FAVORITE_ART_EMOJI];
    } else if (nArtType == ART_TYPE_TEXT){
        [delegate.favTextArtArray addObject: strText];
        [delegate addArt:strText TYPE: STR_FAVORITE_ART_TEXT];
    }
}

- (void) onRemove
{
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];

    if (nControllerType == SAVED_ART) {
        [delegate.savedArtArray removeObject:strText];
        [self setArtArray:delegate.savedArtArray];
        [delegate refreshArt];
    }
    if (nControllerType == FAVOURITE_EMOJI_ART) {
        [delegate.favEmojiArtArray removeObject:strText];
        [self setArtArray:delegate.favEmojiArtArray];
        [delegate refreshArt];
    }
    if (nControllerType == FAVOURITE_TEXT_ART) {
        [delegate.favTextArtArray removeObject:strText];
        [self setArtArray:delegate.favTextArtArray];
        [delegate refreshArt];
    }
    if (nControllerType == RECENT_ART) {
        [delegate.recentArtArray removeObject:strText];
        [self setArtArray:delegate.recentArtArray];
        [delegate refreshArt];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
        case MessageComposeResultSent:
        {
            AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [delegate.recentArtArray addObject: strText];
            [delegate addArt:strText TYPE: STR_RECENT_ART];
            
            break;
        }
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
        {
            AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
            [delegate.recentArtArray addObject: strText];
            [delegate addArt:strText TYPE: STR_RECENT_ART];
            
            NSLog(@"Mail sent");
            break;
        }
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) setArtType: (int) type
{
    nArtType = type;
}

- (void) setControllerType: (int) type
{
    nControllerType = type;
}

@end