//
//  FavouriteListViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "FavouriteListViewController.h"
#import "AppDelegate.h"
#import "ArtSectionViewController.h"
#import "PicSectionViewController.h"
#import "define.h"
#import "GADBannerView.h"

@interface FavouriteListViewController ()

@end

@implementation FavouriteListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Favourite";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    bannerView_ = [[GADBannerView alloc]
                                   initWithFrame:CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPAD) {
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSInteger count = 0;
    if (section == 0) {
        count = 4;
    }
    if (section == 1) {
        count = 2;
    }
//    if (section == 2) {
//        count = 2;
//    }
    
    return count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* title = @"";
    
    if (section == 0) {
        title = @"FAVOURITE";
    }
    if (section == 1) {
        title = @"RECENT";
    }
//    if (section == 2) {
//        title = @"RECENT";
//    }
    
    return title;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"acell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"acell"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0)
            cell.textLabel.text = @"Emoji Art";
        if (indexPath.row == 1)
            cell.textLabel.text = @"Text Art";
        if (indexPath.row == 2)
            cell.textLabel.text = @"Static Pic";
        if (indexPath.row == 3)
            cell.textLabel.text = @"Animated Pic";
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0)
            cell.textLabel.text = @"Art";
        if (indexPath.row == 1)
            cell.textLabel.text = @"Pic";
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0)
            cell.textLabel.text = @"Art";
        if (indexPath.row == 1)
            cell.textLabel.text = @"Pic";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    PicSectionViewController* picSectionViewCtrl = nil;
    ArtSectionViewController* artSectionViewCtrl = nil;
    
    if (delegate.nCurrentDevice == IPAD) {
        picSectionViewCtrl = [[PicSectionViewController alloc] initWithNibName:@"PicSectionViewController_iPad" bundle:nil];
        artSectionViewCtrl = [[ArtSectionViewController alloc] initWithNibName:@"ArtSectionViewController_iPad" bundle:nil];

    } else {
        picSectionViewCtrl = [[PicSectionViewController alloc] initWithNibName:@"PicSectionViewController" bundle:nil];
        artSectionViewCtrl = [[ArtSectionViewController alloc] initWithNibName:@"ArtSectionViewController" bundle:nil];
    }

    [delegate readAllData];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [artSectionViewCtrl setArtArray:delegate.favEmojiArtArray];
            [artSectionViewCtrl setControllerType: FAVOURITE_EMOJI_ART];
            [self.navigationController pushViewController:artSectionViewCtrl animated:YES];
        }
        if (indexPath.row == 1) {
            [artSectionViewCtrl setArtArray:delegate.favTextArtArray];
            [artSectionViewCtrl setControllerType: FAVOURITE_TEXT_ART];
            [self.navigationController pushViewController:artSectionViewCtrl animated:YES];
        }
        if (indexPath.row == 2) {
            [picSectionViewCtrl setArrayImageName:delegate.favStaticPicArray];
            [picSectionViewCtrl setControllerType:FAVOURITE_STATIC_PIC];
            [self.navigationController pushViewController:picSectionViewCtrl animated:YES];
        }
        if (indexPath.row == 3) {
            [picSectionViewCtrl setArrayImageName:delegate.favAnimatePicArray];
            [picSectionViewCtrl setControllerType:FAVOURITE_ANIMATE_PIC];
            [self.navigationController pushViewController:picSectionViewCtrl animated:YES];
        }
    }
//    if (indexPath.section == 1) {
//        if (indexPath.row == 0) {
//            [artSectionViewCtrl setArtArray:delegate.savedArtArray];
//            [artSectionViewCtrl setControllerType:SAVED_ART];
//            [self.navigationController pushViewController:artSectionViewCtrl animated:YES];
//        }
//        if (indexPath.row == 1) {
//            [picSectionViewCtrl setArrayImageName:delegate.savedPicArray];
//            [picSectionViewCtrl setControllerType:SAVED_PIC];
//            [self.navigationController pushViewController:picSectionViewCtrl animated:YES];
//        }
//    }
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [artSectionViewCtrl setArtArray:delegate.recentArtArray];
            [artSectionViewCtrl setControllerType:RECENT_ART];
            [self.navigationController pushViewController:artSectionViewCtrl animated:YES];
        }
        if (indexPath.row == 1) {
            [picSectionViewCtrl setArrayImageName:delegate.recentPicArray];
            [picSectionViewCtrl setControllerType:RECENT_PIC];
            [self.navigationController pushViewController:picSectionViewCtrl animated:YES];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
