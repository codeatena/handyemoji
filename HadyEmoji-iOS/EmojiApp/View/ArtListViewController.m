//
//  ArtListViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ArtListViewController.h"
#import "ArtCategory.h"
#import "ArtSectionViewController.h"
#import "define.h"
#import "KeyBoardViewController.h"
#import "GADBannerView.h"
#import "AppDelegate.h"

@interface ArtListViewController ()

@end

@implementation ArtListViewController

@synthesize nControllerType;
@synthesize parentViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self loadArtEmojiList];
    [self loadArtTextList];
    [self loadArtEmojiContent];
    [self loadArtTextContent];
    
    UISegmentedControl *statFilter = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Emoji", @"Text", nil]];
//    [statFilter setSegmentedControlStyle:UISegmentedControlStyleBar];
    [statFilter setFrame: CGRectMake(0, 0, 160, 30)];
//    [statFilter sizeToFit];
    self.navigationItem.titleView = statFilter;
    
    [statFilter addTarget: self action: @selector(segmentedSwitch:) forControlEvents:UIControlEventValueChanged];
    
    bannerView_ = [[GADBannerView alloc]
                                   initWithFrame:CGRectMake(0.0, 466, GAD_SIZE_320x50.width,                                   GAD_SIZE_320x50.height)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
        if (nControllerType == KEYBOART_ART) {
            [bannerView_ setFrame:CGRectMake(0, 518, 320, 50)];
            [tblArtEmoji setFrame:CGRectMake(0, 65, 320, 453)];
            [tblArtText setFrame:CGRectMake(0, 65, 320, 453)];
        }
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
        if (nControllerType == KEYBOART_ART) {
            [bannerView_ setFrame:CGRectMake(0, 430, 320, 50)];
            [tblArtEmoji setFrame:CGRectMake(0, 65, 320, 453)];
            [tblArtText setFrame:CGRectMake(0, 65, 320, 453)];
        }
    }
    if (delegate.nCurrentDevice == IPAD) {
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
        if (nControllerType == KEYBOART_ART) {
            [bannerView_ setFrame:CGRectMake(10, 934, 748, 90)];
            [tblArtEmoji setFrame:CGRectMake(0, 65, 768, 867)];
            [tblArtText setFrame:CGRectMake(0, 65, 768, 867)];
        }
    }
    
    if (nControllerType == KEYBOART_ART) {
        UIBarButtonItem* btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target: self action:@selector(onCancel:)];
        self.navigationItem.leftBarButtonItem = btnCancel;
    }

    nArtType = ART_TYPE_EMOJI;
    [statFilter setSelectedSegmentIndex: 0];
    [tblArtText setHidden: YES];
}

- (void) onCancel:(id) sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void) segmentedSwitch: (id) sender
{
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    if (selectedSegment == 0) {
        [tblArtEmoji setHidden: NO];
        [tblArtText setHidden: YES];
        nArtType = ART_TYPE_EMOJI;
    }
    if (selectedSegment == 1) {
        [tblArtEmoji setHidden: YES];
        [tblArtText setHidden: NO];
        nArtType = ART_TYPE_TEXT;
    }
}

- (void) loadArtEmojiList
{
    arrArtEmoji = [[NSMutableArray alloc] init];
    
    ArtCategory* artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Greeting_Emo.jpg";
    artCategory.title = @"Greeting";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Celebration_Emo.jpg";
    artCategory.title = @"Celebration";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Life_Emo.jpg";
    artCategory.title = @"Life";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Mood_Emo.jpg";
    artCategory.title = @"Mood";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Love_Emo.jpg";
    artCategory.title = @"Love";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Gesture_Emo.jpg";
    artCategory.title = @"Gesture";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Food_Emo.jpg";
    artCategory.title = @"Food";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Weather_Emo.jpg";
    artCategory.title = @"Weather";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Sport_Emo.jpg";
    artCategory.title = @"Sport";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"School_Emo.jpg";
    artCategory.title = @"School";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Nature_Emo.jpg";
    artCategory.title = @"Nature";
    [arrArtEmoji addObject: artCategory];
    
//    artCategory = [[ArtCategory alloc] init];
//    artCategory.imageName = @"City_Emo.jpg";
//    artCategory.title = @"City";
//    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Fun_Emo.jpg";
    artCategory.title = @"Fun";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Girl_Emo.jpg";
    artCategory.title = @"Girl";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Symbol_Emo.jpg";
    artCategory.title = @"Symbol";
    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Word_Emo.jpg";
    artCategory.title = @"Word";
    [arrArtEmoji addObject: artCategory];
    
//    artCategory = [[ArtCategory alloc] init];
//    artCategory.imageName = @"Others_Emo.jpg";
//    artCategory.title = @"Other";
//    [arrArtEmoji addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Christmas_Emo.jpg";
    artCategory.title = @"Christmas";
    [arrArtEmoji addObject: artCategory];
}

- (void) loadArtTextList
{
    arrArtText = [[NSMutableArray alloc] init];
    
    ArtCategory* artCategory = [[ArtCategory alloc] init];
//    artCategory.imageName = @"Border_Txt.jpg";
//    artCategory.title = @"Border";
//    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Celebration_Txt.jpg";
    artCategory.title = @"Celebration";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Life_Txt.jpg";
    artCategory.title = @"Life";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Love_Txt.jpg";
    artCategory.title = @"Love";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Food_Txt.jpg";
    artCategory.title = @"Food";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Music_Txt.jpg";
    artCategory.title = @"Music";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"People_Txt.jpg";
    artCategory.title = @"People";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Sport_Txt.jpg";
    artCategory.title = @"Sport";
    [arrArtText addObject: artCategory];
    
//    artCategory = [[ArtCategory alloc] init];
//    artCategory.imageName = @"Rabbit_Txt.jpg";
//    artCategory.title = @"Rabbit";
//    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Animal_Txt.jpg";
    artCategory.title = @"Animal";
    [arrArtText addObject: artCategory];
    
//    artCategory = [[ArtCategory alloc] init];
//    artCategory.imageName = @"Vehicle_Txt.jpg";
//    artCategory.title = @"Vehicle";
//    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Weapon_Txt.jpg";
    artCategory.title = @"Weapon";
    [arrArtText addObject: artCategory];
    
    artCategory = [[ArtCategory alloc] init];
    artCategory.imageName = @"Halloween_Txt.jpg";
    artCategory.title = @"Halloween";
    [arrArtText addObject: artCategory];
    
//    artCategory = [[ArtCategory alloc] init];
//    artCategory.imageName = @"Christmas_Txt.jpg";
//    artCategory.title = @"Christmas";
//    [arrArtText addObject: artCategory];
}

- (void) loadArtEmojiContent
{
    NSMutableArray* arrGreeting = [[NSMutableArray alloc] initWithObjects:
                                   @".    sSs  \U0001f4a4\U0001f31f. *\n.  [ (-.-) ]        \U0001f319\n\u2014o-o\u2014*\n| \U0001f497\U0001f497  |.  \u2728*\n| \U0001f497\U0001f497  |*.\n\"Sweet Dreams..\"",
                                   
                                   @"\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\n\U0001f339\U0001f637\U0001f622\U0001f613\U0001f637\U0001f622\U0001f4a8\U0001f339\n\U0001f339\U0001f49d\U0001f489\U0001f375\U0001f48a\U0001f490\U0001f49d\U0001f339\n\U0001f339  GetBetter Soon!  \U0001f339\n\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339",
                                   
                                   @"\U0001f601\U0001f601\U0001f601\U0001f601\U0001f601\U0001f601\U0001f601\n\u2764 Get well soon!\u2764\n+\U0001f637~>\U0001f645/\U0001f604~>\U0001f646+\n\U0001f497U neeeed \U0001f489\U0001f48a\U0001f497\n\U0001f499&\U0001f35a\U0001f354\U0001f375\U0001f35e\U0001f373\U0001f499\n+\u2600TAKE CARE\u2600+\n\U0001f601\U0001f601\U0001f601\U0001f601\U0001f601\U0001f601\U0001f601",
                                   
                                   @"\U0001f4f1\U0001f4f1\U0001f4f1\U0001f4f1\U0001f4f1\U0001f4f1\U0001f4f1\U0001f4f1\n\U0001f4e9\U0001f4e9\u260E \u260E \U0001f4e9 \U0001f4e9 \U0001f4e9 \U0001f4e9\n\U0001f4e9\u260E \U0001f4e9 \U0001f4e9 \u260E \U0001f4e9 \U0001f4e9 \U0001f4e9\n\U0001f4e9\U0001f4e9\U0001f4e9\U0001f4e9\u260E \U0001f4e9 \U0001f4e9 \U0001f4e9\n\U0001f4e9\U0001f4e9\U0001f4e9\u260E \U0001f4e9 \U0001f4e9 \U0001f4e9 \U0001f4e9\n\U0001f4e9\U0001f4e9\u260E \U0001f4e9How\'s it \U0001f4e9\n\U0001f4e9\U0001f4e9\u260E \U0001f4e9 \U0001f4e9going\u2754\n\U0001f4e9\U0001f4e9\U0001f4e9\U0001f4e9\U0001f4e9\U0001f4e9\U0001f4e9\U0001f4e9\n\U0001f4e9\U0001f4e9\u260E \U0001f4e9 \U0001f4e9 \U0001f618 \u2764 \U0001f4e9\n\U0001f4eb\U0001f4eb\U0001f4eb\U0001f4eb\U0001f4eb\U0001f4eb\U0001f4eb\U0001f4eb",
                                   
                                   @"\uE418{ morning Beautiful !\n\uE049 \uE049 \uE049 \uE049 \uE049 \uE04A \uE049 \uE04A\n\uE049 \uE048 \uE048 \uE049 \uE048 \uE048 \uE04A \uE049\n\uE048 \uE048 \uE048 \uE048 \uE048 \uE04A \uE048 \uE04A\n\uE048 \uE048 \uE048 \uE048 \uE04A \uE048 \uE048 \uE049\n\uE048 \uE048 \uE048 \uE048 \uE048 \uE048 \uE048 \uE049\n\uE049 \uE048 \uE048 \uE048 \uE048 \uE048 \uE049 \uE049\n\uE049 \uE04A \uE048 \uE048 \uE048 \uE049 \uE049 \uE326\n\uE04A \uE049 \uE049 \uE048 \uE049 \uE049 \uE049 \uE443\n\uE32D \uE32A \uE32D \uE32A \uE32D \uE32A \uE32D \uE32A\nHave fun at the SNOW \uE405",
                                   
                                   @"\U0001f31f\u3002*\u3002 \U0001f497~\U0001f603xXx\n\u3002 \U0001f49a \u3002*\u3002Have\u3002 \U0001f31f\n\U0001f31f\U0003002a wonderful day.\n.\U0001f49c\u00B4*\u3002.\U0001f31f\u00A8 \u00AF`*\u2764 \u3002 \u3002 \U0001f31f",
                                   
                                   @"\U0001f499\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\n\U0001f499\u2764 \u2764 \u2764 \u2764 \u2764 \U0001f49b\n\U0001f499\u2764HELLO \u266A \u2764 \U0001f49b\n\U0001f499\u2764 \U0001f44d \u2600 \U0001f44e \u2764 \U0001f49b\n\U0001f499\u2764 \u262E O\u02E5 \u02E5 \u018EH\u2764 \U0001f49b\n\U0001f499\u2764 \u2764 \u2764 \u2764 \u2764 \U0001f49b\n\U0001f49a\U0001f49a\U0001f49a\U0001f49a\U0001f49a\U0001f49a\U0001f49b",
                                   
                                   @"\U0001f60aGOOD MORNING\U0001f60a\n\U0001f31f\u2601 \U0001f31f \u2601 \u2601 \U0001f31f \u2601 \U0001f31f\n\u2601 \U0001f31f \u2601 \U0001f31f \U0001f31f \u2601 \U0001f31f \u2601\n\U0001f31f\u2601 \U0001f31f \u2600 \u2600 \U0001f31f \u2601 \U0001f31f\n\u2601 \U0001f31f \u2600 \U0001f49b \U0001f49b \u2600 \U0001f31f \u2601\n\u2601 \U0001f31f \u2600 \U0001f49b \U0001f49b \u2600 \U0001f31f \u2601\n\U0001f31f\u2601 \U0001f31f \u2600 \u2600 \U0001f31f \u2601 \U0001f31f\n\u2601 \U0001f31f \u2601 \U0001f31f \U0001f31f \u2601 \U0001f31f \u2601\n\U0001f31f\u2601 \U0001f31f \u2601 \u2601 \U0001f31f \u2601 \U0001f31f\n\U0001f60aMY\U0001f33bSUNSHINE\U0001f60a",
                                   
                                   @"\"Happy\u2728 \U0001f3b6\U0001f31f\nFriday Night\U0001f389\"\n\U0001f378\U0001f534\U0001f533\U0001f37b  \U0001f37a\U0001f532\n\\/()/\\()\\/~~~~ \\\\{}\n/\\  /\\.            /\\",
                                   
                                   @"\U0001f618{ Gooood morning! )\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2600 \u2601 \u2600\n\u2601 \u2764 \u2764 \u2601 \u2764 \u2764 \u2600 \u2601\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2600 \u2764 \u2600\n\u2764 \u2764 \u2764 \u2764 \u2600 \u2764 \u2764 \u2601\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2601\n\u2601 \u2764 \u2764 \u2764 \u2764 \u2764 \u2601 \u2601\n\u2601 \u2600 \u2764 \u2764 \u2764 \u2601 \u2601 \U0001f3b6\n\u2600 \u2601 \u2601 \u2764 \u2601 \u2601 \u2601 \U0001f42c\n\U0001f334\U0001f334\U0001f334\U0001f334\U0001f334\U0001f334\U0001f334\U0001f334\n( Have a nice day \u266A }\U0001f609",
                                   
                                   @"\U0001f31f\uFF3F \uFF3F \uFF3F \uFF3F \uFF3F \uFF3F \uFF3F \uFF3F \uFF3F\n(ThinkingAboutYou..\u2764)\n`  \uFFE3 \uFFE3 \U0001f42c \uFFE3O \uFFE3.\uFFE3 \uFFE3 \uFFE3\n\U0001f433    \u3002    o        \U0001f41f  \uFF0A\n\u256D \u2501 \u2501 \u2501 \u2501 \u2501 \u256E \u250F \u256E \u256D \u2513\n\u2503 \u2570 \u256F            \u2503 \u2570 \u256E \u256D \u256F\n\u2523 \u2501 \u2501 \u256F        \u2570 \u2501 \u256F \u2503\n\u2570 \u2501 \u2501 \u2501 \u2501 \u2501 \u2501 \u2501 \u2501 \u256F\n\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f420\U0001f300\U0001f300\n\U0001f499\U0001f419\U0001f499\U0001f499\U0001f499\U0001f499\U0001f499\U0001f499",
                                   
                                   @"*    \U0001f340      \u2600\n*\U0001f340\U0001f340  *    *\n\U0001f340\U0001f340\U0001f340    **\n\U0001f340\U0001f340\U0001f340\U0001f340\n\U0001f381 \"Have a\n*  Nice weekend\"",
                                   
                                   @"\uE32E \uE417 \uE142~~~~~~~~\uE009\n\u250F \u2501 \u2501 \u2533 \u2501 \u2501 \u2533 \u2501 \u2501 \u2513\n\u2503 \uE21C  \u2503 \uE21D \u2503  \uE21E \u2503\n\u2523 \u2501 \u2501 \u254B \u2501 \u2501 \u254B \u2501 \u2501 \u252B\n\u2503 \uE21F  \u2503 \uE220 \u2503  \uE221 \u2503\n\u2523 \u2501 \u2501 \u254B \u2501 \u2501 \u254B \u2501 \u2501 \u252B\n\u2503 \uE222  \u2503 \uE223 \u2503  \uE224 \u2503\n\u2523 \u2501 \u2501 \u254B \u2501 \u2501 \u254B \u2501 \u2501 \u252B\n\u2503 \uE204  \u2503 \uE225 \u2503  \uE210 \u2503\n\u2517 \u2501 \u2501 \u253B \u2501 \u2501 \u253B \u2501 \u2501 \u251B\n\uE022*RING MY BELL*\uE022",
                                   
                                   @"\u2601 \U0001f44b\U0001f680 \u2601 \u2601\n      \u2728  BYEBYE*\n\u2601 \u2728              \U0001f388\n  \u2728    \u2601\n\u2728\n\u2728\n\U0001f33e\u2728 \U0001f4a8  \U0001f3c3 \U0001f3e0\U0001f3e2\n\uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3",
                                   
                                   @"\u2600 \u2600 \u2600 \u2600 \u2600 \U0001f380 \u2600 \u2600\n\u2600 \u2601 \u2600 \u2600 \U0001f380 \U0001f380 \U0001f380 \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \U0001f380 \U0001f380\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \U0001f380\n\u2601 \u2601 \U0001f393 \u2601 \u2601 \U0001f393 \u2601 \u2601\n\U0001f4fc\U0001f4fc\u2601 \U0001f497 \u2601 \u2601 \U0001f4fc \U0001f4fc\n\U0001f4fc\U0001f4fc\u2601 \u2601 \u2601 \u2601 \U0001f4fc \U0001f4fc\n\u2600 \u2600 \u2600 \u2600 \u2600 \u2600 \u2600 \u2600\n\U0001f49d\U0001f49d\u24BD \u24BA \u24C1 \u24C1 \u24C4 \U0001f49d \U0001f49d\n\U0001f493\U0001f493\u24C0 \u24BE \u24C9 \u24C9 \u24CE \U0001f493 \U0001f493",
                                   
                                   @".\u2600/\\ \"Good\nafternoon\"/\\\U0001f334    /\\\n. /\U0001f42b    \\  /    \\\U0001f334/  \\\n/**  *      /*  ** \\\U0001f42b\U0001f42b",
                                   
                                   @"\U0001f60c{ It\'s time to sleep! )\n\u2601 \u2601 \u2601 \u2601 \u2601 \U0001f31f \u2601 \U0001f31f\n\u2601 \U0001f49c \U0001f49c \u2601 \U0001f49c \U0001f49c \U0001f31f \u2601\n\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f31f\U0001f49c\U0001f31f\n\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f31f\U0001f49c\U0001f49c\u2601\n\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\u2601\n\u2601 \U0001f49c \U0001f49c \U0001f49c \U0001f49c \U0001f49c \u2601 \u2601\n\u2601 \U0001f31f \U0001f49c \U0001f49c \U0001f49c \u2601 \u2601 \U0001f319\n\U0001f31f\u2601 \u2601 \U0001f49c \u2601 \u2601 \u2601 \U0001f411\n\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\n( Goooood night \u266B }\U0001f62a",
                                   
                                   @"\uE107 \uE107 \uE136 \uE136 \uE136 \uE107 \uE107\n\uE110 \uE110SPEEDY\uE110 \uE110\n  \uE030RECOVERY\uE030\nON YOUR INJURY\n\uE056 \uE056 \uE056 \uE056 \uE056 \uE056 \uE056",
                                   
                                   @"\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n--------\U0001f3a9--\u2728----\n\u2728----( \U0001f440 )--------\n--oo0--\U0001f443-0oo----\n\u24CC \u24BD \u24B6 \u24C9\'\u24C8_\u24CA \u24C5 \u2754\n\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f",
                                   
                                   @"\uE00A \uE00A \uE00A \uE00A \uE00A \uE00A \uE00A \uE00A\n\uE103 \uE103 \uE009 \uE009 \uE103 \uE103 \uE103 \uE103\n\uE103 \uE009 \uE103 \uE103 \uE009 \uE103 \uE103 \uE103\n\uE103 \uE103 \uE103 \uE103 \uE009 \uE103 \uE103 \uE103\n\uE103 \uE103 \uE103 \uE009 \uE103 \uE103 \uE103 \uE103\n\uE103 \uE103 \uE009 \uE103where  r \uE103\n\uE103 \uE103 \uE009 \uE103 \uE103u now\uE336\n\uE103 \uE103 \uE103 \uE103 \uE103 \uE103 \uE103 \uE103\n\uE103 \uE103 \uE009 \uE103 \uE103 \uE418 \uE022 \uE103\n\uE101 \uE101 \uE101 \uE101 \uE101 \uE101 \uE101 \uE101",
                                   
                                   @"\U0001f6a4 How are you\u2754 \u26F5\n~ ~~~ ~~ \U0001f42c~~~~~*~~\n*\U0001f300\U0001f300\U0001f300    \U0001f42c      \U0001f433\n\U0001f420\U0001f420 \U0001f41f**      \U0001f300\U0001f300\U0001f300\n*  \U0001f420  \U0001f300\U0001f300\U0001f4a6      \U0001f419*",
                                   
                                   @"\u25AC \u25AC \u25AC \u25AC \u25AC \u25AC \u25BA \uE110\nDON\'T WORRY,\n\uE022 \U000e022bE\uE022 \uE022\n\u2557 \u2554 \u2554 \u2557 \u2554 \u2557 \u2554 \u2557 \u2557 \u2554\n\u2560 \u2563 \u2560 \u2563 \u2560 \u255D \u2560 \u255D \u255A \u2563\n\u255D \u255A \u255D \u255A \u255D  \u2569    \u2569 \uE337\n\uE058 \u2794 \uE333 \uE057 \u2794 \uE332\n\uE030 \u25C4 \u25AC \u25AC \u25AC \u25AC \u25AC \u25AC",
                                   
                                   @"\uE40D \u24CE \u24C4 \u24CA \u24B6 \u24C7 \u24BA \u24C8 \u24C4 \uE40D\n\uE123    \uE123  \uE123  \uE123 \uE123 \uE123\n\uE123    \uE123 \uE123  \uE123  \uE123\n\uE123 \uE123 \uE123 \uE123  \uE123  \uE123\n\uE123    \uE123 \uE123  \uE123    \uE123\n\uE123    \uE123  \uE123        \uE123\n\uE51FHOT\uE51F \uE51F \uE51F \uE51F \uE51F\n\uE11D \uE11D \uE11D \uE11D \uE11DHOT\uE11D\n\uE429 \uE429HOT\uE429 \uE429 \uE429 \uE429",
                                   
                                   @"\U0001f49a~\U0001f340\U0001f340\U0001f340\U0001f340\U0001f340\n\U0001f340\u2554 \u2557 \u2554 \u2557 \u2554 \u2557 \u2566 \u2557 \u2728 \U0001f340\n\U0001f340\u2551 \u2566 \u2551 \u2551 \u2551 \u2551 \u2551 \u2551 \U0001f44d \U0001f340\n\U0001f340\u255A \u255D \u255A \u255D \u255A \u255D \u2569 \u255D \u3002 \U0001f340\n\U0001f340\u30FB \u30FB \u24C1 \u24CA \u24B8 \u24C0 \U0001f340\n\U0001f340\U0001f340\U0001f340 to you\U0001f49a",
                                   
                                   @"\uE32C \uE32C \uE32C \uE32C \uE32C \uE32C\n\uE00A \u2554 \u2557 \u2554 \u2557 \u2566  \u2566**\uE00A\n\uE009 \u2551  \u2560 \u2563 \u2551  \u2551**\uE009\n\uE326 \u255A \u255D \u255D \u255A \u255A \u255D \u255A \u255D \uE326\n=\uE057 \uE32EME\uE32E \uE057=\n\uE028RIGHT NOW\uE025\n\uE32C \uE32C \uE32C \uE32C \uE32C \uE32C",
                                   
                                   @"\uE416 \uE416 \uE416 \uE416 \uE416 \uE416 \uE416 \uE416 \uE416\n\uE416 \uE059 \uE059 \uE059 \uE059 \uE059 \uE059 \uE059 \uE416\n\uE416 \uE059 \uE421 \uE421 \uE421 \uE421 \uE421 \uE059 \uE416\n\uE416 \uE059 \uE421  FUCK  \uE421 \uE059 \uE416\n\uE416 \uE059 \uE421  YOU!!  \uE421 \uE059 \uE416\n\uE416 \uE059 \uE421 \uE421 \uE421 \uE421 \uE421 \uE059 \uE416\n\uE416 \uE059 \uE059 \uE059 \uE059 \uE059 \uE059 \uE059 \uE416\n\uE416 \uE416 \uE416 \uE416 \uE416 \uE416 \uE416 \uE416 \uE416",
                                   
                                   @"\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f48c\n\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f48c\U0001f48c\U0001f48c\U0001f48c\U0001f48c\U0001f48c\n\U0001f497\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f497\n\U0001f497\U0001f47e\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\n\U0001f497\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f497\U0001f497\n\U0001f497\U0001f47e\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\n\U0001f497\U0001f47e\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\n\U0001f497\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f47e\U0001f497\n\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\n\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\n\U0001f48c\U0001f48c\U0001f47e\U0001f47e\U0001f47e\U0001f48c\U0001f48c\n\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\n\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\n\U0001f48c\U0001f48c\U0001f48c\U0001f47e\U0001f48c\U0001f48c\U0001f48c\n\U0001f48c\U0001f48c\U0001f48c\U0001f48c\U0001f48c\U0001f48c\U0001f48c",
                                   
                                   @"\u2601 \u2601 \U0001f31f \u2601 \u2601 \u2601 \u2601 \u2601\n\u2B50 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2728\n\u2601 \u2601 \U0001f31d \U0001f31d \U0001f31d \u2601 \u2601 \u2601\n\u2601 \U0001f31d \U0001f31d \u2601 \u2601 \U0001f31d \u2601 \u2601\n\U0001f31d\U0001f31d\u2601 \u2728 \u2601 \u2601 \u2601 \u2601\n\U0001f31d\U0001f31d\u2601 \u2601 \u2601 \u2601 \u2601 \U0001f31f\n\U0001f31d\U0001f31d\u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f31d \U0001f31d \u2601 \u2601 \U0001f31d \u2601 \u2601\n\u2601 \u2601 \U0001f31d \U0001f31d \U0001f31d \u2601 \u2601 \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f31fGood night! \u2728 \u2601",
                                   
                                   @"\U0001f48e\u2764 \u2764 \U0001f48e \u2764 \u2764 \U0001f48e\n\U0001f48e\u2764 \u2764 \U0001f48e \u2764 \u2764 \U0001f48e\n\U0001f48e\u2764 \u2764 \u2764 \u2764 \u2764 \U0001f48e\n\U0001f48e\u2764 \u2764 \U0001f48e \u2764 \u2764 \U0001f48e\n\U0001f48e\u2764 \u2764 \U0001f48e \u2764 \u2764 \U0001f48e\n\U0001f48e\U0001f48e\U0001f48e\U0001f48e\U0001f48e\U0001f48e\U0001f48e\n\U0001f48e\u2764 \u2764 \u2764 \u2764 \u2764 \U0001f48e\n\U0001f48e\U0001f48e\U0001f48e\u2764 \U0001f48e \U0001f48e \U0001f48e\n\U0001f48e\U0001f48e\U0001f48e\u2764 \U0001f48e \U0001f48e \U0001f48e\n\U0001f48e\U0001f48e\U0001f48e\u2764 \U0001f48e \U0001f48e \U0001f48e\n\U0001f48e\u2764 \u2764 \u2764 \u2764 \u2764 \U0001f48e",
                                   
                                   @"\U0001f47e\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\U0001f47e\U0001f4bf\n\U0001f47e\U0001f4bf\U0001f47e\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\n\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\n\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\n\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f47e\n\U0001f4bf\U0001f47e\U0001f4bf\U0001f4bf\U0001f4bf\U0001f47e\U0001f47e\U0001f4bf",
                                   
                                   @"\U0001f466          \U0001f469\n\U0001f467          \U0001f474\n\U0001f468\U0001f469\U0001f476\U0001f478\n\U0001f473          \U0001f46e\n\U0001f476          \U0001f472",
                                   
                                   @"\U0001f46e\U0001f477\U0001f471\U0001f478\n\U0001f467\n\U0001f472\U0001f473\U0001f468\U0001f469\n\U0001f474\n\U0001f471\U0001f475\U0001f468\U0001f467",
                                   
                                   @"\U0001f478\n\U0001f466\n\U0001f469\n\U0001f471\n\U0001f476\U0001f475\U0001f468\U0001f466",
                                   
                                   @"\U0001f476\n\U0001f474\n\U0001f475\n\U0001f468\n\U0001f469\U0001f478\U0001f471\U0001f476", 
                                   
                                   @"     \U0001f469\U0001f467\n\U0001f466          \U0001f475\n\U0001f482          \U0001f46e\n\U0001f477          \U0001f472\n     \U0001f471\U0001f473", 
                                   
                                   @"\U0001f365\U0001f352\U0001f352\U0001f365\U0001f352\U0001f352\U0001f365\n\U0001f352\U0001f368\U0001f368\U0001f352\U0001f368\U0001f368\U0001f352\n\U0001f352\U0001f368\U0001f368\U0001f368\U0001f368\U0001f368\U0001f352\n\U0001f352    Miss you!    \U0001f352\n\U0001f365\U0001f352\U0001f368\U0001f368\U0001f368\U0001f352\U0001f365\n\U0001f365\U0001f365\U0001f352\U0001f368\U0001f352\U0001f365\U0001f365\n\U0001f365\U0001f365\U0001f365\U0001f352\U0001f365\U0001f365\U0001f365", 
                                   
                                   @"\U0001f489\U0001f489\U0001f489\U0001f489\U0001f489\U0001f489\U0001f489\n\U0001f489\u2764 \u2764 \U0001f489 \u2764 \u2764 \U0001f489\n\u2764          \u2764          \u2764\n\u2764    Get better!    \u2764\n\U0001f489\u2764               \u2764 \U0001f489\n\U0001f489\U0001f489\u2764     \u2764 \U0001f489 \U0001f489\n\U0001f489\U0001f489\U0001f489\u2764 \U0001f489 \U0001f489 \U0001f489\n\U0001f489\U0001f489\U0001f489\U0001f489\U0001f489\U0001f489\U0001f489", 
                                   
                                   @"\U0001f337\U0001f337\U0001f60a\U0001f60b\U0001f337\U0001f337\n\U0001f367   Cheer up   \U0001f353\n\U0001f33f\U0001f3b1\u2728 \u2728 \U0001f3b1 \U0001f33f\n\U0001f33f\u2728 \u2728 \u2728 \u2728 \U0001f33f\n\U0001f33f\U0001f34e\u2728 \u2728 \U0001f34e \U0001f33f\n\U0001f33f\u2728 \U0001f34e \U0001f34e \u2728 \U0001f33f\n\U0001f347    For you    \U0001f352", 
                                   
                                   @"\uE12F \uE12F \uE035 \uE12F \uE12F\n\uE444 \uE444 \uE536 \uE444 \uE444\n\uE444 \uE536 \uE536 \uE444 \uE444\n\uE444 \uE536 \uE444 \uE444 \uE444\n\uE444 \uE536 \uE536 \uE536 \uE444\n\uE444 \uE444 \uE444 \uE536 \uE444\n\uE444 \uE444 \uE444 \uE536 \uE444\nFOLLOW ME", 
                                   
                                   @"      \U0001f4ad\U0001f4ad\U0001f4ad\n   \U0001f4ad  \U0001f466   \U0001f4ad\n   \U0001f4ad   \U0001f4ad\U0001f4ad\n      \U0001f4ad\n\U0001f469\U0001f381\n\U0001f699\U0001f4a8", 
                                   
                                   @"\uE001 \uE514 \uE005 \uE335 \uE335 \uE510 \uE448 \uE518\n\uE512 \uE515 \uE335 \uE326 \uE503 \uE335 \uE517 \uE50E\n\uE50B \uE50C \uE335 \uE41E \uE415 \uE335 \uE51A \uE516\n\uE004 \uE152 \uE50F \uE335 \uE335 \uE51C \uE511 \uE50D\n\uE337HELLO\uE022WORLD\uE337",

                                   nil];
    
    NSMutableArray* arrCelebration = [[NSMutableArray alloc] initWithObjects:
                                      @"     \U0001f525     \U0001f525     \U0001f525\n     \U0001f49f     \U0001f49f     \U0001f49f\n     \U0001f49f     \U0001f49f     \U0001f49f\n\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\n\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\n\U0001f370\U0001f370\U0001f370\U0001f370\U0001f370\U0001f370\U0001f370\n\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\U0001f36f\n\U0001f370\U0001f370\U0001f370\U0001f370\U0001f370\U0001f370\U0001f370\nHappy\U0001f493\U0001f493birthday!\n\U0001f389\U0001f389\U0001f389\U0001f389\U0001f389\U0001f389\U0001f389",
                                      
                                      @"\uE219Happy Easter\uE219\n\uE32E.-.********.-.***\n***/\uE333\\*\uE32E/\uE32D\\**\n**|\uE333 \uE333|*|\uE32B \uE32C|*\n***\\ \uE333/****\\\uE328/\uE32E\n\uE32E*\uFFE3*******\uFFE3****",
                                      
                                      @"\uE44B \uE44B \uE44B \uE219 \uE219 \uE219 \uE219 \uE219\n\uE44B \uE44B \uE44B \uE014 \uE014 \uE014 \uE014 \uE014\n\uE44B \uE44B \uE44B \uE219 \uE219 \uE219 \uE219 \uE219\n\uE014 \uE014 \uE014 \uE014 \uE014 \uE014 \uE014 \uE014\n\uE219 \uE219 \uE219 \uE219 \uE219 \uE219 \uE219 \uE219\n\uE014 \uE014 \uE014 \uE014 \uE014 \uE014 \uE014 \uE014\n~\"\uE022 \u3002*HAPPY*\u3002 \uE022\"~\n~\'\U000e50cfourth of July\uE50C\'~",
                                      
                                      @"\uE04C            \uE308            \uE04C\n    \uE445 \uE445 \uE445 \uE445 \uE445 \uE445\n\uE445 \uE445 \uE503 \uE445 \uE445 \uE503 \uE445 \uE445\n\uE445 \uE445 \uE503 \uE445 \uE445 \uE503 \uE445 \uE445\n\uE445 \uE035 \uE445 \uE445 \uE445 \uE445 \uE035 \uE445\n\uE445 \uE445 \uE035 \uE035 \uE035 \uE035 \uE445 \uE445\n    \uE445 \uE445 \uE445 \uE445 \uE445 \uE445\n\'HappyHalloweenDay\'\n\uE04C            \uE11B            \uE04C",
                                      
                                      @"    \uE327 \uE327    \uE327 \uE327\n\uE327 \uE327 \uE327 \uE327 \uE327 \uE327 \uE327\n\uE327 \uE327 \uE327 \uE327 \uE327 \uE327 \uE327\n\uE327 \uE327 \uE327 \uE327 \uE327 \uE327 \uE327\n    \uE327 \uE327 \uE327 \uE327 \uE327\n          \uE327 \uE327 \uE327\n              \uE327",
                                      
                                      @"    Happy Valentine Day\n                  \uE428\n\uE32E \uE329 \uE329 \uE329 \uE329 \uE329 \uE329 \uE32E",
                                      
                                      @"\uE327 \uE327 \uE327 \uE327 \uE327 \uE327 \uE327\n~\uE106 \uE333 \uE332 \uE333 \uE332 \uE106~\n\uE030 \u2557 \u2554 \u2554 \u2557 \u2554 \u2557 \u2554 \u2557 \u2557 \u2554 \uE030\n\uE030 \u2560 \u2563 \u2560 \u2563 \u2560 \u255D \u2560 \u255D \u255A \u2563 \uE030\n\uE030 \u255D \u255A \u255D \u255A \u255D+\u2569 \uE326 \u2569 \uE030\n*VALENTINE\'s DAY*\n\uE329 \uE329 \uE329 \uE329 \uE329 \uE329 \uE329",
                                      
                                      @".    \uE110.          \uE110.      \uE110\n  \uE110 \uE110 \uE303 \uE110 \uE110 \uE303 \uE110 \uE110\n    \uE110          \uE110.      \uE110\n  \uE110 \uE110 \uE303 \uE110 \uE110 \uE303 \uE110 \uE110\nHAPPY ST. PATRICK\'S DAY\n  \uE110 \uE110 \uE303 \uE110 \uE110 \uE303 \uE110 \uE110\n    \uE110.        \uE110.        \uE110\n  \uE110 \uE110 \uE303 \uE110 \uE110 \uE303 \uE110 \uE110\n    \uE110.        \uE110.        \uE110",
                                      
                                      @"      , , , , , , ,\n      | | | | | | |\n{\U0001f382_\u2022_\U0001f382_\u2022_\U0001f382}\n{\U0001f382_\u2022_\U0001f382_\u2022_\U0001f382}\n{\U0001f382_\u2022_\U0001f382_\u2022_\U0001f382}",
                                      
                                      @"\uE04A \uE04A \uE04A \uE04A \uE04A \uE04A \uE04A\n\uE04A \uE032 \uE032 \uE04A \uE032 \uE032 \uE04A\n\uE032 \uE04A \uE04A \uE032 \uE04A \uE04A \uE032\n\uE032MISS \uE04A YOU\uE032\n\uE04A \uE032 \uE04A \uE04A \uE04A \uE032 \uE04A\n\uE04A \uE04A \uE032 \uE04A \uE032 \uE04A \uE04A\n\uE04A \uE04A \uE04A \uE032 \uE04A \uE04A \uE04A\n\uE110 \uE110 HAPPY \uE110 \uE110\nVALENTINE\'S DAY\n\uE04C \uE04C \uE04C \uE032 \uE04C \uE04C \uE04C\n\uE04C \uE04C \uE032 \uE04C \uE032 \uE04C \uE04C\n\uE04C \uE032 \uE04C \uE04C \uE04C \uE032 \uE04C\n\uE032LOVE \uE04C YOU\uE032\n\uE032 \uE04C \uE04C \uE032 \uE04C \uE04C \uE032\n\uE04C \uE032 \uE032 \uE04C \uE032 \uE032 \uE04C\n\uE04C \uE04C \uE04C \uE04C \uE04C \uE04C \uE04C",
                                      
                                      @"  \u2728 \U0001f339 \U0001f339    \U0001f339\U0001f339\u2728\n\U0001f339\U0001f381\U0001f46b\U0001f339\U0001f46b\U0001f381\U0001f339\n  \U0001f339\U0001f382\U0001f484\U0001f48f\U0001f484\U0001f382\U0001f339\n  \U0001f339\U0001f48b\U0001f48b  \U0001f48b\U0001f48b\U0001f339\n\u2728 \U0001f339\U0001f48e\U0001f48d\U0001f48e\U0001f339 \u2728\n    \u2728 \U0001f339 \U0001f49d \U0001f339  \u2728\n      \u2728  \U0001f339 \u2728\n\u00A7 \U0001f49b \u2764 For You \u2764 \U0001f49b \u00A7\n\u00A7        \U0001f497\U0001f49d\U0001f49d\U0001f497      \u00A7\n\u00A7 \U0001f380Valentine\'s Day\U0001f380\u00A7",
                                      
                                      @"\uE32D_|****|_\uE022 \uE32D \uE022 \uE32D \uE022\n\uE022 ( \' v \' ) \uE32D \uE022 \uE32D \uE022 \uE32D\n****** \u00A5 ******************\n||.    \"I love U, Dad\uE418.\"||\n|| Happy Father\'s Day! ||\n****************************",
                                      
                                      @"\uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044\n\uE044 \uE032 \uE032 \uE044 \uE032 \uE032 \uE044\n\uE032 \uE032 \uE032 \uE032 \uE032 \uE032 \uE032\n\uE032 \uE032 \uE032 \uE032 \uE032 \uE032 \uE032\n\uE032 \uE032 \uE032 \uE032 \uE032 \uE032 \uE032\n\uE044 \uE032 \uE032 \uE032 \uE032 \uE032 \uE044\n\uE044 \uE044 \uE032 \uE032 \uE032 \uE044 \uE044\n\uE044 \uE044 \uE044 \uE032 \uE044 \uE044 \uE044\n*HappyValentineDay*\n\uE044 \uE112 \uE056 \uE022 \uE414 \uE112 \uE044\n\uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044",
                                      
                                      @"\uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B\n\uE34B \uE303 \uE346 \uE345 \uE347 \uE110 \uE303 \uE447 \uE022 \uE34B\n\uE34B \uE046 \uE046 \uE347 \uE347 \uE347 \uE347 \uE046 \uE046 \uE34B\n\uE34B \uE046 \uE046 H A P P Y \uE046 \uE046 \uE34B\n\uE34B \uE345 \uE346 \uE347 \uE348 \uE346 \uE345 \uE347 \uE348 \uE34B\n\uE34B \uE314 B I R T H D A Y \uE314 \uE34B\n\uE34B \uE303 \uE346 \uE348 \uE345 \uE110 \uE303 \uE447 \uE303 \uE34B\n\uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B \uE34B",
                                      
                                      @"\uE325 \uE325 \uE325 \uE325 \uE325 \uE325 \uE325\n\uE325 \uE032 \uE032 \uE325 \uE032 \uE032 \uE325\n\uE032*(\\/)~\uE32E~(\\/)*\uE032\n\uE032( ^.^)\uE110(\uE419 )\uE032\n\U000e032c(\')(\')    c(\')(\')\uE032\n\uE325 \uE032 \uE032 \uE032 \uE032 \uE032 \uE325\n\uE325 \uE325 \uE032 \uE032 \uE032 \uE325 \uE325\n\uE112 \uE325 \uE325 \uE032 \uE325 \uE325 \uE112\nHappyValentineDay\n\uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304",
                                      
                                      @"\u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022\n\uE032    \uE306    mm    \uE303      \uE032\n\uE032        \\\\[ \' v \' ] //        \uE032\n\uE032  \"Thanks,  Mom!!\"  \uE032\n\uE032Happy Mother\'s Day\uE032\n\u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022 \u2022",
                                      
                                      @"\uE04C \uE049 \uE049 \uE049 \uE049^^\uE049\n\uE049 \U000e049bOO\uE337 \uE049 \uE049\n\uE049 \uE049 \uE11B \uE11B \uE11B \uE049 \uE049\n^^\uE049 \uE049 \u271D \uE049 \uE049 \uE049\n\uE049 \uE049/\uFFE3 \uFFE3 \uFFE3 \uFFE3\\\uE049\n\uE049 \uE049|  R. I. P  |\uE049\n\uE049 \uE049|\u30FC \u30FC*\u30FC \u30FC|\uE049\n\uE445 \uE445 \uE11C \uE11C \uE11C \uE445 \uE445\n+Happy halloween+", 
                                      
                                      @"\uE32E /)/)          i i  i i *\n  (  .  .)*      |\uE335 \uE335|\n  ( \u3064\"\uE022 | \uE32C \uE32C \uE32C|\nHAPPYBIRTHDAY\uE326\n\U000e329aNDILOVEYOU\uE32E", 
                                      
                                      @"\uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044\n\uE044 \uE032 \uE032 \uE044 \uE032 \uE032 \uE044\n\uE030 \uE030 \uE030 \uE030 \uE030 \uE030 \uE030\n\uE032 \uE032 \uE032 \uE032 \uE032 \uE032 \uE032\n\uE030 \uE030 \uE030 \uE030 \uE030 \uE030 \uE030\n\uE044 \uE032 \uE032 \uE032 \uE032 \uE032 \uE044\n\uE044 \uE044 \uE030 \uE030 \uE030 \uE044 \uE044\n\uE044 \uE044 \uE044 \uE032 \uE044 \uE044 \uE044\n\uE32B \uE32B HAPPY\uE32B \uE32B\n\uE32BVALENTINE\'S\uE32B\n\uE32B \uE32B \U000e32bdAY!\uE32B \uE32B",
                                                                        nil];
    
    NSMutableArray* arrLife = [[NSMutableArray alloc] initWithObjects:
                               @"\u2728 \"home \U0001f603  \u2764\n\u2764  \U0001f61a sweet home\"\u2728\n    \u2728      \u2665          \u2764\n  \u2764      \U0001f49b  \U0001f49b    \u2728\n\U0001f3e0\U0001f3e0 \U0001f499 \U0001f3e0 \U0001f499  \U0001f3c3~\U0001f3b6\n\uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3",
                               
                               @"  /\\\U0001f6a5\U0001f6a5\U0001f6a5\\\n/\u3000\\~~\U0001f386~~~\\\n/ \U0001f531 \\___\U0001f385  __\\\n\u258E \u2728 \u258E \U0001f303  \U0001f391  \u258E\n\u258E \U0001f699 \u258E \U0001f490    \U0001f338 \u258E\n\U0001f33e\U0001f33e\U0001f33e\U0001f33e\U0001f33e\U0001f33e",
                               
                               @"\U0001f499LunchTime\U0001f493\n. \U0001f61a\U0001f35c        \U0001f35f\n=  |<\U0001f372 O|||  \U0001f354\n~ \u3145        |  | \U0001f366\U0001f361",
                               
                               @"\U0001f33b\U0001f33b\U0001f33b\U0001f33b\U0001f33b\U0001f33b\U0001f33b\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f380 \u2601 \u2601 \u2601 \U0001f380 \u2601\n\u2601 \u2601 \U0001f380 \u2601 \U0001f380 \u2601 \u2601\n\u2601 \U0001f3a9 \u2601 \U0001f380 \u2601 \U0001f3a9 \u2601\n\U0001f3a9\U0001f451\U0001f3a9\U0001f48e\U0001f3a9\U0001f451\U0001f3a9\n\U0001f3a9\U0001f451\U0001f451\U0001f48e\U0001f451\U0001f451\U0001f3a9\n\u2601 \U0001f3a9 \U0001f3a9 \U0001f48e \U0001f3a9 \U0001f3a9 \u2601\n\u2601 \u2601 \U0001f451 \U0001f48e \U0001f451 \u2601 \u2601\n\u2601 \U0001f3a9 \U0001f3a9 \U0001f48e \U0001f3a9 \U0001f3a9 \u2601\n\U0001f3a9\U0001f3a9\u2601 \u2601 \u2601 \U0001f3a9 \U0001f3a9\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339\U0001f339",
                               
                               @"\U0001f499\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f499\n\U0001f1fa\U0001f1f8\U0001f497\U0001f497\U0001f1fa\U0001f1f8\U0001f497\U0001f497\U0001f1fa\U0001f1f8\n\U0001f1fa\U0001f1f8\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\U0001f1fa\U0001f1f8\n\U0001f1fa\U0001f1f8\U0001f497\U0001f497\U0001f497\U0001f497\U0001f497\U0001f1fa\U0001f1f8\n\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f497\U0001f497\U0001f497\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\n\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f497\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\U0001f1fa\U0001f1f8\n\U0001f1fa\U0001f1f8\u24CC \u24BA*\u24C1 \u24C4 \u24CB \u24BA*\U0001f1fa\U0001f1f8\n\U0001f49c.*\u3002 \u24CA.\u24C8.\u24B6 \u3002*.\U0001f49c",
                               
                               @".\U0001f4a1studying..\n.  (. . )\U0001f4a1\n-<.    />--\n.  \U0001f4d6\nDo not disturb!",
                               
                               @"\u274C \U0001f37a \U0001f37a \U0001f37a \U0001f37a \U0001f37a \u274C\n\U0001f37a\u274C \U0001f37a \U0001f37a \U0001f37a \u274C \U0001f37a\n\U0001f37a\U0001f37a\u274C \U0001f37a \u274C \U0001f37a \U0001f37a\n\U0001f37a\U0001f37a\U0001f37a\u274C \U0001f37a \U0001f37a \U0001f37a\n\U0001f37a\U0001f37a\u274C \U0001f37a \u274C \U0001f37a \U0001f37a\n\U0001f37a\u274C \U0001f37a \U0001f37a \U0001f37a \u274C \U0001f37a\n\u274C \U0001f37a \U0001f37a \U0001f37a \U0001f37a \U0001f37a \u274C\n\U0001f645STOP drinking\U0001f645\n\U0001f616\U0001f616\U0001f616\U0001f616\U0001f616\U0001f616\U0001f616",
                               
                               @"\"Life Is    \u3002    \U0001f33a  \u266C\nBeautiful,  \U0001f33a \U0001f33a\n\u263A  *    \u3001  \U0001f33a    \U0001f33a\nEnjoy it.\"  \U0001f33a  \'    \U0001f33a\n  \U0001f3b6        \U0001f33a      \U0001f33a\n`    \U0001f33a\U0001f33a\U0001f33a\n\U0001f33a\U0001f33a\U0001f33a\U0001f33a  *    \u263A\n  \U0001f33a\U0001f33a\U0001f33a  \u3002 \U0001f3b6\n\u266C          *",
                               
                               @"\u2755 \u2755 \u2755 \u2755 \u2755 \u2755 \u2755 \u2755\n\u3001*. :\U0001f4a1\U0001f4a1\U0001f4a1\U0001f4a1\'\u3002*=\n\u3002 \U0001f4a1 \U0001f4a1 \U0001f4a1 \U0001f4a1 \U0001f4a1 \U0001f4a1 \u3001:\n*\".\U0001f4a1\U0001f4a1\U0001f4a1\U0001f4a1\U0001f4a1\U0001f4a1+\"\n+,\'\U0001f4a1\U0001f4a1\U0001f374\U0001f374\U0001f4a1\U0001f4a1:\u30FB\'\n.\"-\U0001f4a1\U0001f4a1\U0001f374\U0001f374\U0001f4a1\U0001f4a1*\u3001\n\u3002:* \",\U0001f4a1\U0001f374\U0001f374\U0001f4a1=\u3002*..\n\u2728=\u3002\'\U0001f451\U0001f451\U0001f451\u3001\"-\u2728\n\u2728 \u3001+.\U0001f451\U0001f451\U0001f451:\'\u3001,\u2728\n\u3002* \u2728 \u3001 \U0001f451 \U0001f451 \u3001 \u2728*\u3002\n! \u24BC \u24C4 \u24C4 \u24B9 \U0001f6a5 \u24BE \u24B9 \u24BA \u24B6 !\n\u2755 \U0001f601 \U0001f601 \U0001f601 \U0001f601 \U0001f601 \U0001f601 \u2755",
                               
                               @"\uE326. _\uE503_ \uE326\n.    (^ o ^) ~\uE041\n.    \uE03C\nKaraoke night!",
                               
                               @"\uE04A \uE049 \uE049 \uE049 \uE049 \uE049 \uE049 \uE049\n\uE049 \uE049 \uE049_\uE00A_\uE049 \uE049 \uE01D\n\uE049 \uE049 \uE049 \uE00A \uE00A \uE049 \uE049 \uE049\n\uE049 \uE049 \uE049 \uE00A \uE00A \uE049 \uE049 \uE049\n\uE049 \uE049 \uE049 \uE00A \uE00A \uE049 \uE049 \uE049\n\uE049 \uE049 \uE00A \uE00A \uE00A \uE00A \uE049 \uE049\n\uE049 \uE00A \uE00A \uE00A \uE00A \uE00A \uE00A \uE049\n\uE00A \uE00A \uE00A \uE00A \uE00A \uE00A \uE00A \uE00A\n\uE00A \uE00A \uE049 \uE00A \uE00A \uE049 \uE00A \uE00A\n\uE00A \uE049 \uE049 \uE00A \uE00A \uE049 \uE049 \uE00A\n\uE049 \uE049 \uE049 \uE00A \uE00A \uE049 \uE049 \uE049\n\uE049 \uE049 \uE00A \uE00A \uE00A \uE00A \uE049 \uE049\n\uE049 \uE049 \uE049 \uE049 \uE049 \uE049 \uE049 \uE049",
                               
                               @"\uE02F|\uE024 \uE025 \uE026 \uE027 \uE028\n\uE311|\uFF3F  \uFF3F  |  \uFF3F \uE13D\n\uE311|  |  |\uFF3F| |  |\uFF3F| \uE13D\n\uE311|  |  |\uFF3F  |  |    \uE13D\n\uE311 \uE337 \uE115~ \uFF3F    \uE13D\n\uE311|\u30FC|\u30FC|  |\uFF3F|  \uE337\n\uE311|    |    |  |\uFF3F  \uE107\n\uE029 \uE02A \uE02B \uE02C \uE02D \uE02E",
                               
                               @"                        \U0001f6bd\U0001f6bd\U0001f6bd\n                        \U0001f6bd\U0001f6bd\U0001f6bd\n                        \U0001f6bd\U0001f6bd\U0001f6bd\n        \U0001f4a9          \U0001f6bd\U0001f6bd\U0001f6bd\n\U0001f4fc\U0001f4fc\U0001f4fc\U0001f4fc\U0001f4fc\U0001f6bd\U0001f6bd\U0001f6bd\n\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\n  \U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\n  \U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\U0001f6bd\n        \U0001f6bd\U0001f6bd\U0001f6bd\n        \U0001f6bd\U0001f6bd\U0001f6bd",
                               
                               @"\uE04C \uE04C \uE04C \uE32C \uE32C \uE04C \uE04C \uE04C\n\uE32E \uE04C \uE327 \uE328 \uE328 \uE327 \uE04C \uE32E\n\uE329 \uE327 \uE329 \uE329 \uE329 \uE329 \uE32C \uE32C\n\uE327 \uE32D \uE32C \uE32C \uE32C \uE32C \uE32C \uE32E\n\uE329 \uE32E \uE335 \uE335 \uE011 \uE11D \uE00E \uE335\n\uE32E \uE04B \uE04C \uE04C \uE04C \uE13D \uE335 \uE32E\n\uE335 \uE329 \uE327 \uE327 \uE32C \uE327 \uE32C \uE32C\n\uE04C \uE04C \uE030 \uE030 \uE304 \uE118 \uE110 \uE335\n\uE32E \uE335lots of love\uE335 \uE32E",
                               
                               @"                              \U0001f3a9\n                         \U0001f3a9\U0001f60d\n                    \U0001f3a9\U0001f603\U0001f454\n               \U0001f3a9\U0001f60a\U0001f454\U0001f456\n          \U0001f3a9\U0001f60b\U0001f454\U0001f456\U0001f45e\n     \U0001f3a9\U0001f60f\U0001f454\U0001f456\U0001f45e\U0001f4b0\n\U0001f3a9\U0001f615\U0001f454\U0001f456\U0001f45e\U0001f4b0\U0001f4b0\n\U0001f62b\U0001f455\U0001f456\U0001f45e\U0001f4b0\U0001f4b0\U0001f4b0\n\U0001f455\U0001f456\U0001f45e\U0001f4b0\U0001f4b0\U0001f4b0\U0001f4b0\n\U0001f456\U0001f45f\U0001f4b0\U0001f4b0\U0001f4b0\U0001f4b0\U0001f4b0\n\U0001f461\U0001f4b0\U0001f4b0\U0001f4b0\U0001f4b0\U0001f4b0\U0001f4b0",
                               
                               @"  \u303D                            \u303D\n\u2755                            \u2755\n\u2755                            \u2755\n\u25FC \u25FC \u25FC    \u25FC \u25FC \u25FC\n\u25FC \u25FC \u25FC \u2796 \u25FC \u25FC \u25FC\n    \u25FC \u25FC        \u25FC \u25FC",
                                                        nil];
    
    NSMutableArray* arrMood = [[NSMutableArray alloc] initWithObjects:
                               @"\U0001f41d\u2601 \u2601 \u2601 \U0001f41d \u2601\n\u2601 \U0001f41d \u2601 \u2601 \u2601 \u2601\n\u2601 \u2601 \u2601 \u2601 \U0001f41d \u2601\n\u2601 \U0001f36f \U0001f36f \U0001f36f \U0001f36f \u2601\n\u2601 \U0001f36f \U0001f41d \U0001f41d \U0001f41d \u2601\n\u2601 \U0001f36f \U0001f41d \U0001f41d \U0001f36f \u2601\n\u2601 \U0001f36f \U0001f36f \U0001f36f \U0001f36f \u2601\n\u2601 \U0001f36f \u2601 \u2601 \U0001f36f \u2601\n\U0001f340\U0001f340\U0001f340\U0001f33a\U0001f340\U0001f340\n\U0001f340\U0001f33a\U0001f340\U0001f340\U0001f340\U0001f33a",
                               
                               @"     \U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n  \U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\n\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\n  \U0001f337\U0001f337\U0001f337\U0001f337\U0001f337\U0001f337\n    \U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n       \U0001f340\U0001f340\U0001f340\U0001f340\n           \U0001f40a\U0001f40a\n              \U0001f331\n         \U0001f343\U0001f331\n              \U0001f331",
                               
                               @"              \U0001f33b\U0001f33b\n\U0001f33b    \U0001f33b\U0001f33b\U0001f33b\U0001f33b    \U0001f33b\n    \U0001f33b\U0001f33b\U0001f3b1\U0001f3b1\U0001f33b\U0001f33b\n\U0001f33b\U0001f33b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f33b\U0001f33b\n    \U0001f33b\U0001f33b\U0001f3b1\U0001f3b1\U0001f33b\U0001f33b\n\U0001f33b    \U0001f33b\U0001f33b\U0001f33b\U0001f33b    \U0001f33b\n              \U0001f33b\U0001f33b\n              \U0001f335\U0001f335\n    \U0001f60a    \U0001f335\U0001f335    \U0001f60a\n    \U0001f335    \U0001f335\U0001f335    \U0001f335\n\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335",
                               
                               @"\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\n\U0001f436\U0001f43e\U0001f43e\U0001f436\U0001f43e\U0001f43e\U0001f436\n\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\n\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\n\U0001f436\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f436\n\U0001f436\U0001f436\U0001f43e\U0001f43e\U0001f43e\U0001f436\U0001f436\n\U0001f436\U0001f436\U0001f436\U0001f43e\U0001f436\U0001f436\U0001f436\n\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436",
                               
                               @"\U0001f43d\U0001f437\U0001f437\U0001f437\U0001f437\U0001f437\U0001f43d\n\U0001f437\U0001f43c\U0001f43c\U0001f437\U0001f43c\U0001f43c\U0001f437\n\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\n\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\n\U0001f437\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f437\n\U0001f437\U0001f437\U0001f43c\U0001f43c\U0001f43c\U0001f437\U0001f437\n\U0001f43d\U0001f437\U0001f437\U0001f43c\U0001f437\U0001f437\U0001f43d",
                               
                               @"          \u26AB \U0001f436 \U0001f436 \U0001f436 \u26AB\n        \u26AB \U0001f436 \U0001f436 \U0001f436 \U0001f436 \u26AB\n        \u26AB \U0001f3b1 \U0001f436 \U0001f436 \U0001f3b1 \u26AB\n            \U0001f436\U0001f436\U0001f3a5\U0001f436\n          \U0001f436\U0001f436\U0001f445\U0001f436\n        \U0001f436\U0001f436\U0001f436\U0001f436\n        \U0001f436\u25FE \U0001f436 \u25FE\n\U0001f4a9 \u27B0 \U0001f436 \u26AB \U0001f436 \u26AB",
                               
                               @"     \U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\n\U0001f330\U0001f330\U0001f315\U0001f315\U0001f315\U0001f315\U0001f330\U0001f330\n\U0001f330\U0001f330\U0001f315\U0001f315\U0001f315\U0001f315\U0001f330\U0001f330\n\U0001f330\U0001f330\U0001f3b1\U0001f315\U0001f315\U0001f3b1\U0001f330\U0001f330\n     \U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\n       \U0001f315\U0001f315\U0001f497\U0001f315\U0001f315\n            \U0001f315\U0001f445\U0001f315\n           \U0001f315\U0001f315\U0001f315\U0001f315\n     \U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\n   \U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\n\U0001f315\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f315\n\U0001f315\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f315\n\U0001f315\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f315\n\U0001f330\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f330\n\n      \U0001f4bf\U0001f4bf        \U0001f4bf\U0001f4bf\n        \U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\n      \U0001f4bf\U0001f4bf        \U0001f4bf\U0001f4bf",
                               
                               @"\U0001f552\U0001f331\U0001f331\U0001f331\U0001f331\U0001f331\U0001f331\U0001f552\n\U0001f552\U0001f552\U0001f331\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\n\U0001f331\U0001f552\U0001f552\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\n\U0001f331\U0001f552\U0001f552\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\U0001f331\n\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\U0001f331\n\U0001f552\U0001f552\U0001f340\U0001f552\U0001f552\U0001f340\U0001f552\U0001f331\n\U0001f552\U0001f552\U0001f552\U0001f552\u3030 \U0001f552 \U0001f552 \U0001f331\n\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\n\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\U0001f331\n\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\n\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552",
                               
                               @"                    \U0001f380\n        \U0001f424\U0001f424\U0001f424\U0001f424\n     \U0001f424\U0001f3b1\U0001f424\U0001f424\U0001f424\n\U0001f53a\U0001f424\U0001f424\U0001f424\U0001f424\U0001f424\n        \U0001f424\U0001f424\U0001f424\U0001f424\n               \U0001f424\U0001f424          \U0001f424\n          \U0001f424\U0001f424\U0001f4ab\U0001f424\U0001f424\U0001f424\n          \U0001f424\U0001f424\U0001f424\U0001f4ab\U0001f424\n             \U0001f424\U0001f424\U0001f424\U0001f424\n                    \U0001f4cd\U0001f4cd\n                    \U0001f53a\U0001f53a",
                               
                               @"          \u2601 \u2601\n\U0001f363\u2601 \U0001f3b1 \u2601 \u2601\n          \u2601 \u2601 \u2601\n               \u2601 \u2601\n          \u2601 \u2601               \u2601\n     \u2601 \u2601               \u2601 \u2601\n\u2601 \u2601               \u2601 \u2601 \u2601\n\u2601 \u2601     \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n     \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\U0001f300\U0001f300\u2601 \u2601 \u2601 \u2601 \U0001f300 \U0001f300\n\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\n\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300",
                               
                               @"\u2744 \u2744 \U0001f4cd \U0001f4cd \u2744 \u2744 \u2744 \u2744\n\U0001f315\U0001f315\U0001f440\U0001f315\U0001f31b\u2744 \u2744 \u2744\n\u2693 \U0001f315 \U0001f315 \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f31e \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f31e \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f31e \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f31e \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f31e \u2744 \u2744 \u2744 \U0001f6a9\n\u2744 \u2744 \U0001f315 \U0001f31e \U0001f31e \U0001f315 \U0001f315 \U0001f315\n\u2744 \u2744 \U0001f315 \U0001f315 \U0001f315 \U0001f315 \U0001f31e \U0001f31e\n\u2744 \u2744 \U0001f315 \u2744 \U0001f315 \u2744 \U0001f31e \U0001f31e\n\u2744 \u2744 \U0001f315 \u2744 \U0001f31e \u2744 \U0001f31e \U0001f315\n\u2744 \u2744 \U0001f31e \u2744 \U0001f315 \u2744 \U0001f31e \U0001f315\n\u2744 \u2744 \U0001f31e \u2744 \U0001f315 \u2744 \U0001f31e \U0001f315\n\u2744 \u2744 \U0001f330 \u2744 \U0001f330 \u2744 \U0001f330 \U0001f330",
                               
                               @"    \U0001f63c                    \U0001f63c\n\U0001f63c    \U0001f63c\U0001f63c\U0001f63c\U0001f63c    \U0001f63c\n\U0001f63c                              \U0001f63c\n\U0001f63c                    \U0001f4a2    \U0001f63c\n\U0001f63c    \U0001f3b1          \U0001f3b1    \U0001f63c\n\U0001f63c                              \U0001f63c\n\U0001f63c            \U0001f344            \U0001f63c\n\U0001f63c            ^              \U0001f63c\n\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c",
                               
                               @"        \U0001f53a            \U0001f53a\n       \U0001f314\U0001f315\U0001f315\U0001f315\U0001f316\n\u2712 \U0001f314 \U0001f30d \U0001f315 \U0001f315 \U0001f30f \U0001f316 \U0001f526\n\u2796 \U0001f314 \U0001f315 \U0001f497 \U0001f315 \U0001f315 \U0001f316 \u2796\n\U0001f526\U0001f314\U0001f315\U0001f445\U0001f315\U0001f315\U0001f316\u2712\n       \U0001f314\U0001f315\U0001f315\U0001f315\U0001f316",
                               
                               @"     \u26AB \u26AB          \u26AB \u26AB\n     \u26AB \U0001f55c \u26AA \u26AA \U0001f566 \u26AB\n\u26AA \u26AA \u26AA \u26AA \u26AA \u26AA \u26AA \U0001f566\n\u26AA \u26AB \U0001f3b1 \u26AA \u26AA \U0001f3b1 \u26AB \U0001f567\n\u26AB \u26AB \u26AA \u26AA \u26AA \u26AA \u26AB \u26AB\n\u26AB \u26AA \u26AA \u25FC \u25FC \u26AA \u26AA \u26AB\n     \u26AA \u26AA \u3030 \u3030 \u26AA \U0001f557\n          \U0001f562\U0001f562\U0001f562\U0001f557",
                                                            nil];
    
    NSMutableArray* arrLove = [[NSMutableArray alloc] initWithObjects:
                               @"\uE32E /\u25A0 \u25A0 \u25A0\\~\uE326\n    |  \uE022 \uE022  |    \uE32E\n    |    \uE41A    \uE41B\n\uE32E \\\uFF3F \uE003 \uFF3F/\n*I\'m Fallin in LOVE*\n\uE329 \uE327 \uE111 \uE312 \uE425 \uE437 \uE328\nILuvYOU\uE032xoxo\uE418",
                               
                               @"\uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E\n\uE32E \uE32E \uE003 \uE003 \uE32E \uE003 \uE003 \uE32E \uE32E\n\uE32E \uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \u2728\n\u2728 \uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \u2728\n\u2728 \u2728 \uE003 \uE003 \uE003 \uE003 \uE003 \uE32E \uE32E\n\uE32E \uE32E \uE32E \uE003 \uE003 \uE003 \uE32E \uE32E \uE32E\n\uE32E \uE32E \uE32E \uE32E \uE003 \uE32E \uE32E \uE32E \uE32E\n\uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E",
                               
                               @"\uE413 \uE413 \uE413 \uE413 \uE413 \uE413 \uE413\n\uE413 \uE023 \uE023 \uE413 \uE023 \uE023 \uE413\n\uE023 \uE023 \uE023 \uE023 \uE023 \uE023 \uE023\n\uE023 \uE023 \uE023 \uE023 \uE023 \uE023 \uE023\n\uE023 \uE023 \uE023 \uE023 \uE023 \uE023 \uE023\n\uE413 \uE023 \uE023 \uE023 \uE023 \uE023 \uE413\n\uE413 \uE413 \uE023 \uE023 \uE023 \uE413 \uE413\n\uE413 \uE413 \uE413 \uE023 \uE413 \uE413 \uE413\nyou heartless boy.",
                               
                               @"\uE219\n  L=  \uE306 \uE21A\n  /\\.        \\\\ I\n              \u3131 \u3134\nTake my heart",
                               
                               @"\uE049 \uE049 \uE049 \uE049 \uE049 \uE049 \uE049\n\uE049 \uE022 \uE022 \uE049 \uE022 \uE022 \uE049\n\uE022 \uE022 \uE022 \uE022 \uE022 \uE022 \uE022\n\uE022 \uE022 \uE022 \uE022 \uE022 \uE022 \uE022\n\uE022 \uE022 \uE022 \uE022 \uE022 \uE022 \uE022\n\uE049 \uE022 \uE022 \uE022 \uE022 \uE022 \uE049\n\uE049 \uE049 \uE022 \uE022 \uE022 \uE049 \uE049\n\uE049 \uE049 \uE049 \uE022 \uE049 \uE049 \uE049\n\u3002        /\n\uE335 \u3002/\n      /\u3000 \uE049\n    /\n  (\u3000 \u3000 \uFF0B \uE335\n    \uFF3C \u3000 \u3000 \uE049 \uE049 \u3002\n      \uFF3C\n\uE049 \u3000  )\n          /\n\uE335 \u3002 /            \uE049\n      /\n\u3000 \u3000  (\u3000 \u3000 \uFF0B \uE335\n    \uE022\n      \uFF3C \uE415/\n          \uE319\n            | |        \uE529 \uE529\n\uE447 \uE447 \uE447 \uE447 \uE447 \uE447 \uE447 \uE447",
                               
                               @"morning my sunshine girl,\nHave a good day :)\n        \uE304      \uE304\n    \uE304  \uE304 \uE304 \uE304\n  \uE304    \uE32E \uE304    \uE304\n\uE304        \uE32E \uE304    \uE304\n\uE304      \uE32E \uE304      \uE304\n  \uE304 \uE32E \uE304 \uE304 \uE304\n        \uE304 \uE304 \uE304  \uE447\n  \uE447 \uE447  \uE308  \uE447 \uE447\n        \uE447 \uE308 \uE447 \uE447\n  ______\uE308______\n[\uE535 \uE535 \uE535 \uE535 \uE535 \uE535]\n  \\\uE205 \uE205 \uE205 \uE205 \uE205/\n    \\\uE205 \uE205 \uE205 \uE205/\n      \\\uE205 \uE205 \uE205/\n        \uFFE3 \uFFE3 \uFFE3",
                               
                               @"\uE112LOVELOVELOVE\uE112\n\uE112 \uE418~\uE034 \uE230 \uE106-\uE022 \uE112\n\uE112ONLYFOR*YOU*\uE112",
                               
                               @"\uE32E.\u2022*\u00A8*.\u00B8.\u2022*\u00A8*.\u00B8 \u00B8.\u2022*\u00A8`*\u2022 \u01B8 \u04DC \u01B7\n\uE030 \uE303 \uE030 \uE303 \uE030 \uE303 \uE030 \uE303\nSprinkled with love\uE022\n\uE304 \uE305 \uE304 \uE305 \uE304 \uE305 \uE304 \uE305\n\u00A8*.\u00B8.\u2022*\u00A8`*. \u00B8.\u2022*\u00A8*.\u00B8 \u00B8.\u2022*\u00A8`*\u2022.\uE32E\n\uE032 \uE110 \uE032 \uE110 \uE032 \uE110 \uE032 \uE110",
                               
                               @"\uE003 \uE439 \uE439 \uE439 \uE439 \uE439 \uE439 \uE003\n\uE439 \uE439 \uE43A \uE439 \uE439 \uE43A \uE439 \uE439\n\uE439 \uE43A \uE43A \uE43A \uE43A \uE43A \uE43A \uE439\n\uE43A \uE43A \uE43A \uE43A \uE43A \uE43A \uE43A \uE43A\n\uE439 \uE43A \uE43A \uE43A \uE43A \uE43A \uE43A \uE439\n\uE439 \uE439 \uE439 \uE43A \uE43A \uE439 \uE439 \uE439\n\uE003 \uE439 \uE439 \uE439 \uE439 \uE439 \uE439 \uE003\n\uE022 \uE022KISS\uE111ME\uE022 \uE022",
                               
                               @"\U000e106dreaming\n\U000e418blowing u kisses\n\uE417kiss me\n\uE023if we break up i would\uE411\n\uE329i love you much\n\uE333 \uE332 \uE333 \uE332 \uE333 \uE332",
                               
                               @"\uE32E \uE32E \uE335 \uE335 \uE32E \uE32E\n  \uE328Loving you\n\uE044 \uE044 \uE111 \uE111 \uE044 \uE044\n  Missing you\uE328\n  ----\uE001 \uE002------\n\uE056Sleep tight\uE056\n\uE327 \uE327 \uE044 \uE044 \uE327 \uE327",
                               
                               @"\uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304\n\uE304 \uE418 \uE418 \uE418 \uE304 \uE418 \uE418 \uE418 \uE304\n\uE304 \uE418 \uE022 \uE022 \uE418 \uE418 \uE022 \uE022 \uE304\n\uE418 \uE022 \uE022 \uE022 \uE418 \uE022 \uE022 \uE022 \uE418\n\uE418 \uE022 \uE022 \uE022 \uE022 \uE022 \uE022 \uE022 \uE418\n\uE304 \uE418 \uE022 \uE022 \uE022 \uE022 \uE022 \uE418 \uE304\n\uE304 \uE304 \uE418 \uE022 \uE022 \uE022 \uE418 \uE304 \uE304\n\uE304 \uE304 \uE304 \uE418 \uE022 \uE418 \uE304 \uE304 \uE304\n\uE304 \uE106 \uE304 \uE304 \uE418 \uE304 \uE304 \uE106 \uE304\n\uE304 \uE106 \uE106 \uE106 \uE304 \uE106 \uE106 \uE106 \uE304\n\uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304",
                               
                               @"\uE044 \uE022 \uE022 \uE044 \uE022 \uE022 \uE044\n\uE022 \uE112 \uE022 \uE022 \uE022 \uE022 \uE022\n\uE022 \uE112 \uE022 \uE022 \uE022 \uE022 \uE022\n\uE044 \uE112 \uE022 \uE022 \uE022 \uE022 \uE044\n\uE044 \uE112 \uE112 \uE112 \uE022 \uE044 \uE044\n\uE044 \uE044 \uE044 \uE022 \uE044 \uE044 \uE044\n\uE044 \uE328 \uE328 \uE044 \uE328 \uE328 \uE044\n\uE328 \uE328 \uE328 \uE328 \uE106 \uE106 \uE106\n\uE328 \uE328 \uE328 \uE328 \uE106 \uE328 \uE106\n\uE044 \uE328 \uE328 \uE328 \uE106 \uE328 \uE106\n\uE044 \uE044 \uE328 \uE328 \uE106 \uE106 \uE106\n\uE044 \uE044 \uE044 \uE328 \uE044 \uE044 \uE044\n\uE044 \uE32D \uE32D \uE044 \uE32D \uE32D \uE044\n\uE32D \uE32D \uE32D \uE32D \uE32D \uE32D \uE32D\n\uE32D \uE032 \uE32D \uE032 \uE32D \uE32D \uE32D\n\uE044 \uE032 \uE32D \uE032 \uE32D \uE32D \uE044\n\uE044 \uE032 \uE32D \uE032 \uE32D \uE044 \uE044\n\uE044 \uE044 \uE032 \uE32D \uE044 \uE044 \uE044\n\uE044 \uE32C \uE32C \uE044 \uE32C \uE32C \uE044\n\uE32C \uE32C \uE32C \uE41C \uE41C \uE41C \uE32C\n\uE32C \uE32C \uE32C \uE41C \uE32C \uE32C \uE32C\n\uE044 \uE32C \uE32C \uE41C \uE41C \uE41C \uE044\n\uE044 \uE044 \uE32C \uE41C \uE32C \uE044 \uE044\n\uE044 \uE044 \uE044 \uE41C \uE41C \uE41C \uE044",
                               
                               @"\uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE32E\n\uE32E \uE327 \uE327 \uE335 \uE327 \uE327 \uE32E\n\uE327 \uE335 \uE335 \uE327 \uE335 \uE335 \uE327\n\uE327 Miss \uE335 you \uE327\n\uE32E \uE327 \uE335 \uE335 \uE335 \uE327 \uE32E\n\uE32E \uE32E \uE327 \uE335 \uE327 \uE32E \uE32E\n\uE32E \uE32E \uE32E \uE327 \uE32E \uE32E \uE32E\n\uE022 \uE022 \uE022 \uE022 \uE022 \uE022 \uE022",
                               
                               @"\uE32E \uE42B \uE42B \uE42B \uE42B \uE42B \uE42B \uE32E\n\uE42B \uE42B \uE42B \uE42B \uE42B \uE42B \uE42B \uE42B\n\uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044\n\uE129 \uE129 \uE129 \uE044 \uE044 \uE129 \uE129 \uE129\n\uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE044\n\uE044 \uE044 \uE41C \uE044 \uE044 \uE41C \uE044 \uE044\n\uE044 \uE41C \uE41C \uE41C \uE41C \uE41C \uE41C \uE044\n\uE41C \uE41C \uE41C \uE41C \uE41C \uE41C \uE41C \uE41C\n\uE044 \uE41C \uE41C \uE41C \uE41C \uE41C \uE41C \uE044\n\uE044 \uE044 \uE41C \uE41C \uE41C \uE41C \uE044 \uE044\n\uE32E \uE044 \uE044 \uE044 \uE044 \uE044 \uE044 \uE32E\n\uE32D \uE328KISS\uE32CME\uE328 \uE32D",
                               nil];
    
    NSMutableArray* arrGesture = [[NSMutableArray alloc] initWithObjects:
                                  @"\U0001f497\U0001f497\U0001f497\n  \U0001f64b\U0001f4achi!\u2728\n  \U0001f45a\n  \U0001f456\n\U0001f460\U0001f460",
                                  
                                  @"      \U0001f452\n  \U0001f442\U0001f440\U0001f442\n      \U0001f443\n      \U0001f319\n  \U0001f44f\U0001f457\n      /    \\\n    \U0001f460  \U0001f460",
                                  
                                  @"  \u2755 \U0001f603 \U0001f603 \U0001f603 \u2755 \u2755\n\U0001f603\U0001f49c\U0001f603\U0001f49c\U0001f603\U0001f44d\n\U0001f603\U0001f603\U0001f445\U0001f603\U0001f603\u2755\n\u2755 \U0001f603 \U0001f603 \U0001f603 \u2755 \u2755",
                                  
                                  @"\u2B50 \U0001f17e \U0001f17e \u2B50 \u2B50 \u2B50 \U0001f17e \U0001f17e\n\U0001f17e\U0001f60e\U0001f17e\u2B50 \u2B50 \U0001f17e \U0001f60e \U0001f17e\n\U0001f17e\U0001f60e\U0001f17e\u2B50 \u2B50 \U0001f17e \U0001f60e \U0001f17e\n\U0001f17e\U0001f60e\U0001f17e\u2B50 \u2B50 \U0001f17e \U0001f60e \U0001f17e\n\U0001f17e\U0001f60e\U0001f17e\U0001f17e\U0001f17e\U0001f17e\U0001f60e\U0001f17e\n\U0001f17e\U0001f60e\U0001f17e\U0001f60e\U0001f60e\U0001f17e\U0001f60e\U0001f17e\n\U0001f17e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f17e\n\U0001f17e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f17e\n\U0001f17e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f17e\n\U0001f17e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f60e\U0001f17e\u2B50\n\u2B50 \U0001f17e \U0001f60e \U0001f60e \U0001f60e \U0001f17e \u2B50 \u2B50\n\u2B50 \U0001f17e \U0001f60e \U0001f60e \U0001f60e \U0001f17e \u2B50 \u2B50\n\u2B50 \U0001f17e \U0001f17e \U0001f17e \U0001f17e \U0001f17e \u2B50 \u2B50",
                                  
                                  @"\U0001f49b\U0001f49b\U0001f49b\U0001f44d\U0001f44d\U0001f44d\U0001f49b\U0001f49b\n\U0001f49b\U0001f49b\U0001f49b\U0001f44d\u2601 \U0001f44d \U0001f49b \U0001f49b\n\U0001f49b\U0001f49b\U0001f49b\U0001f44d\u2601 \U0001f44d \U0001f49b \U0001f49b\n\U0001f49b\U0001f49b\U0001f44d\u2601 \u2601 \U0001f44d \U0001f44d \U0001f44d\n\U0001f44d\U0001f44d\u2601 \u2601 \u2601 \u2601 \u2601 \U0001f44d\n\U0001f44d\u2601 \u2601 \u2601 \u2601 \U0001f44d \U0001f44d \U0001f44d\n\U0001f44d\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \U0001f44d\n\U0001f44d\u2601 \u2601 \u2601 \u2601 \U0001f44d \U0001f44d \U0001f44d\n\U0001f44d\U0001f44d\u2601 \u2601 \u2601 \u2601 \u2601 \U0001f44d\n\U0001f49b\U0001f49b\U0001f44d\U0001f44d\U0001f44d\U0001f44d\U0001f44d\U0001f49b",
                                  
                                  @"\u2601 \u2601 \U0001f4a9 \U0001f4a9 \U0001f4a9 \U0001f4a9 \U0001f4a9 \u2601\n\U0001f4a9\U0001f4a9\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f4a9\n\U0001f4a9\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f4a9\U0001f4a9\U0001f4a9\n\U0001f4a9\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f4a9\n\U0001f4a9\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f4a9\U0001f4a9\U0001f4a9\n\U0001f4a9\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f44e\U0001f4a9\n\U0001f4a9\U0001f4a9\U0001f44e\U0001f44e\U0001f44e\U0001f4a9\U0001f4a9\U0001f4a9\n\u2601 \u2601 \U0001f4a9 \U0001f44e \U0001f44e \U0001f4a9 \u2601 \u2601\n\u2601 \u2601 \u2601 \U0001f4a9 \U0001f44e \U0001f4a9 \u2601 \u2601\n\u2601 \u2601 \u2601 \U0001f4a9 \U0001f4a9 \U0001f4a9 \u2601 \u2601",
                                  
                                  @"  \U0001f646\U0001f4acOMG!\U0001f4a8\n  \U0001f45a\n  \U0001f456\n\U0001f460\U0001f460",
                                  
                                  @"\u2755 \u2755 \u2755 \u2755 \U0001f197 \U0001f197 \U0001f197 \u2755\n\u2755 \u2755 \u2755 \u2755 \U0001f197 \U0001f44c \U0001f44c \U0001f197\n\u2755 \u2755 \u2755 \u2755 \u2755 \U0001f197 \U0001f44c \U0001f197\n\u2755 \U0001f197 \U0001f197 \U0001f197 \U0001f197 \U0001f197 \U0001f44c \U0001f197\n\U0001f197\U0001f44c\U0001f44c\U0001f44c\U0001f44c\U0001f44c\U0001f44c\U0001f197\n\U0001f197\U0001f44c\U0001f197\U0001f197\U0001f197\U0001f44c\U0001f44c\U0001f197\n\U0001f197\U0001f197\u2755 \u2755 \u2755 \U0001f197 \U0001f44c \U0001f197\n\U0001f197\U0001f44c\U0001f197\u2755 \u2755 \U0001f197 \U0001f44c \U0001f197\n\U0001f197\U0001f44c\U0001f44c\U0001f197\U0001f197\U0001f44c\U0001f44c\U0001f197\n\u2755 \U0001f197 \U0001f44c \U0001f44c \U0001f44c \U0001f44c \U0001f197 \U0001f197\n\u2755 \u2755 \U0001f197 \U0001f197 \U0001f197 \U0001f197 \u2755 \u2755",
                                  nil];
    
    NSMutableArray* arrFood = [[NSMutableArray alloc] initWithObjects:
                               @"\U0001f359Do you want some \U0001f358\n\U0001f354n\U0001f440dle\u2754~\U0001f373~~~~\U0001f35f\n~\U0001f362\u2728 \U0001f61d \U0001f449 \U0001f372 \U0001f35c \U0001f374 \U0001f361~\n\u2755 \u2755 \u24E8 \u24E4 \u24DC \u24DC \u24E8 \u2755 \\\uE022/    \\\uE32C/ \"",
                               
                               @"A glass of\n  \\/        \\ /          Wine,\n  \u3163      \u3163        Tonight?\"\n  \u3157      \u3157",
                               
                               @"\uE417~~~~~~~~~~\uE330 \uE437\n\u2554 \u2550\"\u2554 \u2557 \u2554 \u2550 \u2554 \u2550 \u2554 \u2550 \u2554 \u2550*\n\u2551 \uE326 \u2551 \u2551 \u2560 \u2550 \u2560 \u2550 \u2560 \u2550 \u2560 \u2550\n\u255A \u2550*\u255A \u255D \u2569 \u3002 \u2569 \uFF1A \u255A \u2550 \u255A \u2550\n\uE32E \uE046 \uE045 \uE34B \uE34B \uE045 \uE046 \uE32E",
                               
                               @".        @@@.\n        |*******|\\.\n        |  \uE335  |. )\n        |_____|/\n\"Should I grab a cup of\n  Coffee for you?\uE405\"",
                               
                               @"  Let`s go\n\U0001f356\U0001f35d\U0001f363\U0001f35f\n\U0001f357\U0001f354\U0001f355\U0001f367\n\U0001f370\U0001f379\U0001f368\U0001f366",
                               
                               @"          \U0001f353\U0001f353\n         \U0001f353\U0001f353\U0001f353\n          \U0001f353\U0001f353\U0001f365\n         \U0001f365\U0001f365\U0001f365\U0001f365\n       \U0001f365\U0001f365\U0001f365\U0001f365\U0001f365\n     \U0001f365\U0001f365\U0001f365\U0001f365\U0001f365\U0001f365\n  \U0001f365\U0001f365\U0001f365\U0001f365\U0001f365\U0001f365\U0001f365\n\U0001f365\U0001f35e\U0001f365\U0001f365\U0001f365\U0001f365\U0001f35e\U0001f365\n\U0001f365\U0001f35e\U0001f35e\U0001f35e\U0001f35e\U0001f35e\U0001f35e\n    \U0001f35e\U0001f35e\U0001f35e\U0001f35e\U0001f35e\U0001f35e\n          \U0001f35e\U0001f35e\U0001f35e\U0001f35e\n          \U0001f35e\U0001f35e\U0001f35e\U0001f35e\n          \U0001f35e\U0001f35e\U0001f35e\U0001f35e\n              \U0001f35e\U0001f35e\n               \U0001f35e\U0001f35e\n              \U0001f35e\U0001f35e",
                               
                               @"\u2754 \u2753 \u2754 \u2753 \u2754 \u2753 \u2744 \u2744\n\u2754 \u2753 \u2754 \u2753 \u2754 \u2753 \u2744 \u2744\n\u2754 \u2753 \u2754 \u2753 \u2754 \u2753 \u2744 \u2744\n\u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2744 \u2744\n\u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2615\n\u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2744 \u2615\n\u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2744 \u2615\n\u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2615 \u2744\n\u2744 \u2615 \u2615 \u2615 \u2615 \u2744 \u2744 \u2744\n\u2744 \u2744 \u2744 \u2744 \u2744 \u2744 \u2744 \u2744",
                               
                               @"     \U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\n\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\n\U0001f340\U0001f340\u2B50 \u2B50 \u2B50 \u2B50 \u2B50 \u2B50\n\U0001f363\U0001f363\U0001f363\U0001f363\u2B50 \u2B50 \u2B50 \U0001f363\n\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\u2B50 \U0001f363\n\U0001f340\U0001f340\U0001f340\U0001f340\U0001f340\U0001f340\U0001f340\U0001f340\n\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\n     \U0001f354\U0001f354\U0001f354\U0001f354\U0001f354\U0001f354",
                               
                               @"\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n  \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n  \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n  \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n  \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n    \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n    \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n    \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n\U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n  \U0001f363\U0001f363\u26AA \U0001f363 \u26AA \u26AA\n          I\U0002764bacon",
                               
                               @"\U0001f4ab\U0001f4ab\U0001f42e\U0001f42e\U0001f4ab\U0001f42e\U0001f42e\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f42e\U0001f42e\U0001f42e\U0001f42e\U0001f42e\U0001f42e\U0001f42e\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f42e\U0001f42e\U0001f42e\U0001f42e\U0001f42e\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f4ab\U0001f42e\U0001f42e\U0001f42e\U0001f4ab\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f4ab\U0001f42e\U0001f42e\U0001f42e\U0001f4ab\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f4ab\U0001f42e\U0001f42e\U0001f42e\U0001f4ab\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f357\U0001f363\U0001f363\U0001f356\U0001f356\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f357\U0001f363\U0001f363\U0001f356\U0001f356\U0001f4ab\U0001f4ab\n\U0001f4ab\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\U0001f4ab\n\U0001f4ab\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\U0001f4ab\n\U0001f4ab\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\U0001f4ab\n\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\n\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\n\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\n\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\n\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\n\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\n\U0001f4ab\U0001f357\U0001f363\U0001f363\U0001f363\U0001f363\U0001f356\U0001f356\U0001f4ab\n\U0001f4ab\U0001f4ab\U0001f357\U0001f357\U0001f357\U0001f356\U0001f356\U0001f4ab\U0001f4ab",
                               
                               @"     \u2601 \u2601\n\u2601 \u2601 \u2601 \u2601\n\U0001f37b\U0001f37b\U0001f37b\U0001f37b\U0001f37b\n\U0001f37b\U0001f37b\U0001f37b\U0001f37b     \U0001f37b\n\U0001f37b\U0001f37b\U0001f37b\U0001f37b          \U0001f37b\n\U0001f37b\U0001f37b\U0001f37b\U0001f37b          \U0001f37b\n\U0001f37b\U0001f37b\U0001f37b\U0001f37b     \U0001f37b\n\U0001f37b\U0001f37b\U0001f37b\U0001f37b\U0001f37b",
                               
                               @"\U0001f36f\U0001f369          \U0001f354\U0001f356\n\U0001f353\U0001f34a          \U0001f35e\U0001f363\n  \U0001f364\U0001f355    \U0001f34c\U0001f36f\n      \U0001f345\U0001f354\U0001f358\n          \U0001f34a\U0001f351\n          \U0001f36f\U0001f366",
                               
                               @"\U0001f34a\U0001f354          \U0001f34e\U0001f354\n\U0001f353\U0001f35e          \U0001f352\U0001f34a\n\U0001f36f\U0001f350          \U0001f356\U0001f35f\n\U0001f363\U0001f351          \U0001f351\U0001f35e\n\U0001f36a\U0001f34c          \U0001f36f\U0001f369\n    \U0001f36e\U0001f366\U0001f35e\U0001f356",
                               
                               @"\U0001f36f\U0001f34e          \U0001f351\U0001f366\n\U0001f35e\U0001f356\U0001f34a\U0001f353\U0001f352\U0001f36f\n\U0001f354\U0001f356  \U0001f351  \U0001f36f\U0001f34a\n\U0001f34c\U0001f355          \U0001f364\U0001f35e\n\U0001f366\U0001f354          \U0001f356\U0001f354\n\U0001f36f\U0001f366          \U0001f352\U0001f351",
                               
                               @"\U0001f367\U0001f34e          \U0001f351\U0001f366\n\U0001f35e\U0001f369\U0001f34a\U0001f353\U0001f352\U0001f36f\n\U0001f354\U0001f356  \U0001f351  \U0001f36f\U0001f34a\n\U0001f34c\U0001f355          \U0001f364\U0001f35e\n\U0001f366\U0001f354          \U0001f356\U0001f354\n\U0001f36f\U0001f366          \U0001f352\U0001f351", 
                               
                               @"\U0001f36f\U0001f369          \U0001f354\U0001f356\n\U0001f353\U0001f34a          \U0001f35e\U0001f363\n  \U0001f364\U0001f355    \U0001f34c\U0001f36f\n      \U0001f345\U0001f354\U0001f358\n          \U0001f34a\U0001f351\n          \U0001f36f\U0001f366",
                                                           nil];
    NSMutableArray* arrWeather = [[NSMutableArray alloc] initWithObjects:
                                  @"\u2601 \U0001f4a6 \u2601 \u2601 \u2601 \u2601 \u2601 \U0001f4a6\n\u2601 \u2601IT\'S RAINY\U0001f4a6\u2601\n\U0001f646\u2614 \u2614 \u2614 \u2614 \u2614 \u2614 \U0001f646\n\U0001f3e0\U0001f3e0\U0001f3ec\U0001f691\u26EA \U0001f697 \U0001f3c3 \U0001f302\n\U0001f3eb\U0001f3e2\U0001f699\U0001f302\U0001f3c3\U0001f3ea\U0001f6b2\U0001f3e8\n\U0001f342\U0001f343\U0001f342\U0001f343\U0001f342\U0001f343\U0001f342\U0001f343",
                                  
                                  @"\u26A1    \u2601 \u2601  \'  \' \u2601\n  \u2601  \'  \U0001f4a6 \'    \'\'  \u26A1\n\u2601 \'  \'  \'\'\u26A1\"  \u2601 \u2601 \uE049\n\'\'  \U0001f4a6  \'    \'    \'\'  \U0001f4a6\n\" \u2601  \u2614    \' \u2602 \u2602  \u2601\n\U0001f438\u261D \U0001f646\U0001f4a6  \u2614  \u2602\n\U0001f302\u24E1 \u24D0 \u24D8 \u24DD \u24E8 \u24D3 \u24D0 \u24E8 \U0001f4a6",
                                  
                                  @"\u2601 \u2601    \u2744    \u2744  *\n\u2744  *  \u2744  * \u2601 \u2601 \u2744\n    \u2744      Snowy\u2665*\n\u2744    *    \u2744    \u2744\nIt\'s beautiful\U0001f60d * \u2744\n  *      \u2744      \u2744  *\n\U0001f3e0\U0001f3e0    \u26C4 *  \U0001f46b  \u2744\n\uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3",
                                  
                                  @"\u2728 \u2600 \u2600 \u2600 \u2600 \u2600 \u2728 \u2600\n\U0001f3b6  SUNNY TODAY  \U0001f3b6\n\u2600 \u2600 \u2600 \u2728 \u2600 \u2600 \u2600 \u2728\n\U0001f338\U0001f337\U0001f411\U0001f340\U0001f335\U0001f334\U0001f33b\U0001f33a\n\U0001f339\U0001f33b\U0001f490\U0001f338\U0001f418\U0001f33e\U0001f340\U0001f41b\n\U0001f343\U0001f343\U0001f343\U0001f343\U0001f343\U0001f343\U0001f343\U0001f343\n\u2665 \U0001f497 \U0001f49b \U0001f49a \U0001f49c \U0001f499 \u2665 \U0001f497\n\U0001f604\U0001f603\U0001f604\U0001f603\U0001f604\U0001f603\U0001f604\U0001f603",
                                  
                                  @".    \U0001f300Snowing!\U0001f300\n.        \u26C4 \u26C4 \u26C4\n.      \u26C4~ . ~\u26C4\n.      \u26C4\\____/\u26C4\n.        \u26C4 \u26C4 \u26C4\n.  \U0001f44b. \U0001f380\U0001f380\U0001f380    \U0001f44d\n.      \\\u26C4 \u26C4 \u26C4 \u26C4 /\n.    \u26C4 \u26C4 \u26C4 \u26C4 \u26C4\n.    \u26C4 \u26C4 \u26C4 \u26C4 \U0001f493 \u26C4\n.      \u26C4 \u26C4 \u26C4 \u26C4 \u26C4\n.        \u26C4 \u26C4 \u26C4 \u26C4\n.  \U0001f300Drive Safely!!\U0001f300",
                                  
                                  @".                \uE314\n            \uE04B \uE04B \uE04B.  \uE331\n        \uE04B \uE04B \uE04B \uE04B \uE04B    \uE331\n      \uE04B \uE04B \uE04B \uE04B \uE04B \uE04B \uE331\n    \"Take ur Umbrella\"\n                  J  \uE04F \uE052",
                                  
                                  @"\uE443 \uE443 \uE443 \uE443 \uE443 \uE443 \uE443\n\uE443 \uE049 \uE049 \uE04A \uE049 \uE049 \uE443\n\uE443 \uE049 \uE049 \uE049 \uE049 \uE049 \uE443\n\uE443good\uE049 \U000e049day\uE443\n\uE443 \uE049 \uE049 \uE049 \uE049 \uE049 \uE443\n\uE443God \U000e049bless!!\uE443\n\uE443 \uE049 \uE049 \uE049 \uE049 \uE049 \uE443\n\uE443 \uE049 \uE049 \uE037 \uE049 \uE049 \uE443\n\uE443 \uE443 \uE443 \uE443 \uE443 \uE443 \uE443",
                                  
                                  @"\U0001f343\U0001f4a8\n               \U0001f342\U0001f4a8\n\U0001f617\n\U0001f1ee\U0001f1f9\U0001f1ee\U0001f1f9\U0001f1ee\U0001f1f9\n\U0001f455\n\U0001f456          \U0001f342\U0001f343\U0001f4a8\n\U0001f45f\U0001f45f\n\U0001f33e\U0001f33e\U0001f33e\U0001f33e\U0001f33e\U0001f33e",
                                  
                                  @"\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \U0001f4a6\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \U0001f4a6 \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \U0001f4a6 \U0001f4a6 \u2601\n\u2601 \u2601 \u2601 \u2601 \U0001f4a6 \U0001f4a6 \u2601 \u2601\n\u2601 \u2601 \u2601 \U0001f4a6 \U0001f4a6 \u2601 \u2601 \u2601\n\u2601 \u2601 \U0001f4a6 \U0001f4a6 \U0001f4a6 \u2601 \u2601 \u2601\n\u2601 \U0001f4a6 \U0001f4a6 \U0001f4a6 \u2601 \u2601 \u2601 \u2601\n\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\u2601 \u2601\n\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\U0001f4a6\u2601\n\u2601 \u2601 \u2601 \u2601 \U0001f4a6 \U0001f4a6 \U0001f4a6 \u2601\n\u2601 \u2601 \u2601 \U0001f4a6 \U0001f4a6 \U0001f4a6 \u2601 \u2601\n\u2601 \u2601 \u2601 \U0001f4a6 \U0001f4a6 \u2601 \u2601 \u2601\n\u2601 \u2601 \U0001f4a6 \U0001f4a6 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f4a6 \U0001f4a6 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f4a6 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\U0001f4a6\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601",
                                  
                                  @"\U0001f4a7\U0001f4a7\U0001f4a7\U0001f4a7\U0001f4a7\U0001f4a7\U0001f4a7\u2666\n\U0001f4a7\U0001f629\U0001f4a7\U0001f4a7\U0001f4a7\U0001f4a7\u2666 \u2600\n\U0001f447\U0001f455\U0001f44e\U0001f4a7\U0001f4a7\u2666 \u2600 \u2600\n\U0001f4a7\U0001f456\U0001f4a7\U0001f4a7\u2666 \u2600 \u2600 \u2600\n\U0001f4a7\U0001f45f\U0001f45f\u2666 \u2600 \U0001f603 \u2600 \u2600\n\U0001f4a7\U0001f4a7\u2666 \u2600 \U0001f446 \U0001f45a \U0001f44d \u2600\n\U0001f4a7\u2666 \u2600 \u2600 \u2600 \U0001f456 \u2600 \u2600\n\u2666 \u2600 \u2600 \u2600 \u2600 \U0001f45f \U0001f45f \u2600",
                                  
                                  @"\uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119\n\uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118\n\uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119\n\uE119 \uE119 \uE119 \uE119HAPPY\uE119 \uE119 \uE119\n\uE118 \uE118 \uE118 \U000e118fALL!\uE118 \uE118 \uE118 \uE118\n\uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119\n\uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118\n\uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119 \uE118 \uE119",
                                  nil];
    
    NSMutableArray* arrSport = [[NSMutableArray alloc] initWithObjects:
                                @"\U0001f3b1\u2796 \u270A \u2796 \u2796 \u270A \u2796 \U0001f3b1\n\U0001f31f        \\        /          \U0001f31f\n\u2B50          \\\U0001f601/            \u2B50\n\u2728           \U0001f3bd             \u2728\n               /    \\\n            \U0001f45f    \U0001f45f",
                                
                                @"\U0001f64b\U0001f46c\U0001f46a\U0001f46a\U0001f6b6\U0001f6b6\U0001f64b\U0001f64b\n\U0001f46c\U0001f18e\U0001f6b6\U0001f646\U0001f46a\U0001f6b6\U0001f46a\U0001f46a\n\U0001f6b6\U0001f64b\U0001f46c\U0001f46a\U0001f46a\U0001f171\U0001f46c\U0001f46a\n\n            \U0001f607        \U0001f62f\n\U0001f3c1       \U0001f455        \U0001f45a     \U0001f3c1\n     \u26BD \U0001f45f \U0001f45f   \U0001f45f\U0001f45f\n\n\U0001f46c\U0001f170\U0001f6b6\U0001f646\U0001f46a\U0001f6b6\U0001f46a\U0001f46a\n\U0001f64b\U0001f46c\U0001f46a\U0001f46a\U0001f6b6\U0001f6b6\U0001f64b\U0001f64b\n\U0001f6b6\U0001f64b\U0001f46c\U0001f46a\U0001f46a\U0001f18e\U0001f46c\U0001f46a",
                                
                                @"          \U0001f310\U0001f310\U0001f310\U0001f310\n\U0001f340\U0001f340\U0001f310    \U0001f6b6\U0001f310\U0001f340\U0001f340\n\U0001f340                              \U0001f340\n\U0001f340    \U0001f3c3                    \U0001f340\n\U0001f340              \u26BD \U0001f3c3    \U0001f340\n\U0001f340              \U0001f3c3          \U0001f340\n\U0001f340    \U0001f3c3                    \U0001f340\n\U0001f340                              \U0001f340\n\U0001f340                    \U0001f3c3    \U0001f340\n\U0001f340                              \U0001f340\n\U0001f340\U0001f340\U0001f310\U0001f6b6    \U0001f310\U0001f340\U0001f340\n          \U0001f310\U0001f310\U0001f310\U0001f310",
                                
                                @"        \u2728 \U0001f31f \U0001f3be\n     \u2728                         \U0001f310\n\U0001f310     \U0001f603          \U0001f62e\U0001f511\n     \u2712 \U0001f455          \U0001f45a\n          \U0001f456   \U0001f6a7  \U0001f456\n        \U0001f45f\U0001f45f     \U0001f45f\U0001f45f",
                                
                                @"\U0001f3c6\U0001f3c7\U0001f514\U0001f451\U0001f478\U0001f514\U0001f3c7\U0001f3c6\n\U0001f617\U0001f600\U0001f612\U0001f61e\U0001f636\U0001f610\U0001f615\U0001f619\n\u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C\n\u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC\n\u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C\n\u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC\n\u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C \u25FC \u2B1C\n\U0001f630\U0001f628\U0001f631\U0001f607\U0001f628\U0001f62d\U0001f630\U0001f628\n\U0001f3c6\U0001f3c7\U0001f514\U0001f478\U0001f451\U0001f514\U0001f3c7\U0001f3c6",
                                
                                @"\U0001f46c\U0001f46c\U0001f46c\U0001f46b\U0001f46b\U0001f46b\U0001f46b\U0001f46c\n\u25FD \u25FD \u25FD \u25FD \u25FD \u25FD \u25FD \u25FD\n\u25FD \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \u25FD\n\u25FD \u3030 \U0001f534 \u3030 \u3030 \U0001f534 \u3030 \u25FD\n\u25FD \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \u25FD\n\u25FD \u3030 \u3030 \U0001f534 \u3030 \u3030 \U0001f534 \u25FD\n\u25FD \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \U0001f3ca \u25FD\n\u25FD \u25FD \u25FD \u25FD \u25FD \u25FD \u25FD \u25FD",
                                
                                @"\U0001f3c8\U0001f3c8    \U0001f31f\U0001f3c8\U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f3c8    \U0001f31f    \U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f3c8\U0001f3c8\U0001f31f    \U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8    \U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f3c8    \U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f3c8    \U0001f31f\U0001f3c8\U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f3c8    \U0001f31f    \U0001f3c8\U0001f3c8\n              \U0001f31f\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764\n\u2764 \u2764    \U0001f31f          \u2764\n\u2764 \u2764    \U0001f31f\u2764\n\u2764 \u2764 \u2764 \u2764 \u2764    \U0001f31f\n\u2764 \u2764 \u2764 \u2764 \u2764\n\u2764 \u2764    \U0001f31f\u2764    \U0001f31f\n\u2764 \u2764    \U0001f31f\n\u2764 \u2764    \U0001f31f          \U0001f31f\n              \U0001f31f\n\U0001f3c6\U0001f3c6    \U0001f31f          \U0001f31f\n\U0001f3c6\U0001f3c6    \U0001f31f    \U0001f3c8\n\U0001f3c6\U0001f3c6    \U0001f31f          \U0001f31f\n\U0001f3c6\U0001f3c6    \U0001f31f    \U0001f499\n\U0001f3c6\U0001f3c6    \U0001f31f          \U0001f31f\n\U0001f3c6\U0001f3c6    \U0001f31f\n\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\n\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6\U0001f3c6",
                                
                                @"    \U0001f525\U0001f525\U0001f525\n    \u26AB \u26AB \u26AB\n    \u26AB \U0001f440 \u26AB          \U0001f3c0\n    \u26AB \U0001f445 \u26AB          \u26AB\n          \u25FE              \u26AB\n\u26AB \u2705 \u26AB \u2705 \u26AB \u26AB \u25FE\n\u26AB \u2705 \u2705 \u2705\n\u26AB \u2705 \u2705 \u2705    \U0001f4a2\U0001f4a2\U0001f4a2\n\u26AB \u2705 \u2705 \u2705    \U0001f610\U0001f610\U0001f610\n\u26AB \U0001f17f \U0001f17f \U0001f17f    \U0001f610\U0001f440\U0001f610\n\U0001f44a\U0001f17f    \U0001f17f    \U0001f610\U0001f3a3\U0001f610\n    \U0001f17f    \U0001f17f          \U0001f610\n    \U0001f17f    \U0001f17f    \U0001f610\u2734 \U0001f610\n    \u26AB    \u26AB    \U0001f610\u2734 \U0001f610\n    \u26AB    \u26AB    \U0001f447\u2734 \U0001f447\n    \u25FE    \u25FE    \U0001f17f\U0001f17f\U0001f17f\n    \u25FE    \u25FE    \U0001f17f    \U0001f17f\n    \u25FE    \u25FE    \U0001f610    \U0001f610\n    \U0001f45f    \U0001f45f    \U0001f461    \U0001f461",
                                
                                @"\u2764 \u2764 \u2764 \u2764 \u2764 \u2764\n\u2764Love sports!\u2764\n\U0001f3be\U0001f3b1\u26BE \u26BD \u26BD \U0001f3c8\n\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\n\U0001f6b4\U0001f3ca\U0001f3c2\U0001f3c4\u26F3 \U0001f3c7\n\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\n\U0001f3c8\U0001f3c0\u26BD \u26BE \U0001f3b1 \U0001f3be\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2764      \U0001f6a6\U0001f6a6\U0001f6a6\U0001f6a6\n      \u26AB \u26AB \u26AB \u26AB\n    \u26AB \u26AB \U0001f440 \u26AB \u26AB\n    \u26AB \u26AB \U0001f445 \u26AB \u26AB\n\u26AB  \u26AB \u26AB \u26AB \u26AB\n\u26AB \U0001f455 \U0001f455 \U0001f455 \U0001f455 \U0001f455 \u26AB\n          \U0001f455\U0001f455\U0001f455    \u26AB\n          \U0001f455\U0001f455\U0001f455  \U0001f3c0\U0001f3c0\n          \U0001f455\U0001f455\U0001f455\U0001f3c0\U0001f3c0\U0001f3c0\n          \u2764 \u2764 \u2764  \U0001f3c0\U0001f3c0\n          \u2764    \u2764\n          \u26AB    \u26AB\n          \u26AB    \u26AB\n          \u26AB    \u26AB\n          \U0001f45f    \U0001f45f",
                                                              nil];
    
    NSMutableArray* arrSchool = [[NSMutableArray alloc] initWithObjects:
                                 @"\U0001f551          \u2705 \u2747 \u274E \u274E\n               \u274E \u2705 \U0001f4b9 \u2747\n     \U0001f468     \u2705 \U0001f60a \U0001f4c4 \U0001f4c4\n\U0001f4bc\U0001f454\U0001f44c     \U0001f455\U0001f4c4\U0001f171\n\U0001f4fc\U0001f4fc\U0001f4fc     \U0001f456\n\u2B1B \u2B1B \u2B1B     \U0001f45f\U0001f45f",
                                 
                                 @"\u2B50 \u2B50 \u2B50 \u2B50 \u2B50 \U0001f380 \u2B50\n\U0001f380\U0001f380\U0001f380\u2B50 \U0001f380 \U0001f380 \U0001f380\n\U0001f380\u2B50 \u2B50 \U0001f380 \u2B50 \U0001f380 \u2B50\n\U0001f380\u2B50 \u2B50 \U0001f380 \u2B50 \u2B50 \u2B50\n\U0001f380\u2B50 \u2B50 \U0001f380 \u2B50 \u2B50 \u2B50\n\U0001f380\U0001f380\U0001f380\u2B50 \u2B50 \u2B50 \u2B50\n\U0001f380\u2B50 \u2B50 \U0001f380 \u2B50 \u2B50 \u2B50\n\U0001f380\u2B50 \u2B50 \U0001f380 \u2B50 \u2B50 \u2B50\n\U0001f380\u2B50 \u2B50 \U0001f380 \u2B50 \u2B50 \u2B50\n\U0001f380\U0001f380\U0001f380\u2B50 \u2B50 \u2B50 \u2B50",
                                 
                                 @"\u2B50 \u2B50 \u2B50 \u2B50 \u2B50 \u2B50 \u2B50\n\U0001f61e\U0001f61e\u2B50 \u2B50 \U0001f61e \U0001f61e \U0001f61e\n\U0001f61e\u2B50 \U0001f61e \u2B50 \u2B50 \u2B50 \u2B50\n\U0001f61e\u2B50 \u2B50 \U0001f61e \u2B50 \u2B50 \u2B50\n\U0001f61e\u2B50 \u2B50 \U0001f61e \u2B50 \u2B50 \u2B50\n\U0001f61e\u2B50 \u2B50 \U0001f61e \u2B50 \u2B50 \u2B50\n\U0001f61e\u2B50 \U0001f61e \u2B50 \u2B50 \u2B50 \u2B50\n\U0001f61e\U0001f61e\u2B50 \u2B50 \u2B50 \u2B50 \u2B50",
                                 
                                 @"\U0001f691\U0001f691\U0001f691\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\n\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\U0001f691\U0001f691\U0001f691\n\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\n\U0001f691\U0001f691\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\n\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\n\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\n\U0001f691\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad\U0001f4ad",
                                 
                                 @"\uE110 \uE110 \uE110 \uE110 \uE110 \uE110 \uE110\n\uE110 \uE110 \uE110 \uE503 \uE110 \uE110 \uE110\n\uE110 \uE110 \uE148 \uE148 \uE148 \uE110 \uE110\n\uE110 \uE148 \uE148 \uE148 \uE148 \uE148 \uE110\n\uE110 \uE32C \uE32C \uE32C \uE32C \uE32C \uE110\n\uE110 \uE32C \uE32C \uE32C \uE32C \uE32C \uE110\n\uE110 \uE32C \uE32C \uE32C \uE32C \uE32C \uE110\n\uE110 \uE32C \uE32C \uE32C \uE32C \uE32C \uE110\n\uE110 \uE32C \uE32C \uE32C \uE32C \uE32C \uE110\n\uE110 \uE32C \uE32C \uE32C \uE32C \uE32C \uE110\n\uE110 \uE441 \uE441 \uE441 \uE441 \uE441 \uE110\n\uE110 \uE441 \uE441 \uE441 \uE441 \uE441 \uE110\n\uE110 \uE349 \uE349 \uE349 \uE349 \uE349 \uE110\n\uE110 \uE110 \uE110 \uE110 \uE110 \uE110 \uE110\n\uE110 \u24C8 \u24C9 \u24CA \u24B9 \u24CE \uE326 \uE110\n\uE110 \uE057 \u24BD \u24B6 \u24C7 \u24B9 \uE057 \uE110\n\uE110 \uE301 \uE301 \uE301 \uE301 \uE301 \uE110\n\uE110 \uE110 \uE110 \uE110 \uE110 \uE110 \uE110",
                                 
                                 @"\U0001f60b\U0001f170\U0001f170\U0001f60b\U0001f60b\U0001f60b\U0001f60b\U0001f60b\n\U0001f170\U0001f60b\U0001f60b\U0001f170\U0001f60b\U0001f60b\U0001f170\U0001f60b\n\U0001f170\U0001f60b\U0001f60b\U0001f170\U0001f60b\U0001f170\U0001f170\U0001f170\n\U0001f170\U0001f170\U0001f170\U0001f170\U0001f60b\U0001f60b\U0001f170\U0001f60b\n\U0001f170\U0001f60b\U0001f60b\U0001f170\U0001f60b\U0001f60b\U0001f60b\U0001f60b\n\U0001f170\U0001f60b\U0001f60b\U0001f170\U0001f60b\U0001f60b\U0001f60b\U0001f60b",
                                 
                                 @"\u2744 \u2744 \U0001f4da \U0001f4da \U0001f4da \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f4da \u2744 \u2744 \U0001f4da \u2744 \u2744\n\u2744 \u2744 \U0001f4da \u2744 \u2744 \U0001f4da \u2744 \u2744\n\u2744 \u2744 \U0001f4da \U0001f4da \U0001f4da \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f4da \u2744 \u2744 \U0001f4da \u2744 \u2744\n\u2744 \u2744 \U0001f4da \u2744 \u2744 \U0001f4da \u2744 \u2744\n\u2744 \u2744 \U0001f4da \U0001f4da \U0001f4da \u2744 \u2744 \u2744",
                                 
                                 @"\uE32E \uE32E \uE32E \uE32E \uE32E \uE32E \uE219 \uE32E\n\uE32E \uE32E \uE219 \uE219 \uE32E \uE219 \uE219 \uE219\n\uE32E \uE219 \uE219 \uE219 \uE219 \uE32E \uE219 \uE32E\n\uE219 \uE219 \uE32E \uE32E \uE219 \uE219 \uE32E \uE32E\n\uE219 \uE219 \uE219 \uE219 \uE219 \uE219 \uE32E \uE32E\n\uE219 \uE219 \uE219 \uE219 \uE219 \uE219 \uE32E \uE32E\n\uE219 \uE219 \uE32E \uE32E \uE219 \uE219 \uE32E \uE32E\n\uE219 \uE219 \uE32E \uE32E \uE219 \uE219 \uE32E \uE32E\n\uE327YOU CAN GET IT\uE327 \U0001f557          \u2705 \u2747 \u274E \u274E\n               \u274E \u2705 \U0001f4b9 \u2747\n     \U0001f469     \u2705 \U0001f62b \U0001f4c4 \U0001f4c4\n\U0001f4da\U0001f45a\U0001f44e     \U0001f455\U0001f4c4F-\n\U0001f4fc\U0001f4fc\U0001f4fc     \U0001f456\n\u2B1B \u2B1B \u2B1B     \U0001f45f\U0001f45f",
                                                               nil];
    
    NSMutableArray* arrNature = [[NSMutableArray alloc] initWithObjects:
                                 @"\U0001f41d\u2601 \u2601 \u2601 \U0001f41d \u2601\n\u2601 \U0001f41d \u2601 \u2601 \u2601 \u2601\n\u2601 \u2601 \u2601 \u2601 \U0001f41d \u2601\n\u2601 \U0001f36f \U0001f36f \U0001f36f \U0001f36f \u2601\n\u2601 \U0001f36f \U0001f41d \U0001f41d \U0001f41d \u2601\n\u2601 \U0001f36f \U0001f41d \U0001f41d \U0001f36f \u2601\n\u2601 \U0001f36f \U0001f36f \U0001f36f \U0001f36f \u2601\n\u2601 \U0001f36f \u2601 \u2601 \U0001f36f \u2601\n\U0001f340\U0001f340\U0001f340\U0001f33a\U0001f340\U0001f340\n\U0001f340\U0001f33a\U0001f340\U0001f340\U0001f340\U0001f33a",
                                 
                                 @"     \U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n  \U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\n\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\n  \U0001f337\U0001f337\U0001f337\U0001f337\U0001f337\U0001f337\n    \U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n       \U0001f340\U0001f340\U0001f340\U0001f340\n           \U0001f40a\U0001f40a\n              \U0001f331\n         \U0001f343\U0001f331\n              \U0001f331",
                                 
                                 @"              \U0001f33b\U0001f33b\n\U0001f33b    \U0001f33b\U0001f33b\U0001f33b\U0001f33b    \U0001f33b\n    \U0001f33b\U0001f33b\U0001f3b1\U0001f3b1\U0001f33b\U0001f33b\n\U0001f33b\U0001f33b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f33b\U0001f33b\n    \U0001f33b\U0001f33b\U0001f3b1\U0001f3b1\U0001f33b\U0001f33b\n\U0001f33b    \U0001f33b\U0001f33b\U0001f33b\U0001f33b    \U0001f33b\n              \U0001f33b\U0001f33b\n              \U0001f335\U0001f335\n    \U0001f60a    \U0001f335\U0001f335    \U0001f60a\n    \U0001f335    \U0001f335\U0001f335    \U0001f335\n\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335\U0001f335",
                                 
                                 @"\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\n\U0001f436\U0001f43e\U0001f43e\U0001f436\U0001f43e\U0001f43e\U0001f436\n\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\n\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\n\U0001f436\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f43e\U0001f436\n\U0001f436\U0001f436\U0001f43e\U0001f43e\U0001f43e\U0001f436\U0001f436\n\U0001f436\U0001f436\U0001f436\U0001f43e\U0001f436\U0001f436\U0001f436\n\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436\U0001f436",
                                 
                                 @"\U0001f43d\U0001f437\U0001f437\U0001f437\U0001f437\U0001f437\U0001f43d\n\U0001f437\U0001f43c\U0001f43c\U0001f437\U0001f43c\U0001f43c\U0001f437\n\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\n\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\n\U0001f437\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f43c\U0001f437\n\U0001f437\U0001f437\U0001f43c\U0001f43c\U0001f43c\U0001f437\U0001f437\n\U0001f43d\U0001f437\U0001f437\U0001f43c\U0001f437\U0001f437\U0001f43d",
                                 
                                 @"          \u26AB \U0001f436 \U0001f436 \U0001f436 \u26AB\n        \u26AB \U0001f436 \U0001f436 \U0001f436 \U0001f436 \u26AB\n        \u26AB \U0001f3b1 \U0001f436 \U0001f436 \U0001f3b1 \u26AB\n            \U0001f436\U0001f436\U0001f3a5\U0001f436\n          \U0001f436\U0001f436\U0001f445\U0001f436\n        \U0001f436\U0001f436\U0001f436\U0001f436\n        \U0001f436\u25FE \U0001f436 \u25FE\n\U0001f4a9 \u27B0 \U0001f436 \u26AB \U0001f436 \u26AB",
                                 
                                 @"     \U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\n\U0001f330\U0001f330\U0001f315\U0001f315\U0001f315\U0001f315\U0001f330\U0001f330\n\U0001f330\U0001f330\U0001f315\U0001f315\U0001f315\U0001f315\U0001f330\U0001f330\n\U0001f330\U0001f330\U0001f3b1\U0001f315\U0001f315\U0001f3b1\U0001f330\U0001f330\n     \U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\n       \U0001f315\U0001f315\U0001f497\U0001f315\U0001f315\n            \U0001f315\U0001f445\U0001f315\n           \U0001f315\U0001f315\U0001f315\U0001f315\n     \U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\n   \U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\U0001f315\n\U0001f315\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f315\n\U0001f315\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f315\n\U0001f315\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f315\n\U0001f330\U0001f330\U0001f330\U0001f315\U0001f315\U0001f330\U0001f330\U0001f330\n\n      \U0001f4bf\U0001f4bf        \U0001f4bf\U0001f4bf\n        \U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\n      \U0001f4bf\U0001f4bf        \U0001f4bf\U0001f4bf",
                                 
                                 @"\U0001f552\U0001f331\U0001f331\U0001f331\U0001f331\U0001f331\U0001f331\U0001f552\n\U0001f552\U0001f552\U0001f331\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\n\U0001f331\U0001f552\U0001f552\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\n\U0001f331\U0001f552\U0001f552\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\U0001f331\n\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\U0001f331\n\U0001f552\U0001f552\U0001f340\U0001f552\U0001f552\U0001f340\U0001f552\U0001f331\n\U0001f552\U0001f552\U0001f552\U0001f552\u3030 \U0001f552 \U0001f552 \U0001f331\n\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\n\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\U0001f331\n\U0001f331\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f331\n\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\n\U0001f331\U0001f331\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552\U0001f552",
                                 
                                 @"                    \U0001f380\n        \U0001f424\U0001f424\U0001f424\U0001f424\n     \U0001f424\U0001f3b1\U0001f424\U0001f424\U0001f424\n\U0001f53a\U0001f424\U0001f424\U0001f424\U0001f424\U0001f424\n        \U0001f424\U0001f424\U0001f424\U0001f424\n               \U0001f424\U0001f424          \U0001f424\n          \U0001f424\U0001f424\U0001f4ab\U0001f424\U0001f424\U0001f424\n          \U0001f424\U0001f424\U0001f424\U0001f4ab\U0001f424\n             \U0001f424\U0001f424\U0001f424\U0001f424\n                    \U0001f4cd\U0001f4cd\n                    \U0001f53a\U0001f53a",
                                 
                                 @"          \u2601 \u2601\n\U0001f363\u2601 \U0001f3b1 \u2601 \u2601\n          \u2601 \u2601 \u2601\n               \u2601 \u2601\n          \u2601 \u2601               \u2601\n     \u2601 \u2601               \u2601 \u2601\n\u2601 \u2601               \u2601 \u2601 \u2601\n\u2601 \u2601     \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n     \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\U0001f300\U0001f300\u2601 \u2601 \u2601 \u2601 \U0001f300 \U0001f300\n\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\n\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300",
                                 
                                 @"\u2744 \u2744 \U0001f4cd \U0001f4cd \u2744 \u2744 \u2744 \u2744\n\U0001f315\U0001f315\U0001f440\U0001f315\U0001f31b\u2744 \u2744 \u2744\n\u2693 \U0001f315 \U0001f315 \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f31e \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f31e \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f31e \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f31e \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f315 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f315 \U0001f31e \u2744 \u2744 \u2744 \U0001f6a9\n\u2744 \u2744 \U0001f315 \U0001f31e \U0001f31e \U0001f315 \U0001f315 \U0001f315\n\u2744 \u2744 \U0001f315 \U0001f315 \U0001f315 \U0001f315 \U0001f31e \U0001f31e\n\u2744 \u2744 \U0001f315 \u2744 \U0001f315 \u2744 \U0001f31e \U0001f31e\n\u2744 \u2744 \U0001f315 \u2744 \U0001f31e \u2744 \U0001f31e \U0001f315\n\u2744 \u2744 \U0001f31e \u2744 \U0001f315 \u2744 \U0001f31e \U0001f315\n\u2744 \u2744 \U0001f31e \u2744 \U0001f315 \u2744 \U0001f31e \U0001f315\n\u2744 \u2744 \U0001f330 \u2744 \U0001f330 \u2744 \U0001f330 \U0001f330",
                                 
                                 @"    \U0001f63c                    \U0001f63c\n\U0001f63c    \U0001f63c\U0001f63c\U0001f63c\U0001f63c    \U0001f63c\n\U0001f63c                              \U0001f63c\n\U0001f63c                    \U0001f4a2    \U0001f63c\n\U0001f63c    \U0001f3b1          \U0001f3b1    \U0001f63c\n\U0001f63c                              \U0001f63c\n\U0001f63c            \U0001f344            \U0001f63c\n\U0001f63c            ^              \U0001f63c\n\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c\U0001f63c",
                                 
                                 @"        \U0001f53a            \U0001f53a\n       \U0001f314\U0001f315\U0001f315\U0001f315\U0001f316\n\u2712 \U0001f314 \U0001f30d \U0001f315 \U0001f315 \U0001f30f \U0001f316 \U0001f526\n\u2796 \U0001f314 \U0001f315 \U0001f497 \U0001f315 \U0001f315 \U0001f316 \u2796\n\U0001f526\U0001f314\U0001f315\U0001f445\U0001f315\U0001f315\U0001f316\u2712\n       \U0001f314\U0001f315\U0001f315\U0001f315\U0001f316",
                                 
                                 @"     \u26AB \u26AB          \u26AB \u26AB\n     \u26AB \U0001f55c \u26AA \u26AA \U0001f566 \u26AB\n\u26AA \u26AA \u26AA \u26AA \u26AA \u26AA \u26AA \U0001f566\n\u26AA \u26AB \U0001f3b1 \u26AA \u26AA \U0001f3b1 \u26AB \U0001f567\n\u26AB \u26AB \u26AA \u26AA \u26AA \u26AA \u26AB \u26AB\n\u26AB \u26AA \u26AA \u25FC \u25FC \u26AA \u26AA \u26AB\n     \u26AA \u26AA \u3030 \u3030 \u26AA \U0001f557\n          \U0001f562\U0001f562\U0001f562\U0001f557",
                                                                nil];
    
    NSMutableArray* arrFun = [[NSMutableArray alloc] initWithObjects:
                              @"     \U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\n   \U0001f300\U0001f300\U0001f300\U0001f300\U0001f380\U0001f300\n\U0001f300\U0001f300     \U0001f440     \U0001f300\U0001f300\n\U0001f300\U0001f300     \U0001f443     \U0001f300\U0001f300\n\U0001f300\U0001f300     \U0001f445     \U0001f300\U0001f300\n          \U0001f4e0\U0001f4e0\U0001f4e0\n   \U0001f498\U0001f49d\U0001f49d\U0001f49d\U0001f49d\U0001f49d\n\U0001f498     \U0001f498\U0001f49d\U0001f49d     \U0001f49d\n\U0001f498     \U0001f498\U0001f49d\U0001f49d     \U0001f49d\n\U0001f447     \u25FD \U0001f380 \u25FD     \U0001f447\n          \U0001f498     \U0001f49d\n          \U0001f498     \U0001f49d\n          \U0001f498     \U0001f49d\n          \U0001f45f     \U0001f45f",
                              
                              @"\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \U0001f604 \U0001f60a \u2601\n\u2601 \U0001f604 \U0001f60a \U0001f604 \U0001f60a \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \U0001f604 \U0001f60a \u2601\n\U0001f604\U0001f60a\u2601 \u2601 \U0001f604 \U0001f60a\n\U0001f604\U0001f60a\u2601 \u2601 \U0001f604 \U0001f60a\n\U0001f60a\U0001f604\u2601 \u2601 \U0001f604 \U0001f60a\n\U0001f604\U0001f604\u2601 \u2601 \U0001f604 \U0001f604\n\u2601 \U0001f604 \U0001f60a \U0001f604 \U0001f60a \u2601\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \u2601 \u2601 \u2601\n\u2601 \U0001f604 \U0001f60a \U0001f604 \U0001f60a \u2601\n\u2601 \U0001f604 \U0001f60a \U0001f604 \U0001f60a \u2601",
                              
                              @"          \U0001f318\U0001f6aa\U0001f312\n          \U0001f318\U0001f6aa\U0001f312\n          \U0001f318\U0001f6aa\U0001f312\n               \U0001f6aa\n   \U0001f17e       \U0001f6aa\n\U0001f170          \U0001f6aa       \U0001f18e\n\U0001f170\U0001f170\U0001f170\U0001f6aa\U0001f170\U0001f18e\n     \U0001f170\U0001f170\U0001f6aa\U0001f170\U0001f18e\n          \U0001f170\U0001f6aa\U0001f18e\n     \U0001f170\U0001f170\U0001f6aa\U0001f170\U0001f18e\n\U0001f170\U0001f170\U0001f170\U0001f518\U0001f170\u2B55 \U0001f18e\n\U0001f170\U0001f170\U0001f170\U0001f6a5\U0001f171\u2B55 \U0001f18e\n\U0001f171\U0001f171\U0001f171\U0001f171\U0001f171\U0001f4cd\U0001f18e\n     \U0001f171\U0001f171\U0001f171\U0001f18e\U0001f18e",
                              
                              @"          \U0001f480\U0001f480\U0001f480\n     \U0001f480\U0001f480\U0001f480\U0001f480\U0001f480\n     \U0001f480\U0001f3b1\U0001f480\U0001f3b1\U0001f480\n     \U0001f480\U0001f53b\U0001f53b\U0001f53b\U0001f480\n          \U0001f480\U0001f480\U0001f480\n               \U0001f480\n     \U0001f480\U0001f480\U0001f480\U0001f480\U0001f480\n\U0001f480     \U0001f480\U0001f480\U0001f480     \U0001f480\n\U0001f480     \U0001f480\U0001f480\U0001f480     \U0001f480\n\U0001f480          \U0001f480          \U0001f480\n\U0001f480          \U0001f480          \U0001f480\n\U0001f447          \U0001f480          \U0001f447\n               \U0001f480\n     \U0001f480\U0001f480\U0001f480\U0001f480\U0001f480\n     \U0001f480\U0001f480\U0001f480\U0001f480\U0001f480\n     \U0001f480     \U0001f480     \U0001f480\n     \U0001f480               \U0001f480\n     \U0001f480               \U0001f480\n          \U0001f480               \U0001f480\n     \U0001f480               \U0001f480\n     \U0001f480               \U0001f480\n     \U0001f480               \U0001f480\n     \U0001f480\U0001f480          \U0001f480\U0001f480",
                              
                              @"\u2712          \U0001f526\n     \u2712 \U0001f526\n\U0001f4fa\U0001f4fa\U0001f4fa\U0001f4fa\U0001f4fa\n\U0001f4fa\U0001f4ad\U0001f47d\U0001f4ad\U0001f4fa     \U0001f648\n\U0001f4fa\U0001f4ad\U0001f458\U0001f4ad\U0001f4fa     \U0001f455\n\U0001f4fa\U0001f4fa\U0001f4fa\U0001f4fa\U0001f4fa     \U0001f4ba\n\U0001f6aa               \U0001f6aa  \U0001f461\U0001f461",
                              
                              @"     \u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \U0001f380\n\u2764 \u2764 \U0001f49b \U0001f49b \U0001f49b \U0001f49b \u2764 \u2764\n\u2764 \U0001f49b \U0001f49b \U0001f49b \U0001f440 \U0001f49b \U0001f49b \u2764\n\u2764 \U0001f49b \U0001f49b \U0001f49b \U0001f443 \U0001f49b \U0001f49b \u2764\n\u2764 \U0001f49b \U0001f49b \U0001f49b \U0001f48b \U0001f49b \U0001f49b \u2764\n\u2764     \U0001f49b\U0001f49b\U0001f49b\U0001f49b     \u2764\n\u2764          \U0001f49b               \u2764\n\u2764 \U0001f338 \U0001f49b \U0001f49b \U0001f49b \U0001f338 \U0001f49b \u2764\n\u2764 \U0001f338 \U0001f49b \U0001f49b \U0001f49b \U0001f338 \U0001f49b \u2764\n\U0001f49b\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f49b\n\U0001f49b\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f49b\n\U0001f49b\U0001f338\U0001f48b\U0001f48b\U0001f48b\U0001f48b\U0001f338\U0001f49b\n\U0001f49b\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f49b\n\U0001f49b\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f49b\n\U0001f45b\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\U0001f49b\n     \U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf          \U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf         \U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf          \U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf         \U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf          \U0001f4bf\U0001f4bf\n     \U0001f4bf\U0001f4bf          \U0001f4bf\U0001f4bf\n        \U0001f460               \U0001f460",
                              
                              @"          \U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\n    \U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\U0001f4bf\n    \U0001f4bf\U0001f4bf\u26AB \U0001f440 \u26AB \U0001f4bf \U0001f4bf\n          \u26AB \u26AB \u2B55 \u26AB \u26AB\n            \u26AB \u26AB \u26AB \u26AB      \U0001f3a4\n                \u2734 \u2734        \u26AB\n    \u26AB \u2734 \u2734 \u2734 \u2734 \u2734 \u26AB\n  \u26AB      \u2734 \u2734 \u2734\n\u26AB          \U0001f3b5\U0001f3b5\U0001f3b5\n              \u2734 \u2734 \u2734\n              \U0001f4c0\U0001f4c0\U0001f4c0\n            \U0001f4c0\U0001f4c0\U0001f4c0\U0001f4c0\n          \U0001f4c0\U0001f4c0      \U0001f4c0\U0001f4c0\n      \U0001f4c0\U0001f4c0            \U0001f4c0\U0001f4c0\n    \U0001f4c0\U0001f4c0              \U0001f4c0\U0001f4c0\n    \U0001f4c0\U0001f4c0              \U0001f4c0\U0001f4c0\n      \U0001f45f                  \U0001f45f",
                              
                              @"          \U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\n          \U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\n          \U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\n          \U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\n    \U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\U0001f3a9\n      \u26AA \u26AA \U0001f440 \u26AA \u26AA\n  \u2755 \u26AA \u26AA \U0001f443 \u26AA \u26AA \u2754\n          \u26AA \U0001f53b \U0001f53b \u26AA\n              \u26AA \u26AA\n          \u26AB \u26AB \u26AB \u26AB\n        \u26AB  \u26AB \u26AB  \u26AB\n    \u26AB    \u26AB \u26AB    \u26AB\n    \u26AA    \u26AB \u26AB    \u26AA\n    \U0001f447    \u26AB \u26AB    \U0001f447\n    \U0001f377    \u2B1B \u2B1B\n            \u2B1B \u2B1B \u2B1B\n            \u2B1B    \u2B1B\n            \u2B1B    \u2B1B\n            \u2B1B    \u2B1B\n            \U0001f45e    \U0001f45e",
                                                          nil];
    
    NSMutableArray* arrGirl = [[NSMutableArray alloc] initWithObjects:
                               @"              \uE443 \uE443 \uE443\n        \uE443 \uE443 \uE443 \uE443 \uE443\n  \uE443 \uE443 \uE443 \uE443 \uE443 \uE443 \uE443\n\uE443    \u2312    \uE219    \u2312    \uE443\n\uE443    \uE332            \uE332    \uE443\n\uE443            \u2219 \uE41C            \uE443\n\uE443              \uE41D            \uE443",
                               
                               @"\U0001f459\u2709 \u2709 \u2709 \u2709 \u2709 \U0001f459\n\U0001f459\u2709 \u2709 \u2709 \u2709 \u2709 \U0001f459\n\U0001f459\U0001f459\U0001f459\u2709 \U0001f459 \U0001f459 \U0001f459\n\U0001f459\U0001f459\U0001f459\U0001f380\U0001f459\U0001f459\U0001f459\n\u2709 \U0001f459 \U0001f459 \u2709 \U0001f459 \U0001f459 \u2709\n\u2709 \u2709 \u2709 \u2709 \u2709 \u2709 \u2709\n\U0001f380\U0001f459\U0001f459\U0001f459\U0001f459\U0001f459\U0001f380\n\u2709 \u2709 \U0001f459 \U0001f459 \U0001f459 \u2709 \u2709\n\u2709 \u2709 \u2709 \U0001f459 \u2709 \u2709 \u2709",
                               
                               @"\U0001f493\U0001f493\U0001f493\U0001f493\U0001f493\U0001f493\U0001f493\n\U0001f497\U0001f49a\U0001f497\U0001f478\U0001f497\U0001f49a\U0001f497\n\U0001f49c\U0001f49a\U0001f49c\U0001f457\U0001f49c\U0001f49a\U0001f49c\n\U0001f485\U0001f485\U0001f485\U0001f456\U0001f485\U0001f485\U0001f485\n\U0001f48b\U0001f48b\U0001f48b\U0001f460\U0001f460\U0001f48b\U0001f48b\n\U0001f494\U0001f494\U0001f494\U0001f494\U0001f494\U0001f494\U0001f494",
                               
                               @"    \U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\n  \U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\n\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\U0001f300\n\U0001f300\U0001f300\u26AB \U0001f440 \u26AB \U0001f300 \U0001f300\n    \u26AB \u26AB \U0001f48b \u26AB \u26AB\n          \u26AB \u26AB \u26AB    \u26AB\n\u26AB \U0001f49f \U0001f49f \U0001f49f \U0001f49f \U0001f49f \u26AB\n\u26AB    \U0001f49f\U0001f49f\U0001f49f\n\U0001f45b      \U0001f380\U0001f380\n          \U0001f49f\U0001f49f\U0001f49f\n      \U0001f49f\U0001f49f\U0001f49f\U0001f49f\n      \U0001f49f\U0001f49f\U0001f49f\U0001f49f\n          \u26AB    \u26AB\n          \u26AB    \u26AB\n          \u26AB    \u26AB\n          \U0001f460    \U0001f460",
                               
                               @"\U0001f600\U0001f600\U0001f600\U0001f600\U0001f600\U0001f380\n\U0001f600\U0001f3b1\U0001f600\U0001f600\U0001f3b1\U0001f600\n\U0001f600\U0001f600\U0001f600\U0001f600\U0001f600\U0001f600\n\U0001f600\U0001f534\U0001f600\U0001f600\U0001f534\U0001f600\n\U0001f600\U0001f600\U0001f534\U0001f534\U0001f600\U0001f600\n\U0001f600\U0001f600\U0001f600\U0001f600\U0001f600\U0001f600\n       \U0001f457\U0001f457\U0001f457\n     \U0001f457\U0001f457\U0001f457\U0001f457\n  \U0001f457\U0001f457\U0001f457\U0001f457\U0001f457\n          \U0001f456\U0001f456\n          \U0001f456\U0001f456\n          \U0001f456\U0001f456\n          \U0001f45f\U0001f45f",
                               
                               @"         \U0001f495\U0001f495\U0001f495\U0001f338\U0001f495\n      \U0001f33a\U0001f497\U0001f338\U0001f380\U0001f497\U0001f338\n   \U0001f495\U0001f338\U0001f497\U0001f497\U0001f33a\U0001f496\U0001f493\n\U0001f338\U0001f496\U0001f33a\U0001f497\U0001f338\U0001f497\U0001f497\U0001f338\n\U0001f495\U0001f497\U0001f338\U0001f380\U0001f497\U0001f497\U0001f338\U0001f497\n\U0001f495\U0001f497\U0001f497\U0001f338\U0001f497\U0001f497\U0001f497\U0001f33a\n   \U0001f495\U0001f497\U0001f49e\U0001f497\U0001f338\U0001f380\U0001f338\n      \U0001f338\U0001f497\U0001f33a\U0001f497\U0001f496\U0001f497\n        \U0001f497\U0001f497\U0001f497\U0001f338\U0001f49e\n               \U0001f4e6\U0001f4e6\n     \U0001f467     \U0001f4e6\U0001f4e6     \U0001f41d\n     \U0001f64f     \U0001f4e6\U0001f4e6\n     \U0001f458     \U0001f4e6\U0001f4e6  \U0001f41d\n   \U0001f461\U0001f461  \U0001f4e6\U0001f4e6\n\U0001f338\U0001f343\U0001f343\U0001f4e6\U0001f4e6\U0001f343\U0001f338\U0001f343\n\U0001f343\U0001f343\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f343",
                               
                               @"\uE035 \uE035 \uE13E \uE13E \uE035 \uE035 \uE035\n\uE035 \uE12F \uE12F \uE12F \uE12F \uE035 \uE035\n\uE035 \uE319 \uE319 \uE319 \uE319 \uE319 \uE035\n\uE035 \uE13E \uE13E \uE13E \uE13E \uE13E \uE035\n\uE035 \uE323 \uE323 \uE323 \uE323 \uE323 \uE035\n\uE035 \uE13E \uE13E \uE13E \uE13E \uE13E \uE035\nFASHION ROCKS\n\uE00E \uE00E L I K E D\uE00E \uE00E\n\uE10E \uE10E \uE10E \uE10E \uE10E \uE10E \uE10E",
                               
                               @"\uE04A \uE32E \uE145 \uE03F \uE32E \uE130 \uE32E \uE130 \uE32E\n\uE04E \uE022 \uE022 \uE04E \uE022 \uE022 \uE130 \uE04E \uE32E\n\uE022 \uE032 \uE032 \uE032 \uE032 \uE130 \uE022 \uE130 \uE32E\n\uE022 \uE347 \uE347 \uE347 \uE130 \uE347 \uE022 \uE31D \uE32E\n\uE022 \uE31C \uE31C \uE31C \uE31C \uE31C \uE022 \uE31D \uE32E\n\uE31D \uE022 \uE13E \uE13E \uE13E \uE022 \uE31D \uE31D \uE32E\n\uE31D \uE130 \uE022 \uE310 \uE022 \uE31D \uE31D \uE329 \uE32E\n\uE130 \uE31D \uE31D \uE022 \uE31D \uE31D \uE31D \uE329 \uE32E\n\uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \uE003 \uE32E\n\uE32E \uE32EM.U.A\uE41CH\uE337 \uE32E \uE32E",
                               
                               @"   \U0001f460\U0001f460\n\U0001f460\U0001f460\U0001f460\n\U0001f460\U0001f460\U0001f460\n\U0001f460\U0001f460\U0001f460\n\u2757     \U0001f460\U0001f460\n\u2757     \U0001f460\U0001f460\n\u2757          \U0001f460\U0001f460\n\u2757          \U0001f460\U0001f460\U0001f460\n\u2757          \U0001f460\U0001f460\U0001f460\U0001f460\U0001f460\n\u26D4             \u26D4 \u26D4 \u26D4 \u26D4",
                               
                               @"    \U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\n\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f338\n\U0001f338\U0001f343\U0001f338\U0001f338\U0001f338\U0001f338\n\U0001f338\U0001f343\U0001f338\n\U0001f338\U0001f343\U0001f338\U0001f338\U0001f338\U0001f338\n\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f338\n\U0001f338\U0001f338\U0001f338\U0001f338\U0001f343\U0001f338\n              \U0001f338\U0001f343\U0001f338\n\U0001f338\U0001f338\U0001f338\U0001f338\U0001f343\U0001f338\n\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f338\n    \U0001f338\U0001f338\U0001f338\U0001f338\n\n\U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n\U0001f490\U0001f490\U0001f490\U0001f490\U0001f490\U0001f490\n\U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n          \U0001f490\U0001f490\n          \U0001f33c\U0001f33c\n          \U0001f490\U0001f490\n          \U0001f33c\U0001f33c\n\U0001f490\U0001f490\U0001f490\U0001f490\U0001f490\U0001f490\n\U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\U0001f33c\n\U0001f490\U0001f490\U0001f490\U0001f490\U0001f490\U0001f490\n\n    \U0001f338\U0001f338\U0001f338\U0001f338\U0001f338\n\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f338\n\U0001f338\U0001f343\U0001f338\U0001f338\U0001f338\U0001f338\n\U0001f338\U0001f343\U0001f338\n\U0001f338\U0001f343\U0001f338\U0001f338\U0001f338\U0001f338\n\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f338\n\U0001f338\U0001f338\U0001f338\U0001f338\U0001f343\U0001f338\n              \U0001f338\U0001f343\U0001f338\n\U0001f338\U0001f338\U0001f338\U0001f338\U0001f343\U0001f338\n\U0001f338\U0001f343\U0001f343\U0001f343\U0001f343\U0001f338\n    \U0001f338\U0001f338\U0001f338\U0001f338",
                               
                               @"              \U0001f380\n      \U0001f495\U0001f497\U0001f497\U0001f493\n    \U0001f495\U0001f497\U0001f440\U0001f497\U0001f493    \U0001f48d\n    \U0001f496\U0001f48e\U0001f445\U0001f48e\U0001f496    \U0001f446\n          \U0001f4ad\U0001f4ad\U0001f4ad          \u2601\n    \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2601    \U0001f4ad\U0001f4ad\U0001f4ad\n\u2601      \U0001f4ad\U0001f4ad\n\U0001f490    \U0001f4ad\U0001f4ad\U0001f4ad\n      \U0001f4ad\u2728 \u2728 \U0001f4ad\n    \U0001f4ad\u2728 \U0001f4ad \u2728 \U0001f4ad\n  \U0001f4ad\u2728 \U0001f4ad \U0001f4ad \u2728 \U0001f4ad\n\U0001f4ad\u2728 \U0001f4ad \U0001f4ad \U0001f4ad \u2728 \U0001f4ad\n\U0001f4ad\u2728 \U0001f4ad \U0001f4ad \U0001f4ad \u2728 \U0001f4ad\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\U0001f4ad\u2728 \U0001f4ad \U0001f4ad \U0001f4ad \u2728 \U0001f4ad\n          \U0001f460    \U0001f460",
                                                            nil];
    
    NSMutableArray* arrSymnbol = [[NSMutableArray alloc] initWithObjects:
                                  @"\u2744 \u2744 \u2744 \u2744 \u2744 \u2744\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\U0001f60a\U0001f60a\U0001f60a\U0001f60a\U0001f60a\U0001f60a\n\U0001f60a\U0001f60a\U0001f60a\U0001f60a\U0001f60a\U0001f60a\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\u2744 \u2744 \U0001f60a \U0001f60a \u2744 \u2744\n\u2744 \u2744 \u2744 \u2744 \u2744 \u2744",
                                  
                                  @"\U0001f499\u2B50 \U0001f499 \u2B50 \u2764 \u2764 \u2764 \u2764\n\u2B50 \U0001f499 \u2B50 \U0001f499 \u2601 \u2601 \u2601 \u2601\n\U0001f499\u2B50 \U0001f499 \u2B50 \u2764 \u2764 \u2764 \u2764\n\u2B50 \U0001f499 \u2B50 \U0001f499 \u2601 \u2601 \u2601 \u2601\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601\n\u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764 \u2764",
                                  
                                  @"\U0001f499\U0001f456               \u2764 \u260E\n\U0001f499\U0001f456               \u2764 \u260E\n\U0001f499\U0001f4d8               \u2764 \u260E\n     \U0001f499\U0001f4d8     \u2764 \u2764\n          \U0001f499\U0001f4d8\u260E\n          \u260E \U0001f499 \U0001f4d8\n     \U0001f621\u260E     \U0001f499\U0001f4d8\n\U0001f621\u260E               \U0001f499\U0001f456\n\u2764 \u260E               \U0001f499\U0001f456\n\u2764 \U0001f460               \U0001f499\U0001f456\n     \u2764 \U0001f460     \u2708 \U0001f499\n          \u2764 \U0001f460 \U0001f499\n          \u2708 \u2764 \U0001f460\n     \u2708 \U0001f499     \u2764 \U0001f460\n\u2708 \U0001f456               \u2764 \U0001f460\n\U0001f499\U0001f456               \u2764 \u260E\n\U0001f499\U0001f456               \u2764 \u260E\n     \U0001f499\U0001f4d8     \u2764 \u2764\n          \U0001f499\U0001f4d8\u260E\n          \u260E \U0001f499 \U0001f4d8\n     \U0001f621\u260E     \U0001f499\U0001f4d8\n\U0001f621\u260E               \U0001f499\U0001f456\n\u2764 \u260E               \U0001f499\U0001f456\n\u2764 \U0001f460               \U0001f499\U0001f456\n     \u2764 \U0001f460     \u2708 \U0001f499\n          \u2764 \U0001f460 \U0001f499\n          \u2708 \u2764 \U0001f460\n     \u2708 \U0001f499     \u2764 \U0001f460\n\u2708 \U0001f456               \u2764 \U0001f460\n\U0001f499\U0001f456               \u2764 \u2764",
                                  
                                  @"     \U0001f48e\U0001f48e\U0001f48e\U0001f48e\U0001f48e\n  \U0001f48e                     \U0001f48e\n\U0001f48e                         \U0001f48e\n\U0001f48e\u2712 \u3030 \u27B0 \u3030 \U0001f526 \U0001f48e\n  \U0001f48e   \u2712     \U0001f526   \U0001f48e\n     \U0001f48e     \U0001f374     \U0001f48e\n       \U0001f48e   \U0001f374   \U0001f48e\n          \U0001f48e\U0001f374\U0001f48e\n          \U0001f48e\u2714 \U0001f48e\n          \U0001f4b3\U0001f4b3\U0001f4b3\n          \U0001f4b3\U0001f4b3\U0001f4b3\n          \U0001f4fc\u26AB \U0001f4fc",
                                  
                                  @"\u2648 \u2649 \u264A \u264B \u264C \u264D \u264E\n\u264F \u2650 \u2651 \u2652 \u2653 \u2648 \u2649\n\U0001f311\U0001f401\U0001f401\U0001f311\U0001f401\U0001f401\U0001f311\n\U0001f312\U0001f407\U0001f407\U0001f312\U0001f407\U0001f407\U0001f312\n\U0001f313\U0001f401\U0001f401\U0001f313\U0001f401\U0001f401\U0001f313\n\U0001f314\U0001f407\U0001f407\U0001f314\U0001f407\U0001f407\U0001f314\n\U0001f315\U0001f401\U0001f401\U0001f315\U0001f401\U0001f401\U0001f315\n\U0001f316\U0001f407\U0001f407\U0001f316\U0001f407\U0001f407\U0001f316\n\U0001f317\U0001f401\U0001f401\U0001f317\U0001f401\U0001f401\U0001f317\n\U0001f318\U0001f407\U0001f407\U0001f318\U0001f407\U0001f407\U0001f318\n\U0001f311\U0001f401\U0001f401\U0001f311\U0001f401\U0001f401\U0001f311\n\u2648 \u2649 \u264A \u264B \u264C \u264D \u264E\n\u264F \u2650 \u2651 \u2652 \u2653 \u2648 \u2649",
                                  
                                  @"\U0001f527\U0001f527\U0001f527\U0001f3c8\U0001f527\U0001f527\U0001f527\n\U0001f527\U0001f527\U0001f3c8\U0001f3c8\U0001f3c8\U0001f527\U0001f527\n\U0001f527\U0001f3c8\U0001f527\U0001f3c8\U0001f527\U0001f3c8\U0001f527\n\U0001f527\U0001f527\U0001f527\U0001f3c8\U0001f527\U0001f527\U0001f527\n\U0001f527\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\U0001f527\n\U0001f3c8\U0001f527\U0001f527\U0001f527\U0001f527\U0001f527\U0001f3c8\n\U0001f3c8\U0001f527\U0001f527\U0001f527\U0001f527\U0001f527\U0001f3c8\n\U0001f3c8\U0001f527\U0001f527\U0001f527\U0001f527\U0001f527\U0001f3c8\n\U0001f3c8\U0001f527\U0001f527\U0001f527\U0001f527\U0001f527\U0001f3c8\n\U0001f527\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\U0001f527",
                                  
                                  @"\u2601 \u2601 \u2601 \u2650 \u2650 \u2650 \u2650 \u2650\n\u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2650 \u2650\n\u2601 \u2650 \u2601 \u2601 \u2601 \u2650 \u2601 \u2650\n\u2601 \u2601 \u2650 \u2601 \u2650 \u2601 \u2601 \u2650\n\u2601 \u2601 \u2601 \u2650 \u2601 \u2601 \u2601 \u2601\n\u2601 \u2601 \u2650 \u2601 \u2650 \u2601 \u2601 \u2601\n\u2601 \u2650 \u2601 \u2601 \u2601 \u2650 \u2601 \u2601\n\u2650 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601 \u2601",
                                      nil];
    
    NSMutableArray* arrWord = [[NSMutableArray alloc] initWithObjects:
                               @"\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6bc\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6bc\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6bc\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6bc\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6bc\n\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6bc\n\n\U0001f6bc\U0001f6bc          \U0001f6bc\U0001f6bc\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n    \U0001f6ba\U0001f6ba\U0001f6ba\U0001f6ba\n\n\U0001f6bc\U0001f6bc          \U0001f6bc\U0001f6bc\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6ba\U0001f6ba\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n\U0001f6ba\U0001f6ba          \U0001f6ba\U0001f6ba\n\U0001f6bc\U0001f6bc          \U0001f6bc\U0001f6bc",
                               
                               @"\U0001f46c\U0001f46c\U0001f46c\U0001f46c\n\U0001f46c\U0001f3c8          \U0001f46c\n\U0001f46c\U0001f3c8          \U0001f46c\n\U0001f46c\U0001f46c\U0001f46c\U0001f46c\n\U0001f46c\U0001f3c8          \U0001f46c\n\U0001f46c\U0001f3c8          \U0001f46c\n\U0001f46c\U0001f46c\U0001f46c\U0001f46c\n\n\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f46c\n\U0001f3c8\U0001f3c8\U0001f3c8\U0001f3c8\n\U0001f3c8\U0001f46c\n\U0001f3c8\U0001f46c\n\U0001f3c8\U0001f46c\n\n\U0001f37a\U0001f37a\U0001f37a\U0001f37a\U0001f37a\n\U0001f37a\U0001f3c8\n\U0001f37a\U0001f37a\U0001f37a\U0001f37a\n\U0001f37a\U0001f3c8\n\U0001f37a\U0001f3c8\n\U0001f37a\U0001f3c8",
                               
                               @"\U0001f49b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f49b\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f3b1\U0001f3b1\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f49b\U0001f49b\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f49b\U0001f49b\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f3b1\U0001f3b1\U0001f3b1\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f3b1\U0001f3b1\n\U0001f49b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f49b\n\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\n8\U00020e38\U00020e38\U00020e38\U00020e38\u20E3 \U0001f49b\n8\U00020e38\U00020e38\U00020e38\U00020e38\U00020e38\u20E3\n8\U00020e38\u20E3 \U0001f49b \U0001f49b8\U00020e38\u20E3\n8\U00020e38\u20E3 \U0001f49b \U0001f49b8\U00020e38\u20E3\n8\U00020e38\U00020e38\U00020e38\U00020e38\u20E3 \U0001f49b\n8\U00020e38\u20E3 \U0001f49b \U0001f49b8\U00020e38\u20E3\n8\U00020e38\u20E3 \U0001f49b \U0001f49b8\U00020e38\u20E3\n8\U00020e38\u20E3 \U0001f49b \U0001f49b \U0001f49b8\u20E3\n\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\U0001f49b\n\U0001f49b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f49b\n\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f3b1\U0001f3b1\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f3b1\U0001f3b1\n\U0001f49b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f49b\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f3b1\U0001f3b1\n\U0001f3b1\U0001f3b1\U0001f49b\U0001f49b\U0001f3b1\U0001f3b1\n\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\n\U0001f49b\U0001f3b1\U0001f3b1\U0001f3b1\U0001f3b1\U0001f49b",
                               
                               @"\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f33a\U0001f33a\u2728 \u2728 \u2728 \U0001f33a \U0001f33a \u2728\n\U0001f33a\u2728 \U0001f33a \u2728 \U0001f33a \u2728 \u2728 \U0001f33a\n\U0001f33a\u2728 \U0001f33a \u2728 \U0001f33a \u2728 \u2728 \U0001f33a\n\U0001f33a\U0001f33a\u2728 \u2728 \u2728 \U0001f33a \U0001f33a \u2728\n\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f33a\U0001f33a\U0001f33a\u2728 \U0001f33a \U0001f33a \U0001f33a \u2728\n\u2728 \U0001f33a \u2728 \u2728 \u2728 \U0001f33a \u2728 \u2728\n\u2728 \U0001f33a \u2728 \u2728 \u2728 \U0001f33a \u2728 \u2728\n\U0001f33a\U0001f33a\U0001f33a\u2728 \u2728 \U0001f33a \u2728 \u2728\n\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f",
                               
                               @"\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f31f\U0001f635\U0001f635\U0001f635\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f635\U0001f635\U0001f635\U0001f31f\n\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\u2728 \u2728 \u2728 \u2728 \u2728 \u2728\n\U0001f31f\U0001f31f\U0001f31f  \U0001f31f\U0001f31f\U0001f31f\n\U0001f31f\U0001f635\U0001f31f  \U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f635  \U0001f635\U0001f635\U0001f31f\n\U0001f31f\U0001f635 \U0001f635\U0001f635 \U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f \U0001f31f \U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f \U0001f31f \U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f \U0001f31f \U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f \U0001f31f \U0001f635\U0001f31f\n\U0001f31f\U0001f31f\U0001f31f  \U0001f31f\U0001f31f\U0001f31f\n\u2728 \u2728 \u2728 \u2728 \u2728 \u2728\n\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f31f\U0001f635\U0001f635\U0001f635\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f635\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f31f\U0001f31f\U0001f635\U0001f31f\n\U0001f31f\U0001f635\U0001f635\U0001f635\U0001f635\U0001f31f\n\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\U0001f31f\n\u2728 \u2728 \u2728 \u2728 \u2728 \u2728\n     \U0001f31f\U0001f31f\U0001f31f\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f31f\U0001f31f\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f635\U0001f635\U0001f31f\n     \U0001f31f\U0001f31f\U0001f31f\U0001f31f",
                               
                               @"     \U0001f63b\U0001f63d\U0001f63d\U0001f439\n\U0001f436\U0001f439\n\U0001f436\U0001f439\U0001f439\U0001f439\U0001f42d\n               \U0001f439\U0001f42d\n\U0001f437\U0001f437\U0001f437\U0001f42d\n\n\U0001f430\U0001f438     \U0001f422\U0001f42f\n\U0001f430\U0001f438     \U0001f422\U0001f42f\n\U0001f430\U0001f438     \U0001f422\U0001f42f\n\U0001f430\U0001f438     \U0001f422\U0001f42f\n     \U0001f430\U0001f438\U0001f42f\n\n\U0001f428\U0001f43b\U0001f43b\U0001f43b\n\U0001f428\U0001f43c     \U0001f43c\U0001f43b\n\U0001f428\U0001f43c     \U0001f43c\U0001f43b\n\U0001f428\U0001f43b\U0001f43b\U0001f43b\n\U0001f428\U0001f43c",
                               
                               @"\U0001f431\U0001f431\U0001f431\U0001f431\U0001f431\n\U0001f431\U0001f631\U0001f631\U0001f631\U0001f631\U0001f431\n\U0001f431\U0001f631          \U0001f631\U0001f431\n\U0001f431\U0001f631          \U0001f631\U0001f431\n\U0001f431\U0001f431\U0001f431\U0001f431\U0001f431\n\U0001f431\U0001f631          \U0001f631\U0001f431\n\U0001f431\U0001f631          \U0001f631\U0001f431\n\U0001f431\U0001f631\U0001f631\U0001f631\U0001f631\U0001f431\n\U0001f431\U0001f431\U0001f431\U0001f431\U0001f431\n\n\U0001f621\U0001f621          \U0001f621\U0001f621\n\U0001f479\U0001f479          \U0001f479\U0001f479\n\U0001f479\U0001f479          \U0001f479\U0001f479\n    \U0001f479\U0001f479\U0001f479\U0001f479\n          \U0001f479\U0001f479\n          \U0001f479\U0001f479\n          \U0001f621\U0001f621\n\n\U0001f351\U0001f351\U0001f351\U0001f351\U0001f351\U0001f48b\n\U0001f351\U0001f351\U0001f351\U0001f351\U0001f351\U0001f48b\n\U0001f351\U0001f351\n\U0001f351\U0001f351\U0001f351\U0001f48b\n\U0001f351\U0001f351\U0001f351\U0001f48b\n\U0001f351\U0001f351\n\U0001f351\U0001f351\U0001f351\U0001f351\U0001f351\U0001f48b\n\U0001f351\U0001f351\U0001f351\U0001f351\U0001f351\U0001f48b",
                               
                               @"\u2B1B \U0001f338          \U0001f341\u2B1B\n\u2B1B \U0001f33a          \U0001f338\u2B1B\n\u2B1B \U0001f338          \U0001f33a\u2B1B\n\u2B1B \u2B1B \u2B1B \u2B1B \u2B1B \u2B1B\n\u2B1B \U0001f339 \U0001f338 \U0001f33a \U0001f337 \u2B1B\n\u2B1B \U0001f33a          \U0001f338\u2B1B\n\n\u2B1B \u2B1B \u2B1B \u2B1B \u2B1B \U0001f338\n\u2B1B \U0001f338 \U0001f339 \U0001f338 \U0001f33a \U0001f337\n\u2B1B \u2B1B \u2B1B\n\u2B1B \U0001f33a\n\u2B1B \U0001f338 \U0001f341 \U0001f33a \U0001f338 \U0001f338\n\u2B1B \u2B1B \u2B1B \u2B1B \u2B1B \U0001f338\n\n\u2B1B \U0001f338          \U0001f331\u2B1B\n\u2B1B \U0001f341          \U0001f33a\u2B1B\n    \u2B1B \U0001f338 \U0001f337 \u2B1B\n          \u2B1B \u2B1B \U0001f338\n            \u2B1B \U0001f33a\n            \u2B1B \U0001f341",
                               
                               @"\U0001f33a\U0001f33a                    \U0001f33a\U0001f33a\n\U0001f33a\U0001f33a     \U0001f33a\U0001f33a     \U0001f33a\U0001f33a\n\U0001f33a\U0001f33a     \U0001f33a\U0001f33a     \U0001f33a\U0001f33a\n\U0001f33a\U0001f33a     \U0001f33a\U0001f33a     \U0001f33a\U0001f33a\n\U0001f33a\U0001f33a     \U0001f33a\U0001f33a     \U0001f33a\U0001f33a\n\U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\U0001f33a\n     \U0001f33a\U0001f33a          \U0001f33a\U0001f33a\n\n          \U0001f62f\U0001f62f\U0001f62f\U0001f62f\n     \U0001f62f\U0001f62f          \U0001f62f\U0001f62f\n     \U0001f62f\U0001f62f          \U0001f62f\U0001f62f\n     \U0001f62f\U0001f62f          \U0001f62f\U0001f62f\n     \U0001f62f\U0001f62f          \U0001f62f\U0001f62f\n     \U0001f62f\U0001f62f          \U0001f62f\U0001f62f\n          \U0001f62f\U0001f62f\U0001f62f\U0001f62f\n\n\U0001f646\U0001f646                    \U0001f646\U0001f646\n\U0001f646\U0001f646     \U0001f646\U0001f646     \U0001f646\U0001f646\n\U0001f646\U0001f646     \U0001f646\U0001f646     \U0001f646\U0001f646\n\U0001f646\U0001f646     \U0001f646\U0001f646     \U0001f646\U0001f646\n\U0001f646\U0001f646     \U0001f646\U0001f646     \U0001f646\U0001f646\n\U0001f646\U0001f646\U0001f646\U0001f646\U0001f646\U0001f646\U0001f646\U0001f646\n     \U0001f646\U0001f646          \U0001f646\U0001f646",
                               
                               @"\u2728 \U0001f496 \U0001f498 \u2728 \U0001f496 \U0001f498 \u2728\n\U0001f496\U0001f497\U0001f497\U0001f496\U0001f497\U0001f497\U0001f498\n\U0001f496\U0001f497\U0001f495\U0001f493\U0001f493\U0001f497\U0001f498\n\U0001f496\U0001f497\U0001f493\U0001f493\U0001f493\U0001f495\U0001f498\n\u2728 \U0001f496 \U0001f493 \U0001f495 \U0001f495 \U0001f498 \u2728\n\u2728     \U0001f496\U0001f495\U0001f498     \u2728\n\u2728          \U0001f498          \u2728\n\u2728                         \u2728\n\u2728 \U0001f499 \U0001f48e     \U0001f499\U0001f48e\u2728\n\u2728 \U0001f499 \U0001f48e     \U0001f499\U0001f48e\u2728\n\u2728 \U0001f499 \U0001f48e     \U0001f499\U0001f48e\u2728\n\u2728 \U0001f499 \U0001f48e \U0001f48e \U0001f499 \U0001f48e \u2728\n\u2728 \u2728 \U0001f499 \U0001f499 \U0001f48e \u2728 \u2728",
                               
                               @"\u2665 \u2665 \u2665 \u2665 \u2665 \u2709\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2665\n\u2665 \u2665 \u2709 \u2709 \u2665 \u2665\n\u2665 \u2665 \u2709 \u2709 \u2665 \u2665\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2665\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2709\n\u2665 \u2665 \u2709 \u2709 \u2709 \u2709\n\u2665 \u2665 \u2709 \u2709 \u2709 \u2709\n\u2665 \u2665 \u2709 \u2709 \u2709 \u2709\n\u2709 \u2709 \u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\u2709 \u2709 \u2709 \u2709\n\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\n\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\U0001f49c\n\u2709 \u2709 \u2709 \u2709 \u2709 \u2709\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2665\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2665\n\u2709 \u2709 \u2709 \u2709 \u2665 \u2665\n\u2709 \u2709 \u2709 \u2665 \u2665 \u2709\n\u2709 \u2709 \u2665 \u2665 \u2709 \u2709\n\u2709 \u2665 \u2665 \u2709 \u2709 \u2709\n\u2665 \u2665 \u2709 \u2709 \u2709 \u2709\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2665\n\u2665 \u2665 \u2665 \u2665 \u2665 \u2665",
                               
                               @"\U0001f608\U0001f608\U0001f44c\U0001f44c\U0001f608\U0001f608\n\U0001f608\U0001f608\U0001f44c\U0001f44c\U0001f608\U0001f608\n\U0001f608\U0001f608\U0001f44c\U0001f608\U0001f608\U0001f44c\n\U0001f608\U0001f608\U0001f608\U0001f608\U0001f44c\U0001f44c\n\U0001f608\U0001f608\U0001f44c\U0001f608\U0001f608\U0001f44c\n\U0001f608\U0001f608\U0001f44c\U0001f44c\U0001f608\U0001f608\n\U0001f608\U0001f608\U0001f44c\U0001f44c\U0001f608\U0001f608\n\U0001f44c\U0001f44c\U0001f44c\U0001f44c\U0001f44c\U0001f44c\n\U0001f699\U0001f699\U0001f44c\U0001f44c\U0001f699\U0001f699\n\U0001f699\U0001f699\U0001f44c\U0001f44c\U0001f699\U0001f699\n\U0001f699\U0001f699\U0001f44c\U0001f699\U0001f699\U0001f44c\n\U0001f699\U0001f699\U0001f699\U0001f699\U0001f44c\U0001f44c\n\U0001f699\U0001f699\U0001f44c\U0001f699\U0001f699\U0001f44c\n\U0001f699\U0001f699\U0001f44c\U0001f44c\U0001f699\U0001f699\n\U0001f699\U0001f699\U0001f44c\U0001f44c\U0001f699\U0001f699",
                                                      nil];
    
//    NSMutableArray* arrCity = [[NSMutableArray alloc] initWithObjects:
//                               nil];
//    NSMutableArray* arrOthers = [[NSMutableArray alloc] initWithObjects:
//                                 nil];
    NSMutableArray* arrChristmas = [[NSMutableArray alloc] initWithObjects:
                                    @"\u30FB+ \"\uE448  \uE04C \u2022  \uE32E\n\uE335  \u3002  \u2022 ..\uE049:\n/  \uE049  \uE13D *  + \uE32E\n\uE32E \uE337Merry\uE32E \uE335\n\uE310  Christmas\uE337\n\uE036 \uE038 \uE201 \uE115 \uE14D \uE43D\n\uE304 \uE304 \uE304 \uE304 \uE304 \uE304 \uE304",
                                    
                                    @"\u3002      \uE049  \u3002 \u3002  \uE04C \u3002\n      *        \uE033        *    \u3002\n  \uE10D      \uE437 \uE327      \u3002\n  \u3002 \u3002  \uE335 \uE04E \uE32E\n*        \uE030 \uE10B \uE314 \uE303    \uE049\n*    \uE441 \uE52E \uE048 \uE053 \uE33E\n            \uE308 \uE447 \uE319      *\n\uE049    \uE054 \uE035 \uE101 \uE42E \uE520\n  \u30FBHappy Holiday\u30FB *\n\u3002 \uE529 \uE523 \uE119 \uE346 \uE047 \uE339 \uE131\n\uE022 \uE032 \uE10A \uE43A \uE102 \uE13E \uE41C \uE310\n\uE448Merry Christmas\uE448\n\uE110HAPPY New Year\uE110\n                \uE10E\n                \uE131",
                                    
                                    @"\uE04C          \uE335    *\n      \uE335            \uE049 \uE049  \uE335\n*                  *      *\n      *      \uE32E          \uE335  *\n\uE335        \uE41E \uE448\n\uE201 \uE033    \uE036 \uE036  \uE037 \uE157 \uE033\n\uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3 \uFFE3\n*Santa is coming to town*\n  \uE112Merry Christmas\uE437",
                                    
                                    @"\uE415 \uE056 \uE057 \uE414 \uE405 \uE106 \uE418 \uE40D\n\uE022*New*YearBegins\uE022\n\uE001 \uE002 \uE005 \uE004 \uE51A \uE519 \uE518 \uE515\n\uE328~GODbless\'YOU\'\uE328\n\uE516 \uE517 \uE152 \uE04E \uE51C \uE424 \uE426 \uE10C\n\uE32C",
                                    
                                    @"                  \uE335\n            \u25E2 \uE219 \uE219 \u25E3\n\u3000  \u25E2 \uE219 \uE219 \uE219 \uE219 \u25E3\n      {\uFF5E \uFF5E \uFF5E \uFF5E \uFF5E \uFF5E \uFF5E}\n        l\u3000 \u25CF \u3000 \u3000 \u3000  \u25CF \u3000l\n\uFF08 \u2570 \uE049 \uE049 \uE41A \uE049 \uE049 \u256F \uFF09\n  \uFF08 \uE049 \uE049 \uE049 \uE049 \uE049 \uE049 \uFF09\n    \uFF08 \uE049 \uE049 \uE332 \uE049 \uE049 \uFF09\n    \uE033                          \uE033\n  \uE033 \uE033    \uE112 \uE112    \uE033 \uE033",
                                    
                                    @"\uE117 \uE117 \uE117 \uE033 \uE117 \uE117 \uE117\n\uE117 \uE117 \uE033 \uE033 \uE117 \uE117 \uE117\n\uE117 \uE117 \uE033 \uE033 \uE033 \uE117 \uE117\n\uE117 \uE117 \uE033 \uE033 \uE033 \uE033 \uE117\n\uE117 \uE033 \uE033 \uE033 \uE033 \uE033 \uE117\n\uE117 \uE117 \uE033 \uE033 \uE033 \uE117 \uE117\n\uE117 \uE033 \uE033 \uE033 \uE033 \uE033 \uE117\n\uE117 \uE033 \uE033 \uE033 \uE033 \uE033 \uE117\n\uE033 \uE033 \uE033 \uE033 \uE033 \uE033 \uE033\n\uE117 \uE117 \uE325 \uE325 \uE325 \uE117 \uE117\n\uE117 \uE117 \uE325 \uE325 \uE325 \uE117 \uE117",
                                    
                                    @"\uE32E \u3002        \uE335 \u3002    \uE32E\n  \u3002          \uE033        \u3002 \u3002\n    \uE32E    \uE033 \uE033 \u3002 \u3002  \uE32E\n\uE32E        \uE033 \uE219 \uE033  \uE32E \u3002\n    \u3002 \uE033 \uE219 \uE314 \uE033 \u3002 \uE32E\n\u3002  \uE033 \uE314 \uE033 \uE219 \uE033 \u3002 \u3002\n  \uE32E  \uE033 \uE033 \uE314 \uE033 \u3002\n\u3002 \u3002 \uE033 \uE033 \uE219 \uE033 \uE033 \u3002 \uE32E\n    \uE033 \uE033 \uE314 \uE033 \uE219 \uE033\n  \uE033 \uE219 \uE033 \uE033 \uE314 \uE033 \uE033\n\uE32E \uE033 \uE314 \uE033 \uE033 \uE219 \uE033 \uE32E\n  \uE033 \uE033 \uE219 \uE033 \uE033 \uE314 \uE033\n\uE033 \uE219 \uE033 \uE314 \uE219 \uE033 \uE033 \uE033\n          MERRY\n  \U000e32ecHRISTMAS \uE32E",
                                    nil];
    
    arrArtEmojiContent = [[NSMutableArray alloc] initWithObjects:
                          arrGreeting, arrCelebration, arrLife, arrMood, arrLove, arrGesture, arrFood, arrWeather, arrSport, arrSchool, arrNature, arrFun, arrGirl, arrSymnbol, arrWord, arrChristmas,
                          nil];
}

- (void) loadArtTextContent
{
//    NSMutableArray* arrBorder = [[NSMutableArray alloc] initWithObjects:
//                                 
//                                 
//                                 nil];
    
    NSMutableArray* arrCelebration = [[NSMutableArray alloc] initWithObjects:
                                     
                                      @" (\n  )\\\n  /  )  Grow\n( * (  old\n  \\#/  with\n.-\"#\'-.  me!\n|\"-.-\"|\n|        |\n|        |\n|        |\n\'-._,-\'  Be My\n      Valentine",
                                      
                                      @"@@@@\n@@()@@ wWWWw\n@@@@      (___)\n    /                Y\n  \\ |              \\ |/\n    |///          \\\\|///\n^^^^^^^^^^^^^^^\nHappy V-Day, baby!",
                                      
                                      @"  o___o\n    (^o^)\n    o/( )\\o\n  O_.^._O\n  Happy\nValentine\'s\n    Day",
                                      
                                      @".    \u30FE(\uFF20\u2312\u30FC\u2312\uFF20)\u30CE\n\u2606HAPPY BIRTHDAY\u2606\n__i_i_i_i_i_i_i_i_i_i_i_i_\n(\uFF3F\uFF0A\uFF3F\uFF0A\uFF0A\uFF0A\uFF0A\uFF3F\uFF0A\uFF3F)\n(\uFFE3\uFF0A\uFFE3\uFF0A\uFF0A\uFF0A\uFF0A\uFFE3\uFF0A\uFFE3)\n(\uFFE3\uFF0A\uFFE3\uFF0A\uFF0A\uFF0A\uFF0A\uFFE3\uFF0A\uFFE3)\n  \uFFE3\uFFE3)\uFFE3\uFFE3\uFFE3\uFFE3\uFFE3(\uFFE3\uFFE3\n        _)\uFFE3\uFFE3\uFFE3\uFFE3\uFFE3(_\n    \uFF5C_\uFF3F\uFF3F\uFF3F\uFF3F___\uFF5C",
                                      
                                      @".          ()  () () ()  ()\n        \u0325 _\u0332 \u2551  \u2551 \u2551 \u2551  \u2551 _\u0332  \u0325\n    {. \u2022 _ \u2743 _ \u2022 _ \u2743_ \u2022  .}\n__{\u2022 .\u274B. _ .\u274B. _ .\u274B.  \u2022}__\n~~~~~~~~~~~~~~~~~~\n.\u2022*\u2022. \u0570\u0251\u0539\u0539\u057E \u048D\u00ED\u027E\u0535\u0570\u056A\u0251\u057E .\u2022*\u2022.",
                                      
                                      @"      _\n  _.;_\'-._\n{`--.-\'_,}\n{; \\,__.-\'/}\n{.\'-`._;-\';\n`\'--._.-\'\n    .-\\\\,-\"-.\n    `- \\( \'-. \\\n          \\;---,/\n    .-\"\"-;\\\n  /    .-\' )\\\n  \\,----\'` \\\\\n                \\|\nBe My Valentine rose..",
                                      
                                      @"Happy Valentine\'s Day\n          !~\n        /|\n      /  |\n    /    |\\\n  /      | \\\n/____|  \\\n          |___\\\n.========,\n`----------`\n~~~~~~~~~~\nI\'ll sail an ocean for you",
                                      
                                      @".                              .--.\n  {\\                      /  q {\\\n  {  \'\\                  \\  (-(~\'\n{  \'.{\'\\                  \\  \\ )\n{ \' - { \' \\  .-\"\"\"\"\"\"-. \\  \\\n{ . _ { \'.\' \\/              \'.)  \\\n{  _.  { .    {\'                    ]\n{._    {  \'{    ;\'-=-.          ]\n  { -.  { . \' {  \';-=-.\'        /\n    {  ._.  {.;    \'-=-      .\'\n        {_.-\' \'\'.__      _,-\'\n                        [ ][ ]\'\n                        .=\'=,\nHAPPY THANKSGIVING",
                                      
                                      @"\u2606\u3002\u2605\u3002\u2606\u3002\u2605\n\u3002\u2606 \u3002\u2606\u3002\u2606\n\u2605\u3002\uFF3C\uFF5C\uFF0F\u3002\u2605\nHappy New Year!\n\u2605\u3002\uFF0F\uFF5C\uFF3C\u3002\u2605\n\u3002\u2606\u3002 \u3002\u2606\u3002\n  \u2606\u3002 \u2605\u3002 \u2606", 
                                      
                                      @"Happy Hanukkah\n            ()\n()  ()  ()  | |  ()  ()  ()\n| | | | | |  | |  | | | | | |\n(. ! ! ! ! ! ! ! ! ! ! ! ! .)\n            | |\n            | |\n=====./ \\.=====", 
                                      
                                      @"      \uFF3C(^o^)~\n\u226A\u2606\u266A*CONGRATS*\u266A\u2606\u226B\n                  ~(^o^)\uFF0F",
                                                                        nil];
    
    NSMutableArray* arrLife = [[NSMutableArray alloc] initWithObjects:
                               
                               @"  ,  \" .        .\n,\'    |    `.  |>\uFE64|\n\\  -+-  /  ,\' Go\n  \\  |  /  |>\uFE64| Fly\n    \\ | /    /.    a\n      V`-- \'    Kite",
                               
                               @"                  _\n                ( ((\n                \\ = \\\n  ________ \\_.`-\\\n(__|___|__          (    \\----\n          (____)) _\n          (____))\n            (___))____/-----",
                               
                               @"    ________\n  /  \\_______\\\n/___\\_______\\\n|       | [+]   [+] |\n|_\u03A0_|_\u03A8__\u03A8_|",
                               
                               @"  ___________\n.\u2019 __________  \u2018.\n| .\u2019                  \u2018. |\n| | #                  | |\n| |                    | |\n| \u2018._________ .\u2019 |\n|          o            |\n\u2018.___________.\u2019\n      _ .\u2019    \u2018._\n    \u2019-----------\u2019\u2019..._\n____________  \u2018.-----.\n\u2018--------------------\u2019 \u2018.___.\u2019",
                               
                               @"\u2606\u250C\u2500\u2510  \u2500\u2510\u2606\n\u3000\u2502\u2592\u2502 /\u2592/\n\u3000\u2502\u2592\u2502/\u2592/\n\u3000\u2502\u2592 /\u2592/\u2500\u252C\u2500\u2510\u25EF\n\u3000\u2502\u2592\u2502\u2592|\u2592\u2502\u2592\u2502\n\u250C\u2534\u2500\u2534\u2500\u2510-\u2518\u2500\u2518\n\u2502\u2592\u250C\u2500\u2500\u2518\u2592\u2592\u2592\u2502\u25EF\n\u2514\u2510\u2592\u2592\u2592\u2592\u2592\u2592\u250C\u2518\n\u25EF\u2514\u2510\u2592\u2592\u2592\u2592\u250C",
                               
                               @"                              __\n     ________       /    /\n    |    \u2022____ \u2022    |/    /\n   /|    |VVVV|    |   /\n /  |    |/VVV\\|    |\n \u2014|                   |\n       |___|___ |",
                               
                               @"\"(>^_^)>#\nI made you a waffle\n\n#\uFE64(^_^\uFE64)\nBut then I was like...\n\n(>^#^\uFE64)\n\"I\'m hungry...\"\n\n(>^_^)> So I ate it.",
                               
                               @"    \uE011Inglorious Basterds\uE011\n\n                ./ \u0301 \u0304/).                (: ]\n              ./...//                  \\\\..\\.\n              ./...//                      \\\\..\\.\n            /..../7                      \\\\...\\.\n          ../ \u0301 \u0304/.../ \u0301 \u0304\\.........      ......./`\\.../`\\...\n        ././.../..../..../.|_).  .(_|.\\....\\....\\....\\.\\\n      (.(....(....(..../.)..)  (..(.\\....)....)....).)\n      \\................\\/.../      \\..\\/.............../\n        \\................./          \\................/\n        \\..............(              )............./\n          \\.............\\.            /............/",
                               
                               @"  \u00B8...\u00B8    __/ /\\______\n,\u00B7\u00B4\u00BA o`\u00B7,/__/  /\\____/\\_\\\n```)\u00A8(\u00B4\u00B4\u00B4 | | | | | || |l\u00B1\u00B1\u00B1\u00B1 |\n\u00B8,.-\u00B7\u00B2\u00B0\u00B4 \u00B8,.-\u00B7~\u00B7~\u00B7-.,\u00B8 `\u00B0\u00B2\u00B7-. :)..$\n\nOn my way home",
                               
                               @"  _____\n((  ____  \\----\n((  _____\n((_____\n((____      ----\n          /    /\n        (_((",
                               
                               @"\u2572 \u256D\u2501\u2501\u2501\u2501\u256E\n\u2572 \u2503Good\u2661.    |\n\u2572 \u2570\u2533\u256ELuck.\u2503\n    \u2572. | \u2570\u2501\u2501\u256F\n\u2572 \u256D\u2501<\u2501\u2501\u256E\u2572\u2572\n\u2572\u2503\u256D\u256E\u256D\u256E\u2503\u2572\u2572\n\u2517\u252B\u250F\u2501\u2501\u2513\u2523\u251B\u2572\n\u2572\u2503\u2570\u2501\u2501\u256F\u2503 \u2572\n\u2572\u2570\u2533\u2501\u2501\u2533\u256F\u2572\u2572\n\u2572\u2572\u251B\u2572\u2572\u2517\u2572\u2572\u2572",
                               
                                                           nil];
    
    NSMutableArray* arrLove = [[NSMutableArray alloc] initWithObjects:
                               
                               @" (\"-^-/\")\n  `o__o\' ]\n  (_Y_) _/\n  _..`--\'-.`,\n(__)_,--(__)\n    7:  ; 1\n_/,`-.-\' :\n(_,)-~~(_,)\nI Love U,\nI Need U,\nI Want U!",
                               
                               @"          |~ This is my\n          |~  castle...\n          /_\\\n      |~| # |_|~\n|~/_\\| # | __|~          |~\n/_\\|#| |#|/__/_\\._._._.|\n|#| |_| |_|___|#|...--...|\n|......-.......|  |#|          |\n\\| []  |#| []  |  |#|_____|\n|___|#|___|/\n...will U be my princess?",
                               
                               @"      I    _  _    U\n          (  \\/  )      _\n  ((`\\    \\  /        //)\n  \\\\ \\    \\/        ///\n    \\\\ \\_        _///\n      /    \\_  _/    \\\n    |      ^  )(  ^  |\n      )        {  }    (\n    /      \\;__:,  \\ \\\n  |        \\      )  /  |\n_|      \'----\'--\'  |_\n{_\\        /    \\      /_}\n    |      ||      ||  |\n    |      ))    ((  |\n    `\"\"\"\"``    ``\"\"\"`",
                               
                               @"\u250F\u2557 \u250F\u2557\n\u2551\u2503 \u2551\u2503\u2554\u2501\u2566\u2566\u2533\u2550\u2557\n\u2551\u2503 \u2503\u255A\u252B\u2551\u2503\u2503\u2503\u2569\u252B\n\u2517\u255D \u255A\u2501\u2569\u2550\u253B\u2501\u2569\u2501\u255D\n\u2513\u2554\u2513\n\u2551\u255A\u251B\u2523\u2550\u2566\u2533\u2557\n\u2517\u2557\u250F\u2563\u2503\u2503\u2551\u2503\n  \u2517\u255D\u2517\u2550\u253B\u2550\u255D",
                               
                               @"::: (\\_(\\  I\n*: (=\u2019 :\u2019)  :* miss\n\u2022..(,(\u201D)(\u201D) \u00A4\u00B0.\u00B8\u00B8.\u2022\u00B4\u00AF`\u00BB you",
                               
                               @"",
                               
                               @"I will love you\n        ,\n      ()    /)\n----.---\'----( @)\n    \\        \\)\n    ()\n    `\nuntil this rose wither..",
                               
                               @"    (\\/)  .-.    .-.\n    \\/  ((`-)  (-`))\n            \\\\      //  (\\/)\n              \\\\  //      \\/\n  .=\"\"\"=._))((_.=\"\"\"=.\n/  .,  .\'  \'.  ,.            \\\n/__(,_.-\'        \'-._,)___\\\n`    /|                  |\\        `\n    /_|__          __|_\\\n      | `))          ((` |\n      |                  |\n    -\"==        ==\"-",
                               
                               @"      __...._.-.\n    / .-.      \'-.)\n    \\ \' ,          \\\n      |\'.        o\'--D\n      \\.          /      |\n        ; ._    _\\ -\'-/\n      .\'      __\' \\\' .- \" -.  .- \" -.\n    .\'      .\'    \' .|\'          \'        \',\n  /        \\      \' ._,                |\n  ;            \'-._        \\            /\n( |              / \' -.__/          . \'\n  \\  __      , \'          \'-. , .-\'\n    \'/    \\..-\'|\n    |    |    \'-..,\n    |    \'-.  __  )\n    \\,___,)",
                               
                               @"        .          .            .\n.            .        //    O    .\n                    //\n        .          *                  .\n                            .\n        .\n                    .            .\n      (\\(\\ (\\(\\\n      (    )(    )\n__ (  o  )(  o )________\n\u2730*\u2312*\u2730\u203F\u2730*\u2312*\u2730\u203F\u2730*\n    Love is in the air\n\u2730*\u2312*\u2730\u203F\u2730*\u2312*\u2730\u203F\u2730*",
                               
                               @"  /) /)      (\\ /)\n(.\u2022  )      (\u2022.\u2022  )\n(  o )    ( v v )\n\nAre you mad at me?", 
                               
                               @"\uFE4E \u2508 \u2508 \u2508 \uFE4E\n\uFE4E\u2508    \u25CF  o  .\uFE4E \uFE4E\n\u2508 \u2508 /\u2588\\/\u2593\\ \uFE4E \u2508\n\u2585\u2586\u2587\u2588\u2588\u2588\u2588\u2588\u2587\u2586\u2585",
                               
                                                          nil];
    
    NSMutableArray* arrFood = [[NSMutableArray alloc] initWithObjects:
                               
                               @"     [_ ]\n   /      \\\n /_____\\\n| COLA |\n| _ (~)_ .|\n \\          /\n /          \\\n|             |\n U_U_U",
                               
                               @"                    )\n                  O\n          (                  )\n      (                          )\n  (                                )\n(                                      )\n  \\      \\        l        /    /\n    \\      \\      l      /      /\n      \\      \\    l    /      /\n      ^^^^^^^^^^^^",
                               
                               @"\u2554\u2566\u2566\n\u2560\u256C\u256C\u256C\u2563\n\u2560\u256C\u256C\u256C\u2563OK! WHO ATE MY\n\u2560\u256C\u256C\u256C\u2563CHOCOLATE!!\n\u255A\u2569\u2569\u2569\u255D",
                               
                               @"  ___________\n  |___________|\n  |____________\n/CCCCCCCCCc\\\n/COCOOOOOOC\\\n|CCCCCCCCCCC|\n|CCOOOOOCOCc|\n|COOC|\u2015\u2015\u2015\u2015\u2015|\n|CCCC|  CHERRY |\n|CCCC|      JAM    |\n|COCC|_________ |\n|CCCCCCCCCCC |\n\\CCOCCCOCCCC/\n  \u2015\u2015\u2015\u2015\u2015\u2015\u2015",
                               
                               @"    \u2570\u256E\u2570\u256E\u2570\u256E\n\u256D\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u256E\u2571\u2003\n\u2570\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u256F\u2571\u2003\n\u2503\u256D\u256D\u256E\u250F\u250F\u250F\u250F\u2523\u2501\u256E\n\u2503\u2503\u2503\u2503\u2523\u2523\u2523\u2523\u2503\u2571\u2503\u2003\n\u2503\u2570\u2570\u256F\u2503\u2503\u2517\u2517\u2523\u2501\u256F\u2003\n\u2570\u2501\u2501\u2501\u2501\u2501\u2501\u2501\u256F\u2003",
                               
                               @"                          ___\n                        /        \\\n      \\---O-------\\_/--\\_/-\n        \\.  o            o    /\n          \\.      o        /\n            \\.      o    /\n              \\. o      /\n                    | |\n                    | |\n                    | |\n              ___|_|___",
                               
                               @"              _\n    _    _|  |_    _\n    \\ \\  |  |  |  |  / /\n      \\ \\|  |  |  |/ /\n        |\\|_|_|_|/ /|\n        |              |\n        |  fries    |\n        \\______/",
                               
                               @"* @( )O oc( )*\n(Q@*0CG*O()  ___\n|\\__________/|/    _  \\\n|    |      |      |  |  /    |  |\n|    |      |      |  |  |    |  |\n|    |      |      |  |  |    |  |\n|    |      |      |  |  |    |  |\n|    |      |      |  |  |    |  |\n|    |      |      |  |  \\_  |  |\n|    |      |      |  |\\___  /\n|\\__|___|___|_/|\n\\__________/",
                               
                               @"        _____________\n      / \\_____________\\\n    /  //                      //\n  /  //                      //\n/_//____________//\n\\  \\\\    \'          ,      \\\\\n  \\  \\\\,        \'            \\\\\n    \\  \\\\_____,___\'___\\\\\n      \\ /_____________\\\\\n        WHO ATE ALL\n          MY PIZZA?!?!",
                               
                                                         nil];
    
    NSMutableArray* arrMusic = [[NSMutableArray alloc] initWithObjects:
                                
                                @"        _\n      / 7\n      /_(\n      |_|\n      |_|\n      |_|\n      |_|  /\\\n/\\..|=|/  /\n\\    |_|  /\n  )  _  \\\n  /  |_|    \\\n/    -=-o  /\n\\    /~ \\ _/\n  \\/",
                                
                                @"  :l|||l:\n    |||\n  .~|||~..\n/    |||    \\\\\n)} ||| { (((\n|    =    I||\n\\    \u00A5  ///\n    **|***\n      |",
                                
                                @"      : # :\n        #\n        #\n        #\n    . - # - .\n  (      #      )\n  )    ( # )  (\n  /      #      \\\n(        #        )\n  `    ____    \'",
                                
                                @"  /\\\n:=\n  |_|\n  |o|\n()|o|\n()|o|p\n  |=|p\n  |o|\nq|o|\nq|o|\n/__\\",
                                
                                @"    U\n    | |\n  / /  \\ \\\n| |      | |\n[=====-]\n[=====-]\n[=====-]\n| |    | |\n\\ \\_/ / |\n      /  \\\n    /___\\",
                                
                                @"          ________\n    ___ \\          /\n.\u2022\'  ___\'\u2022\\        /O\n| |  -  - - \\)      (/\n| |=||:||:||===\'\'\'\'\'\n\\ \\_____/      /\n  \'--______--\'",
                                
                                @"    ______\n  /              \\___\n|\\                      \\\nl\\ \\____________\\\n|| \\|{||l|l||l|l||l|||l|l||l|l||}\n  || ***************||",
                                
                                nil];
    
    NSMutableArray* arrPeople = [[NSMutableArray alloc] initWithObjects:
                                 
                                 @"\u250E-\u2501\u2501-\u2511\nd  \u3054, .\u3054 b-\n\u2503\u252D\u2500\u252E\u2503\n\u2516\u2526\u2502\u251E\u251A",
                                 
                                 @"                _____\n              /    _ ) ) )\n            (___|  \'  \' -\n                ;  _ =\n        ___/  / _      / _\n      /  )    \\  /    )    ) )\n      /  / |  -  - / \\  \\/ ;\n    |  /  |        /    \\  /\n    ;    : : : : : :\n_ ( /  / / / / / / \\ \\ \\ \\ \\\n_/ | _ / / / / / / /    /  /\n                      |  / | /\n                      |    |\n                    ( | ( |",
                                 
                                 @"    /\\_/(___)\\/(_,^,\n  /.........................>\n\uFE64............................\\\n  .|/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\|\n/\u2022\\  ___      ___  /\u2022\\\n\\\u2022\u2022\\.    O      O    /\u2022\u2022/\n  W \\.      _        / W\n        \\_______/\n        /(|.    |)\\\n      /  \\ \\__/ / \\\n    /    )/\\_/\\(    \\\n  /    /  |  |  \\  \\\n/  / |  |  |.  |\\  \\\n\\  \\ |  |  |.  |/.  /\n  \\  |.  |  |    |.  /\n    \\ |  \\ /    | /\n      |______|\n      |.          |\n      |            |\n      |.    |.    |\n      |.    |.    |\n      |      |    |\n      |.    |.    |\n      |/---\\|/---\\|\n      |___|___|",
                                 
                                 @"  ____\n/_____\\__\n|        0 |    FREEZE!!!\n\\_____O\n/          \u2014\u2014\u2014 ___\n|          \u2014\u2014\u2014|_|\u2014\n|  Police|\n|______/\n[    [_]  |\n|          |\n|  __    |\n| [__]  |\n|        |\n|        |\n|_____|__\n  |_______|",
                                 
                                 @"    \u25DF\u25DF\u25DE\u25DE\n  (\u25D4\u25E1\u25D4)\n\u2570 \u2592\u2592 \u256F\n    \u255C\u2559",
                                 
                                 @"            .----------.\n            |  __    __  |\n          (|    c    c  |)\n            |      L      |\n            \\    /__\\    /\n              \\______/\n      _____/        \\_____\n    /      \\ \\              \\ \\    \\\n  /      |  | |  :;:;:;|;:;:;:| |  |  \\\n  |      | /  \\_:;:;:|:;:;:/  \\_|    |\n  |      |\\                      /|    |\n  \\    /  |                    | |  /",
                                 
                                 @"\u2572\u2572\u256D\u2501\u2501\u2501\u2501\u256E\n\u256D\u256E\u2503\u2586\u2508\u2508\u2586\u2503\u256D\u256E\n\u2503\u2570\u252B\u25BD\u25BD\u25BD\u25BD\u2523\u256F\u2503\n\u2570\u2501\u252B\u25B3\u25B3\u25B3\u25B3\u2523\u2501\u256F\n\u2572\u2572\u2503\u2508\u2508\u2508\u2508\u2503\n\u2572\u2572\u2503\u2508\u250F\u2513\u2508\u2503\n\u2594\u2594\u2570\u2501\u256F\u2570\u2501\u256F",
                                 
                                 @"  ___________\n //////////            \\\\\n|||||||||||                 |\n|||||||||O )    ( O ) ,)\n|||||||        /           /|\n||||||\\       __       /|||\n||||||||\\_______/||||\n|||||||||||||____|||||||||\n||||||||||                   \\\n||||||| |               |     |\n||||||  |               |     |",
                                 
                                 
                                 @"        \u3006\u2312\u2312\u30FD\n      \u5F61 / / / \u5165\u30FD\n    / /  \uFF3F    \uFF3F \u30DF )\n  d    \uFF41    \uFF41  \u30DF )\n  \uFF5C \"  \u03B9    \"    | ) )\n      \uFF3C    \u30FC    \u4EBA ( (\n      /    ` \u30FC\u2032        \u309D\n    /        \uFF36            )\n    /  /__        \uFF59    /\n  (_____\u30DF__/    / |\n\uFFE3\uFFE3\u5F61_______/\uFFE3\uFFE3\uFFE3",
                                 
                                 @"                .\n                / \\\n              _\\ /_\n    .    .  (,\'v`.)  .    .\n    \\)    ( )  ,\' `.  ( )  (/\n      \\`. / `-\'    `-\' \\ ,\'/\n        : \'  ______  \' :\n      |  _,-\'  ,-.  `-._  |\n      |,\' ( )_ `-\' _( ) `.|\n      (|,-,\'-._  _.-`.-.|)\n      /\uFE64(o)> \uFE64(o)>\\  \\\n      :  :      |  |      :  :\n      |  |      (.-.)      |  |\n      |  |  ,\' ___ `.  |  |\n      ;  |)  / ,\'---\'. \\  (|  :\n  _,-/  | /\\(      )/\\  |  \\-._\n  -\'.-(    |  `-\'\'\'-\'  |    )-.`--\n    `.  ;`._______,\':  ,\'\n    ,\' `/                    \\\'`.\n      `-------.--------\'\n                  \'",
                                 
                                 @"    |                /\n  \\|/            \\|/\n  \\|//        \\|//\n  \\\\|//      \\\\|///\n  \\\\///    \\\\|//\n  \\\\|//    \\\\|///\n  \\\\///_\\|///____\n\\\\///|/|///////////    \\\n//\\|//\\|//XXXXXXX\\\n///|//////////////    \uFE64  |\n//////////////////    ==    \\\n/////////////////            __\\\n//////////////@        ~~\n  ///////////  ||            |\n    ///////  /|\\      ___/\n    XXX  /|\\\\    /\n  ////////    //|\\\\    /\n////////    //|\\\\.  |\n/////////    //|\\\\      \\\n/////////    //|\\        \\\n/////////      /|\\            \\\n/////////      |              \\",
                                 
                                 @"            .----.\n        _.\'__    `.\n  .--(#)(##)---/#\\\n.\' @          /###\\\n:            ,  ####\n  `-..__.-\' _.-\\###/\n        `;_  :    `\'\n          .\'\"\"\"\"\"`.\n      /,    BE    ,\\\n      //  COOL!  \\\\\n      `-._______.-\'\n        ___`.| .\'___\n      (____| ____)", 
                                 
                                 @"\u256D \uFE4C \u256E  \u256D \u223D \u256Eo\u03BF\u041E\u25CB\n(o \".\" o)  (o -.- o)\n(~~\uFE4A\uFE38  \uFE38\uFFE3\uFE38)\n(  \u25C6 \u25C6      \u25C6 \u25C6  )\n(  \u25C6 \u25C6      \u25C6 \u25C6  )\n(  \u25C6 \u25C6      \u25C6 \u25C6  )",
                                 
                                                                nil];
    
    NSMutableArray* arrSport = [[NSMutableArray alloc] initWithObjects:
                                
                                @"      ________\n      |      __      |            o\n    |      |__|    |    O__/\n    |__WW ___|    /  \\____\n            |  |              \\    \\___\n            |  |\n            |  |\n______|    |_______",
                                
                                @"              *\\o/*\n                  |\n                /\\\n  *\\o/*  o// \\\\o    *\\o/*\n      |      |        |        |\n    /\\    /\\      /\\      /\\",
                                
                                @"    ,--.            \uFE64__)\n  `- |________7\n      |`.            |\\\n  .--|.  \\          |.\\--.\n/    j  \\  `.7__j__\\    \\  ------\n|    o  |  (o)____O)  |  -----\n\\        /    J    \\          /    ---\n  `---\'              `---\'",
                                
                                @"      _ . - = \" \" = - . _\n    .\' \\ \\ -  +++++  - / / \' .\n(      | |                    | |      )\n  \' . / /                    \\ \\ . \'\n        ` \' - = . . = - \' `",
                                
                                @"|            |\n|            |\n|            |\nL______J\n      |    ()\n,,,,,,,|,,,,,\\\\,,,,,\n              \\\\\n                \\\\    0\n                  \\\\  /|\\\n,,,,,,,,,,,,,,,,,,,,,, JL,,,,,,,,,,,,,,",
                                
                                @"              O (\u2022\u2022)\n                \\_|_\n                  /\\", 
                                
                                @"|####################|\n                      \u2022\n                (|||) O\n                _ |_/\n                  /\\",
                                
                                                            nil];
    
//    NSMutableArray* arrRabbit = [[NSMutableArray alloc] initWithObjects:
//                                 
//                                 
//                                 nil];
    
    NSMutableArray* arrAnimal = [[NSMutableArray alloc] initWithObjects:
                                 
                                 @"._/\\_      __/\\__\n) . (_    _) .\'  (\n`) \'.(      ) .\'    (`\n  `-._\\ (_ ) /__(~`\n      (ovo)-.__.--._\n      )            `-.____\n    /\n    ( ,// )\n    `\'\'\\/-.\n            \\\n            |",
                                 
                                 @".              ,%%%%%,\n              ,%%%%%%,\n            ,%%/\\%%/\\%%,\n          ,%%%\\c \"\" J/%%,\n%.    %%%%/ o  o \\%%,\n`%%.  %%%%  _    |%%,\n`%%`  %%%(___Y__)%,\'\n    //        ;%%%`\\-/%%\'\n  ((            / `%%%%\'\n    \\\\        .\'              |\n      \\\\    /          \\  | |\n        \\\\/              ) | |\n          \\            /_ | |__\n        (__________)))))))",
                                 
                                 @"        ,,////,\n      _////////_\n    .\' -,  / / /`\'-._    _.-\'|\n    / _  \\\\/ / / / /  \',.=\'_.\'/\n  / (o)  ||/_/_/_/_/_/_.-\'_.\'\n.\'      ||\\ \\ \\ \\ \\ \\ \'-._\'.\n\'.--.    //\\ \\ \\ \\ \\  .\'\"-._\'.\\\n  `\'-.\\ \\  \\ \\ \\__.-\'\\)    \'-.|\n      \\\\)`\"\"\"\"\"`\n        `",
                                 
                                 @". M\n/ \'  \\            )\n`    \\_____/\n      (_____) o\n      /|        /|  o\n      / |        \\|  8o",
                                 
                                 @"  __\n.^o ~\\\nY /\'~) }        __\nl/    / /    ,-~    ~~--.,_\n    ( (    /    ~-._        ^.\n    \\ \"--\'--.          \"-._      \\\n    \"-.___              --.,_ ^.\n              \"~r-.,___.-\'-.  ^.\n                YI    \\          ~-.\\\n                ||      \\            `\\\n                ||      //\n                ||      //\n                ()    //\n                ||  //\n                ||  ( c\n  _ _  ___I|__`--__._ __  _\n\"~    ~  \"~  ::  ~~\"    ~  ~~\n                ::\n                .:",
                                 
                                 @".    .-\"-.\n  / |6 6|\\\n  {/(_0_)\\}\n  _/ ^ \\_\n  (/ / ^ \\ \\)-\'\n    \"\"\' \'\"\"",
                                 
                                 @".    __\n>( \'    )\n    )  /\n  /  (\n  /      `-------- /\n  \\      ~=-    /\n~^~^~^~^~^~^~^",
                                 
                                 @".                          , .\n                        ( \\( \\ )\n,_                    ;    o >\n{ `--.              /    (_)\n`={ \\`-._____/`    |\n  `-{    /      -=`\\      |\n    `={  -= = _/    /\n      `\\      .-\'      /`\n        {`-____.\'===,_\n        / /`                `\\\\\n      / /\n      `\\=",
                                 
                                 @".  __                  __\n/    \\              /      \\\n|      \\______/        |\n|        (\u2022)    (\u2022)          |\n|                              |\n\\ _/v |        | v\\_ /\nV  |    \\      /      |\n  \\|      \\    |__    |\n    |        | \\___/    |\n    |____|    |____|",
                                 
                                 @"\u2606\u256D\u2510\u250C\u256E\u2606\u00B0\uFF0E\u00B7\n\u256D\u2518\u2514\u2518\u2514\u256E\u2234\u00B0\u2606\u00B0\n\u2514\u2510\uFF0E\uFF0E\u250C\u2518\u2014\u256E\u2234\u00B0\n\u256D\u2534\u2014\u2014\u2524        \u251C\u256E\n\u2502\uFF4F\u3000\uFF4F\u2502\u3000\u3000\u2502\u25CF\u00B0\n\u2570\u252C\u2014\u2014\u256F        \u2502 \u2234\u00B0\u00B7\n\u2606 \\\u02CD\u02CD\u02CD|\u02CD\u02CD\u02CD/\u02CD\u02CD\u02CD/\u02CD\u02CD\u02CD/\u2234\u2606.",
                                 
                                 @"                _    _\n                _  \\ / __\n                /  \\ | /  \\\n                    \\|/\n              _,.---v---._\n  /\\__/\\  /                \\\n  \\_  _/ /                  \\\n    \\ \\_|            @ __|\n      \\                    \\_\n      \\    ,__/          /\n    ~~~`~~~~~~~/~~~~",
                                 
                                 @".==-.                    .-==.\n\\()8`-._  `.  . \'  _.-\'8()/\n(88\"  ::.  \\./  .::  \"88)\n  \\_.\'`-::::. (#) .::::-\'`._/\n    `._... .q(_) p. ..._.\'\n      \"\"-..-\'  |=| `-..-\"\"\n      .\"\"\' .  \' |=| `. `\"\".\n    ,\':8(o)./ |=| \\.(o)8:`.\n  (O :8 ::/ \\_/ \\:: 8: O)\n    \\O `::/          \\::\' O/\n    \"\"--\'              `--\"\"", 
                                 
                                 @"          .\"`\".\n  .---./ _=_ \\.---.\n{    (,(oYo),)    }\n{    {|  \"  |}      }\n{    { \\(---)/ }    }\n{    {}\'-=-\'{}      }\n{    {}._:_.{}      }\n{    {} -:- {}      }\n{_{_}`===` {_}_}\n((((((\\)        (/))))))",
                                 
                                                              nil];
//    
//    NSMutableArray* arrVehicle = [[NSMutableArray alloc] initWithObjects:
//                                  
//                                  
//                                  nil];
    
    NSMutableArray* arrWeapon = [[NSMutableArray alloc] initWithObjects:
                                 
                                 @"    ,______________,\n*\\\\ |______________|=\n    /xxxx/\n  /xxxx/\n/xxxx/",
                                 
                                 @"=_________________.\n  |_    _\'\u2014\'_________|=\n    /      /_%__|\n  /      /\n/____|",
                                 
                                 @"        /\\\n      / /  \\\n      | |  |\n      | |  |\n      | |  |\n      | |  |\n      | |  |\n      | |  |\n  __ | |  | __\n/___| |_ |___\\\n        ww\n        MM\n      _MM_\n    (&\uFE64>&)\n      ~~~~",
                                 
                                 @"   /:::::::::::::::|________^\n /________.|_________]\n  /../D   |____|\n/../        |____|\n             |____|",
                                 
                                 @"|:::::::::\"\"\"\"\"\"\"\"\"\"\"\"\"======^\n|____/\'\'\'\'/. /D\'\'\'\'\'\':::\'\'\n             /_/     [.     .]",
                                 
                                 @"            ,  * **** *  ,\n          , (                ) ,\n    , (                          ) ,\n  , (  ~~  ,,*,, ,,*,, ~~    ) ,\n(* , , ~ ~  | | | | |  ~ ~ ,  , *)\n    ( *      | | | | |      *    )\n            {~*~*~*~}\n                | | | | |\n                | | | | |\n            \\ | | | | | /\n    ...*. \\@@@@@/.*...",
                                 
                                 @"                /\\    /\\\n              |    ||    |\n              |    ||    |\n                \\  ||  /\n                  \\ || /\n                    ||\n                    ||\n                    ||\n                    ||\n                    ||\n                    ||\n                /\\ | | /\\\n              |  \\||/  |\n              |    \u00A5    |\n              \\        /\n                \\    /\n                  \\ /",
                                 
                                 @"                  #\n                ((\n                  ))\n              [===]\n          \u2022            \u2022\n        \u2022                  \u2022\n        \u2022                  \u2022\n          \u2022 ............ \u2022",
                                 
                                 @"    \u2551\\.\n    \u2551\u2592\\.\n    \u2551\u2592\u2592\\\n    \u2551\u2591\u2592\uFEFF\u2551\n    \u2551\u2591\u2592\u2551\n    \u2551\u2591\u2592\uFEFF\u2551\n    \u2551\u2591\u2592\u2551\n    \u2551\u2591\u2592\u2551\n    \u2551\u2591\u2592\uFEFF\u2551\n    \u2551\u2591\u2592\u2551\n    \u2551\u2591\u2592\uFEFF\u2551\n    \u2551\u2591\u2592\u2551\n\u2593\u2593\u2593\u2593\u2593\u2593\u2593\n    ]\u2588\u2593[\n    ]\u2588\u2593[\n    ]\u2588\u2593[",
                                 
                                 @"      ________\n    /  /--_____/\n  /  / . (_____)\n/_/    / V V V \\\n       /_|__|__|_\\\n       |_|__|__|_.|\n       |_|__|__|_.|\n       |_|__|__|_.|\n       \\_|__|__|_/",
                                 
                                 nil];
    
    NSMutableArray* arrHalloween = [[NSMutableArray alloc] initWithObjects:
                                    
                                    @"  /|                            |\\\n  | \\ _,.--\"\"\"\"\"\"\"--.,_ / |\n  .\'-\'                        \'-\'.\n:    **--.        .--**    :\n  ).  (\u263B\u263B) \'  \' (\u263B\u263B)  .(\n/_.  \'\"\"\"\'  /| |\\  \'\"\"\"\'  ._\\\n    /_. . . . . . . . . . _\\\n        ^vV|/\\|v|/\\|Vv^",
                                    
                                    @"           \\ \\\n  . - \'``` ^ ```\' -.\n/     /\\  __  /\\    \\\n|    ^^  \\/  ^^    |\n\\    \\_..__.._/   /\n  ` \' - ....... - \' `",
                                    
                                    @"                  |\\\n                /. \\\n              /.    |\n          \uFE64.\u2026.......>\n          ///\'\'o\'\'\'\'\'o\'\'\' (:::)\n        /// (.    V.    ) /  /\n      ///.  \\. \\.    \\  /  /\n          /  \\. \\.  _\\.\n        (.    (:::).      )\nww---/.              /----------\nww  \"\"(&)\"\"\"(&)\"",
                                    
                                    @"    _____\n  /            \\\n  | R. I. P  |\n  | ~~~~~ |\n  | ~~~~~ |\n/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\",
                                    
                                    @"            ________\n          /    ~    ~  \\\n          )    \u20AC    \u20AC    )\n        (  |\\  ____/|  (  BOO!!!!\n  /\u2014\\ )  \\_____/  )    /--\\\n  |      |                    |  /    |\n  \\                                      /\n    \\                                /\n        \\                            /\n          \\                      /\n            )                    )\n              (                (\n                )              )\n                (            (\n                    )        )\n                      (      (\n                        )  (\n                          \\/",
                                    
                                    @"              .-\"\"\"\".\n            /            \\\n    __  /  . - .    .. \\\n  /    \'\\ /      \\/      \\\n  |  _  \\/      .==.==.\n  |  (    \\ \\    /____\\__\\\n  \\  \\            ( _( )( _( )\n    \\  \\                      \'---.\n      \\                                \\\n/\\    |`            (__)______/\n  \\  |      /\\ ____/\n    \\        \\| | V V\n      \\        \\| \" \" \" \",\n        \\      ______)\n          \\    /`\n            \\ (",
                                    
                                    @"  \u2571\u2594\u2594\u2594\u2594\u2594\u2594\u2572\n\u2595\u2508\u256D\u2501\u256E\u256D\u2501\u256E\u2508\u258F\n\u2595\u2508\u2503\u256D\u256F\u2570\u256E\u2503\u2508\u258F\n\u2595\u2508\u2570\u256F\u256D\u256E\u2570\u256F\u2508\u258F\n\u2595\u2508\u2508\u2508\u2503\u2503\u2508\u2508\u2508\u258F\n\u2595\u2508\u2508\u2508\u2570\u256F\u2508\u2508\u2508\u258F\n\u2595\u2571\u2572\u2571\u2572\u2571\u2572\u2571\u2572|",
                                    
                                    @"   . - - - .\n/              \\\n|  ( \\    / )  |\n( _   o   _  )\n   | === |\n   \' - . - \'",
                                    
                                    @"        . - .\n     ( e . e )\n       ( m )\n   . - =\"= - .     W\n / /   =T=  \\ \\ , /\n( )   ==|==   ( )\n  \\     =V=\n   M( oVo )\n       / /   \\ \\\n     / /       \\ \\\n   ( )           ( )\n    \\ \\          | |\n      \\ \'        \' |\n  ==\"          \"==",
                                    
                                    @"                    /\\\n                  /    \\\n                /        \\\n          ==========\n          ((( /  \u2022  \u2022 \\ )))      ||\n            )))\\_  - _/(((      ||\n          (((.-/      \\=)))==[||}\n              | |        |        ||\n              |,\\.    ./        ||\n                *=O=          ||\n                /        \\        ||\n              /            \\      ||\n            |.._._._._..|    /\'\'\'\'\\\n                  ||  ||          |/|/\\|\n                o> o>",
                                    
                                                                nil];
    
//    NSMutableArray* arrChristmas = [[NSMutableArray alloc] initWithObjects:
//                                    
//                                    nil];
    
    arrArtTextContent = [[NSMutableArray alloc] initWithObjects: arrCelebration, arrLife, arrLove, arrFood, arrMusic, arrPeople, arrSport, arrAnimal, arrWeapon, arrHalloween, nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSInteger count = 0;
    
    if (tableView == tblArtEmoji)
        count = [arrArtEmoji count];
    if (tableView == tblArtText)
        count = [arrArtText count];
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:@"acell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"acell"];
    }
    
    if (tableView == tblArtEmoji) {
        ArtCategory* artCategory = [[ArtCategory alloc] init];
        artCategory = [arrArtEmoji objectAtIndex: indexPath.row];
        cell.textLabel.text = artCategory.title;
        cell.imageView.image = [UIImage imageNamed: artCategory.imageName];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if (tableView == tblArtText) {
        ArtCategory* artCategory = [[ArtCategory alloc] init];
        artCategory = [arrArtText objectAtIndex: indexPath.row];
        cell.textLabel.text = artCategory.title;
        cell.imageView.image = [UIImage imageNamed: artCategory.imageName];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArtSectionViewController* artSectionViewCtrl = nil;
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (delegate.nCurrentDevice == IPAD) {
        artSectionViewCtrl = [[ArtSectionViewController alloc] initWithNibName:@"ArtSectionViewController_iPad" bundle:nil];
    } else {
        artSectionViewCtrl = [[ArtSectionViewController alloc] initWithNibName:@"ArtSectionViewController" bundle:nil];
    }
    
//    NSMutableArray* arrImageName = [[NSMutableArray alloc] init];
    NSMutableArray* arrArt = [[NSMutableArray alloc] init];
    ArtCategory* artCategory = [[ArtCategory alloc] init];

    if (tableView == tblArtEmoji) {
        arrArt = [arrArtEmojiContent objectAtIndex: indexPath.row];
        artCategory = [arrArtEmoji objectAtIndex: indexPath.row];
    }
    if (tableView == tblArtText) {
        arrArt = [arrArtTextContent objectAtIndex: indexPath.row];
        artCategory = [arrArtText objectAtIndex: indexPath.row];
    }
//    [picSectionViewCtrl setArrayImageName: arrImageName];
    
    [artSectionViewCtrl setArtArray: arrArt];
    [artSectionViewCtrl setArtType: nArtType];
    [artSectionViewCtrl setControllerType: nControllerType];
    artSectionViewCtrl.parentViewCtrl = self;
    artSectionViewCtrl.title = artCategory.title;
    
    [self.navigationController pushViewController:artSectionViewCtrl animated:YES];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void) addArt:(NSString*) str
{
    KeyBoardViewController* keyboardViewCtrl = (KeyBoardViewController*) self.parentViewCtrl;
    [keyboardViewCtrl addArt:str];
}

@end
