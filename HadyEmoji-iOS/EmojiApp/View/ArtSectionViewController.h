//
//  ArtSectionViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableViewDataSource.h"
#import <MessageUI/MessageUI.h>
#import "iAd/ADBannerView.h"
#import "GADBannerView.h"

@interface ArtSectionViewController : UIViewController <UIBubbleTableViewDataSource, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet UIBubbleTableView *bubbleTable;
    
    NSMutableArray *bubbleData;
    int             nArtType;
    int             nControllerType;
    
    GADBannerView*  bannerView_;
}

@property (nonatomic, retain) NSString* strText;
@property(nonatomic, retain) UIViewController* parentViewCtrl;

- (void) setArtArray:(NSMutableArray*) arr;
- (void) onAction;
- (void) setArtType: (int) type;
- (void) setControllerType: (int) type;

@end
