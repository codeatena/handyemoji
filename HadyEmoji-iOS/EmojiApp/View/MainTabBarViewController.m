//
//  MainTabBarViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "ArtListViewController.h"
#import "PicListViewController.h"
#import "FavouriteListViewController.h"
#import "SettingViewController.h"
#import "define.h"
#import "AppDelegate.h"

@interface MainTabBarViewController ()

@end

@implementation MainTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    if (self) {
        // Custom initialization

    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    tabBarCtrl = [[UITabBarController alloc] init];
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    ArtListViewController* artListViewCtrl = nil;
    PicListViewController* picListViewCtrl = nil;
    FavouriteListViewController* favouriteListViewCtrl = nil;
    SettingViewController* settingViewCtrl = nil;
    
    if (delegate.nCurrentDevice == IPAD) {
        keyboadViewCtrl = [[KeyBoardViewController alloc] initWithNibName:@"KeyboardViewController_iPad" bundle:nil];
        artListViewCtrl = [[ArtListViewController alloc] initWithNibName:@"ArtListViewController_iPad" bundle:nil];
        picListViewCtrl = [[PicListViewController alloc] initWithNibName:@"PicListViewController_iPad" bundle:nil];
        favouriteListViewCtrl = [[FavouriteListViewController alloc] initWithNibName:@"FavouriteListViewController_iPad" bundle:nil];
        settingViewCtrl = [[SettingViewController alloc] initWithNibName:@"SettingViewController_iPad" bundle:nil];
    } else {
        keyboadViewCtrl = [[KeyBoardViewController alloc] initWithNibName:@"KeyBoardViewController" bundle:nil];
        artListViewCtrl = [[ArtListViewController alloc] initWithNibName:@"ArtListViewController" bundle:nil];
        picListViewCtrl = [[PicListViewController alloc] initWithNibName:@"PicListViewController" bundle:nil];
        favouriteListViewCtrl = [[FavouriteListViewController alloc] initWithNibName:@"FavouriteListViewController" bundle:nil];
        settingViewCtrl = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    }
    
    UINavigationController* navArtCtrl = [[UINavigationController alloc] initWithRootViewController: artListViewCtrl];
    UINavigationController* navPicCtrl = [[UINavigationController alloc] initWithRootViewController:picListViewCtrl];
    UINavigationController* navFavouriteCtrl = [[UINavigationController alloc] initWithRootViewController:favouriteListViewCtrl];
    
    NSMutableArray* arrViewCtrl = [[NSMutableArray alloc] initWithObjects: keyboadViewCtrl, navArtCtrl, navPicCtrl, navFavouriteCtrl, settingViewCtrl, nil];
    self.viewControllers = arrViewCtrl;
    
    UITabBar *tabBar = self.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];

    tabBarItem1.title = @"Keyboard";
    tabBarItem2.title = @"Art";
    tabBarItem3.title = @"Pic";
    tabBarItem4.title = @"Favourite";
    tabBarItem5.title = @"Setting";
    
    UIImage* image = [UIImage imageNamed:@"icons_Art.png"];
    CGSize newSize = CGSizeMake(45, 35);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    tabBarItem1.image = [UIImage imageNamed:@"keyboard.png"];
    tabBarItem2.image = newImage;
    tabBarItem3.image = [UIImage imageNamed:@"pic.png"];
    tabBarItem4.image = [UIImage imageNamed:@"favorite.png"];
    tabBarItem5.image = [UIImage imageNamed:@"setting.png"];

    artListViewCtrl.nControllerType = CATEGORY_ART;
//    [self.view addSubview: tabBarCtrl.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{

}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSLog(@"tab selected: %@", item.title);
    
    if ([item.title isEqualToString: @"Keyboard"]) {
        [keyboadViewCtrl touchKeyboadTap];
    }
}

@end
