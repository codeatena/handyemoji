//
//  PicListViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"

@interface PicListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet    UITableView*        tblPicStatic;
    IBOutlet    UITableView*        tblPicAnimated;
    
    NSMutableArray* arrPicStatic;
    NSMutableArray* arrPicAnimated;
    
    NSMutableArray* arrPicSectionStatic;
    NSMutableArray* arrPicSectionAnimated;
    GADBannerView*  bannerView_;

    int nPicType;
}

@end
