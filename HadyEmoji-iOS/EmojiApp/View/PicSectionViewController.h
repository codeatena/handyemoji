//
//  PicSectionViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "GADBannerView.h"

@interface PicSectionViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIActionSheetDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    NSMutableArray* arrImageName;
    
    IBOutlet UICollectionView* collPicSection;
    
    int curIndex;
    int nPicType;
    int nControllerType;
    
    GADBannerView*  bannerView_;
}

- (void) setArrayImageName:(NSMutableArray*) arr;
- (void) setPicType: (int) nType;
- (void) setControllerType: (int) type;

@end