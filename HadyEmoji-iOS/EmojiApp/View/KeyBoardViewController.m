//
//  KeyBoardViewController.m
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "KeyBoardViewController.h"
#import "AppDelegate.h"
#import "define.h"
#import "ArtListViewController.h"
#import "GADBannerView.h"

@interface KeyBoardViewController ()

@end

@implementation KeyBoardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];

    [self loadTextView];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];

    if (delegate.nCurrentDevice == IPAD) {
        emojiViewCtrl = [[EmojiViewController alloc] initWithNibName:@"EmojiViewController_iPad" bundle:nil];
        [emojiViewCtrl.view setFrame: CGRectMake(0, 180, 320, 300)];
    } else {
        emojiViewCtrl = [[EmojiViewController alloc] initWithNibName:@"EmojiViewController" bundle:nil];
        [emojiViewCtrl.view setFrame: CGRectMake(0, 180, 320, 300)];
    }
  
//    [self.view addSubview:emojiViewCtrl.view];
    textView.internalTextView.inputView = emojiViewCtrl.view;
    [emojiViewCtrl setParentViewController: self];
//    [btnEmoji setTitle: @"ABC" forState: UIControlStateNormal];
    
    

    
//    textView.text = [NSString stringWithFormat:@"%c", 0x7c] ;
    
//    textView.text =  @".    sSs  \U0001f4a4\U0001f31f. *\n.  [ (-.-) ]        \U0001f319\n\u2014o-o\u2014*\n| \U0001f497\U0001f497  |.  \u2728*\n| \U0001f497\U0001f497  |*.\n\"Sweet Dreams..\"";
    
//    textView.text = [NSString stringWithFormat: @"\u002e    \u0073\u0053\u0073  \ud83d\udca4\ud83c\udf1f\u002e \u002a\n\u002e  \u005b \u0028\u002d\u002e\u002d\u0029 \u005d        \ud83c\udf19\n\u2014\u006f\u002d\u006f\u2014\u002a\n\u007c \ud83d\udc97\ud83d\udc97  \u007c\u002e  \u2728\u002a\n\u007c \ud83d\udc97\ud83d\udc97  \u007c\u002a\u002e\n\u0022\u0053\u0077\u0065\u0065\u0074 \u0044\u0072\u0065\u0061\u006d\u0073\u002e\u002e\u0022"];
    
//    NSMutableAttributedString *stringText = [[NSMutableAttributedString alloc] initWithString:@"This is a text"];
//    //Bold the first four characters.
//    [stringText addAttribute: NSFontAttributeName value: [UIFont fontWithName: @"Helvetica-Bold" size:14] range: NSMakeRange(0, 4)];
//    // Sets the font color of last four characters to green.
//    [stringText addAttribute: NSForegroundColorAttributeName value: [UIColor greenColor] range: NSMakeRange(9, 4)];
    
    bannerView_ = [[GADBannerView alloc]
                   initWithFrame:CGRectMake(0.0, 466, GAD_SIZE_320x50.width,                                   GAD_SIZE_320x50.height)];//设置位置
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = MY_BANNER_UNIT_ID;//调用你的id
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    [self.view addSubview:bannerView_];//添加bannerview到你的试图
    
    // Initiate a generic request to load it with an ad.
    [bannerView_ loadRequest:[GADRequest request]];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        viewMenu.frame = CGRectMake(0, 524, 320, 44);
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        viewMenu.frame = CGRectMake(0, 436, 320, 44);
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPAD) {
        viewMenu.frame = CGRectMake(0, 984, 768, 44);
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
    }
}

-(void) keyboardWillShow:(NSNotification *)note{
    
//    NSLog(@"keyboardWillShow");
    // get keyboard size and loctaion
    /*
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
	containerView.frame = containerFrame;
    
	
	// commit animations
	[UIView commitAnimations];
     */
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        viewMenu.frame = CGRectMake(0, 524, 320, 44);
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        viewMenu.frame = CGRectMake(0, 436, 320, 44);
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPAD) {
        viewMenu.frame = CGRectMake(0, 984, 768, 44);
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
    }
//    viewMenu.frame = CGRectMake(0, 524, 520, 44);
//    textView.inputView.frame = CGRectMake(0, 100, 320, 200);
    
    NSLog(@"growingTextViewDidBeginEditing");
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        [viewMenu setFrame: CGRectMake(0, 254, 320, 44)];
        [bannerView_ setFrame: CGRectMake(0, 298, 320, 50)];
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        [viewMenu setFrame: CGRectMake(0, 166, 320, 44)];
        [bannerView_ setFrame: CGRectMake(0, 210, 320, 50)];
    }
    if (delegate.nCurrentDevice == IPAD) {
        viewMenu.frame = CGRectMake(0, 622, 768, 44);
        bannerView_.frame = CGRectMake(10, 668, 748, 90);
    }
    
    //    adView.backgroundColor = [UIColor redColor];
    [UIView commitAnimations];
}
//
-(void) keyboardWillHide:(NSNotification *)note{
    /*
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	containerView.frame = containerFrame;
	
	// commit animations
	[UIView commitAnimations];
     */
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        viewMenu.frame = CGRectMake(0, 524, 320, 44);
        bannerView_.frame = CGRectMake(0.0, 466, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        viewMenu.frame = CGRectMake(0, 436, 320, 44);
        bannerView_.frame = CGRectMake(0.0, 378, GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    if (delegate.nCurrentDevice == IPAD) {
        viewMenu.frame = CGRectMake(0, 984, 768, 44);
        bannerView_.frame = CGRectMake(10, 877, 748, 90);
    }
    
    [UIView commitAnimations];
}

- (void) loadTextView
{
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (delegate.nCurrentDevice == IPHONE_RETINA) {
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 40)];
    }
    if (delegate.nCurrentDevice == IPHONE_GENEREAL) {
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 120, 320, 40)];
    }
    if (delegate.nCurrentDevice == IPAD) {
        containerView = [[UIView alloc] initWithFrame:CGRectMake(224, 120, 320, 40)];
    }
    
//    containerView.backgroundColor = [UIColor blueColor];
	textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(10, 3, 300, 40)];
    textView.isScrollable = NO;
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
	textView.returnKeyType = UIReturnKeyGo; //just as an example
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
    //    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor whiteColor];
//    textView.placeholder = @"Type to see the textView grow!";
    textView.backgroundColor = [UIColor clearColor];
    // textView.text = @"test\n\ntest";
	// textView.animateHeightChange = NO; //turns off animation
    
    [self.view addSubview:containerView];
	
    UIImage *rawEntryBackground = [UIImage imageNamed:@"bubbleKeyboard.png"];
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:15];
    UIImageView *entryImageView = [[UIImageView alloc] initWithImage:entryBackground];
    entryImageView.frame = CGRectMake(10, 0, 310, 45);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = [UIImage imageNamed:@"MessageEntryBackground.png"];
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:background];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    // view hierachy
    //    [containerView addSubview:imageView];
    [containerView addSubview:entryImageView];
    [containerView addSubview:textView];

    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = containerView.frame;
    r.size.height -= diff;
//    r.origin.y += diff;
	containerView.frame = r;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onEmoji:(id)sender
{
    NSString* strTilte = [btnEmoji titleForState: UIControlStateNormal];
    
//    NSLog(@"%@", strTilte);
    
    if ([strTilte isEqualToString: @"ABC"]) {
        [btnEmoji setTitle:@"Emoji" forState: UIControlStateNormal];
        [self showKeyboard];
    } else {
        [btnEmoji setTitle:@"ABC" forState: UIControlStateNormal];
        [self showEmoji];
    }
    
    [emojiViewCtrl onEmojiOrABC];
}

- (void) showKeyboard
{
    [textView resignFirstResponder];
    textView.internalTextView.inputView = nil;
    [textView becomeFirstResponder];
}

- (void) showEmoji
{
    [textView resignFirstResponder];
    textView.internalTextView.inputView = emojiViewCtrl.view;
    [textView becomeFirstResponder];
}

- (void) touchKeyboadTap
{
    [textView becomeFirstResponder];
}

- (IBAction)onArt:(id)sender
{
    ArtListViewController* artListViewCtrl = nil;
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    if (delegate.nCurrentDevice == IPAD) {
        artListViewCtrl = [[ArtListViewController alloc] initWithNibName:@"ArtListViewController_iPad" bundle:nil];
    } else {
        artListViewCtrl = [[ArtListViewController alloc] initWithNibName:@"ArtListViewController" bundle:nil];
    }
    
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController: artListViewCtrl];
    
    artListViewCtrl.nControllerType = KEYBOART_ART;
    artListViewCtrl.parentViewCtrl = self;
    
    [self presentViewController:navCtrl animated:YES completion:nil];
}

//- (IBAction)onFont:(id)sender
//{
//    [self showEmoji];
//    [emojiViewCtrl onFont];
//}

- (IBAction)onSend:(id)sender
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Message", @"Email", @"Copy", nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:self.view];
}

- (IBAction)onDone:(id)sender
{
    [textView resignFirstResponder];
}

- (void) addEmoji: (NSString*) strAdd
{
    NSRange range = textView.selectedRange;
    NSString * firstHalfString = [textView.text substringToIndex:range.location];
    NSString * secondHalfString = [textView.text substringFromIndex: range.location];
    textView.internalTextView.scrollEnabled = NO;  // turn off scrolling or you'll get dizzy ... I promise
    
    textView.text = [NSString stringWithFormat: @"%@%@%@",
                     firstHalfString,
                     strAdd,
                     secondHalfString];
    range.location += [strAdd length];
    textView.selectedRange = range;
    textView.internalTextView.scrollEnabled = YES;
    //    textView.internalTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
//    textView.selectedRange = NSMakeRange(textView.text.length, 0);
//    [textView scrollRangeToVisible:textView.selectedRange];


//    [textView scrollRangeToVisible:range];

}

- (void) addArt: (NSString*) strAdd
{
    [self showEmoji];

    NSRange range = textView.selectedRange;
    NSString * firstHalfString = [textView.text substringToIndex:range.location];
    NSString * secondHalfString = [textView.text substringFromIndex: range.location];
    textView.internalTextView.scrollEnabled = NO;  // turn off scrolling or you'll get dizzy ... I promise
    
    textView.text = [NSString stringWithFormat: @"%@%@%@",
                     firstHalfString,
                     strAdd,
                     secondHalfString];
    range.location += [strAdd length];
    textView.selectedRange = range;
    textView.internalTextView.scrollEnabled = YES;
//    textView.internalTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    textView.selectedRange = NSMakeRange(textView.text.length, 0);
    [textView scrollRangeToVisible:textView.selectedRange];
    
}

- (void) onBack
{
    [textView.internalTextView deleteBackward];
//    if (strText.length > 0) {
//        textView.text = [strText substringToIndex: strText.length - 1];
//    }
}

- (void) onClear
{
    textView.text = @"";
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self sendMessage];
    } else if (buttonIndex == 1) {
        [self sendEmail];
    } else if (buttonIndex == 2) {
        [self onCopy];
    } else if (buttonIndex == 3) {
        [self onSave];
    }
}

- (void) sendMessage
{
    MFMessageComposeViewController *controller = [[[MFMessageComposeViewController alloc] init] autorelease];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = textView.text;
//        controller.recipients = [NSArray arrayWithObjects:@"1(234)567-8910", nil];
        controller.messageComposeDelegate = self;
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void) sendEmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
//        [mailer setSubject:@"A Message from MobileTuts+"];
        
//        UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
//        NSData *imageData = UIImagePNGRepresentation(myImage);
//        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];
        
        NSString *emailBody = textView.text;
        [mailer setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController: mailer animated:YES completion:nil];
        [mailer release];
    }
}

- (void) onCopy
{
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:[textView text]];
}

- (void) onSave
{
    AppDelegate * delegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [delegate.savedArtArray addObject:textView.text];
    [delegate addArt:textView.text TYPE: STR_SAVED_ART];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) setFont: (UIFont*) font
{
    textView.internalTextView.font = font;
    [self showKeyboard];
    [btnEmoji setTitle:@"Emoji" forState:UIControlStateNormal];
}

@end