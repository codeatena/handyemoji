//
//  ArtListViewController.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iAd/ADBannerView.h"
#import "GADBannerView.h"

@interface ArtListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView*   tblArtEmoji;
    IBOutlet UITableView*   tblArtText;
    IBOutlet ADBannerView*  banner;
    
    NSMutableArray* arrArtEmoji;
    NSMutableArray* arrArtText;
    
    NSMutableArray* arrArtEmojiContent;
    NSMutableArray* arrArtTextContent;
    
    int         nArtType;
    
    GADBannerView*  bannerView_;
}

@property (nonatomic) int nControllerType;
@property (nonatomic, retain) UIViewController* parentViewCtrl;

- (void) addArt:(NSString*) str;

@end
