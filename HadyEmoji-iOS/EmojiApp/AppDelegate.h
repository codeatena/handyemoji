//
//  AppDelegate.h
//  EmojiApp
//
//  Created by JinSung Han on 3/8/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Art;
@class Pic;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) NSMutableArray* favEmojiArtArray;
@property (nonatomic, retain) NSMutableArray* favTextArtArray;
@property (nonatomic, retain) NSMutableArray* favStaticPicArray;
@property (nonatomic, retain) NSMutableArray* favAnimatePicArray;

@property (nonatomic, retain) NSMutableArray* savedArtArray;
@property (nonatomic, retain) NSMutableArray* savedPicArray;

@property (nonatomic, retain) NSMutableArray* recentArtArray; // test commit
@property (nonatomic, retain) NSMutableArray* recentPicArray;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic) int nCurrentDevice;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void) readAllData;
- (void) addArt:(NSString*) text TYPE:(NSString*)           strType;
- (void) refreshArt;
- (void) addPic:(NSString*) fileName TYPE:(NSString*)       strType;
- (void) refreshPic;

@end
