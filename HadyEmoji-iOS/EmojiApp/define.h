//
//  define.h
//  EmojiApp
//
//  Created by JinSung Han on 3/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#ifndef EmojiApp_define_h
#define EmojiApp_define_h

#define MY_BANNER_UNIT_ID       @"ca-app-pub-2929477936730053/1326339726"

#define STR_FAVORITE_ART_EMOJI  @"FAVOURTIE_ART_EMOJI"
#define STR_FAVORITE_ART_TEXT   @"FAVOURTIE_ART_TEXT"
#define STR_SAVED_ART           @"SAVED_ART"
#define STR_RECENT_ART          @"RECENT_ART"

#define STR_FAVORITE_PIC_STATIC     @"FAVOURTIE_PIC_STATIC"
#define STR_FAVORITE_PIC_ANIMATE    @"FAVOURTIE_PIC_ANIMATE"
#define STR_SAVED_PIC               @"SAVED_PIC"
#define STR_RECENT_PIC              @"RECENT_PIC"

/////////   Art

#define ART_TYPE_EMOJI  0
#define ART_TYPE_TEXT   1


#define FAVOURITE_EMOJI_ART     0
#define FAVOURITE_TEXT_ART      1
#define SAVED_ART               2
#define RECENT_ART              3

#define CATEGORY_ART            4
#define KEYBOART_ART            5


//////////   Pic

#define PIC_TYPE_STATIC         0
#define PIC_TYPE_ANIMATE        1


#define FAVOURITE_STATIC_PIC    0
#define FAVOURITE_ANIMATE_PIC   1

#define SAVED_PIC               2
#define RECENT_PIC              3

#define CATEGORY_PIC            4

#define IPHONE_GENEREAL         0
#define IPHONE_RETINA           1
#define IPAD                    2

#endif
